<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSellerInfoToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('shop_name')->nullable()->after('group_id');
            $table->text('shop_address')->nullable()->after('shop_name');
            $table->string('shop_phone')->nullable()->after('shop_address');
            $table->string('bank_name')->nullable()->after('shop_phone');
            $table->string('bank_account_number')->nullable()->after('bank_name');
            $table->string('bank_cart_number')->nullable()->after('bank_account_number');
            $table->string('bank_shaba_number')->nullable()->after('bank_cart_number');
            $table->integer('approved')->default(0)->after('bank_shaba_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('shop_name');
            $table->dropColumn('shop_address');
            $table->dropColumn('shop_phone');
            $table->dropColumn('bank_name');
            $table->dropColumn('bank_account_number');
            $table->dropColumn('bank_cart_number');
            $table->dropColumn('bank_shaba_number');
            $table->dropColumn('approved')->default(0);

            //
        });
    }
}
