-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Dec 08, 2020 at 06:44 PM
-- Server version: 5.7.26
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `pro`
--

-- --------------------------------------------------------

--
-- Table structure for table `active_codes`
--

CREATE TABLE `active_codes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` int(11) NOT NULL,
  `expired_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `active_codes`
--

INSERT INTO `active_codes` (`id`, `phone`, `code`, `expired_at`) VALUES
(3, '09123951214', 50447, '2020-12-04 14:28:27');

-- --------------------------------------------------------

--
-- Table structure for table `additional_pages`
--

CREATE TABLE `additional_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `additional_pages`
--

INSERT INTO `additional_pages` (`id`, `name`, `slug`, `status`, `meta_title`, `meta_description`, `description`, `created_at`, `updated_at`) VALUES
(2, 'تماس با ما', 'contact-us', 1, 'تایتل در باره ما', 'متن توضیحات صفحه', '<p>متن درباره ما</p>', '2020-11-30 22:12:00', '2020-12-01 12:59:31');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'وزن', '2020-11-04 13:04:58', '2020-11-04 13:04:58'),
(2, 'انرژی', '2020-11-05 06:14:23', '2020-11-05 06:14:23'),
(3, 'kjhg', '2020-11-05 06:30:36', '2020-11-05 06:30:36'),
(4, 'کشور سازنده', '2020-11-07 13:04:39', '2020-11-07 13:04:39'),
(5, 'منبع', '2020-11-07 13:08:11', '2020-11-07 13:08:11');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_product`
--

CREATE TABLE `attribute_product` (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `value_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_product`
--

INSERT INTO `attribute_product` (`product_id`, `attribute_id`, `value_id`) VALUES
(1, 1, 7),
(2, 1, 1),
(4, 1, 7),
(5, 1, 2),
(2, 2, 3),
(5, 3, 4),
(2, 4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_values`
--

CREATE TABLE `attribute_values` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_values`
--

INSERT INTO `attribute_values` (`id`, `attribute_id`, `value`, `created_at`, `updated_at`) VALUES
(1, 1, '۱۱۰', '2020-11-04 13:06:42', '2020-11-04 13:06:42'),
(2, 1, '۱۱۰کیلو', '2020-11-05 06:13:51', '2020-11-05 06:13:51'),
(3, 2, '۲۴۰ کالری', '2020-11-05 06:14:23', '2020-11-05 06:14:23'),
(4, 3, 'dfghj,', '2020-11-05 06:30:36', '2020-11-05 06:30:36'),
(5, 4, 'چین', '2020-11-07 13:04:39', '2020-11-07 13:04:39'),
(6, 5, 'سهانوین', '2020-11-07 13:08:11', '2020-11-07 13:08:11'),
(7, 1, '۱۱۰۰ کیلوگرم', '2020-11-09 11:28:52', '2020-11-09 11:28:52'),
(8, 4, 'ایران', '2020-11-10 13:28:14', '2020-11-10 13:28:14');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `name`, `location`, `status`, `created_at`, `updated_at`) VALUES
(3, 'اسلایدر', 'home-banner', '1', '2020-11-30 13:32:16', '2020-11-30 18:29:56');

-- --------------------------------------------------------

--
-- Table structure for table `banner_images`
--

CREATE TABLE `banner_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `banner_id` bigint(20) UNSIGNED NOT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banner_images`
--

INSERT INTO `banner_images` (`id`, `banner_id`, `images`, `url`, `created_at`, `updated_at`) VALUES
(6, 3, '/images/Slider/audio-technica-slider2.jpg', 'home', '2020-11-30 18:29:56', '2020-11-30 18:29:56'),
(7, 3, '/images/Slider/eve-audio-slider-new.jpg', 'home2', '2020-11-30 18:29:56', '2020-11-30 18:29:56');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `parent_id`, `description`, `meta_title`, `meta_description`, `image`, `sort_order`, `status`, `created_at`, `updated_at`) VALUES
(1, 'خشکبار', 'dried-fruit', 0, 'توضیحات خشکبار', 'متای خشکبار', 'توضیحاتمتای خشکبار', NULL, 2, 1, NULL, '2020-11-01 07:21:23'),
(4, 'حبوبات', 'beans', 0, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(7, 'میثم', 'sunflower', 0, 'توضیحات', 'متای عنوان', 'متای توضیحات', '/images/product1/ProdS2.jpg', 5, 1, '2020-11-01 07:22:26', '2020-11-25 18:28:19');

-- --------------------------------------------------------

--
-- Table structure for table `category_product`
--

CREATE TABLE `category_product` (
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_product`
--

INSERT INTO `category_product` (`category_id`, `product_id`) VALUES
(1, 1),
(4, 1),
(1, 2),
(1, 4),
(4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `state_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `state_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 2, 'تهران', '2020-11-09 17:50:44', '2020-11-09 17:50:44'),
(2, 2, 'پیشوا', '2020-11-09 18:19:23', '2020-11-09 18:23:23'),
(3, 2, 'پاکدشت', '2020-11-09 18:19:36', '2020-11-09 18:23:12'),
(4, 2, 'ورامین', '2020-11-09 18:20:04', '2020-11-09 18:23:04'),
(5, 2, 'اسلامشهر', '2020-11-09 18:25:29', '2020-11-09 18:25:29'),
(6, 2, 'شهریار', '2020-11-09 18:25:39', '2020-11-09 18:25:39'),
(7, 2, 'قدس', '2020-11-09 18:25:45', '2020-11-09 18:25:45'),
(8, 2, 'ملارد', '2020-11-09 18:26:18', '2020-11-09 18:26:18'),
(9, 2, 'بهارستان', '2020-11-09 18:26:26', '2020-11-09 18:26:26'),
(10, 2, 'قرچک', '2020-11-09 18:26:32', '2020-11-09 18:26:32'),
(11, 2, 'شهریار', '2020-11-09 18:26:52', '2020-11-09 18:26:52'),
(12, 2, 'رباط کریم', '2020-11-09 18:27:06', '2020-11-09 18:27:06'),
(13, 2, 'پردیس', '2020-11-09 18:27:13', '2020-11-09 18:27:13'),
(14, 2, 'دماوند', '2020-11-09 18:27:29', '2020-11-09 18:27:29'),
(15, 2, 'فیروزکوه', '2020-11-09 18:27:36', '2020-11-09 18:27:36'),
(16, 2, 'شمیرانات', '2020-11-09 18:27:42', '2020-11-09 18:27:42'),
(17, 4, 'شاهین شهر', '2020-11-11 20:30:00', '2020-11-11 20:30:00'),
(19, 4, 'فولادشهر', '2020-11-11 20:30:00', '2020-11-11 20:30:00'),
(20, 4, 'فلاورجان', '2020-11-26 04:26:45', '2020-11-26 04:26:45');

-- --------------------------------------------------------

--
-- Table structure for table `city_prices`
--

CREATE TABLE `city_prices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `city_prices`
--

INSERT INTO `city_prices` (`id`, `city_id`, `price`, `created_at`, `updated_at`) VALUES
(44, 2, '1500', '2020-12-05 21:59:56', '2020-12-05 21:59:56'),
(45, 3, '1700', '2020-12-05 21:59:56', '2020-12-05 21:59:56'),
(46, 5, '3000', '2020-12-05 22:01:40', '2020-12-05 22:01:40'),
(47, 2, '2000', '2020-12-05 22:06:14', '2020-12-05 22:06:14'),
(48, 2, '5000', '2020-12-05 22:06:45', '2020-12-05 22:06:45'),
(49, 1, '15000', '2020-12-05 22:12:30', '2020-12-05 22:12:30'),
(50, 1, '200', '2020-12-06 04:01:19', '2020-12-06 04:01:19');

-- --------------------------------------------------------

--
-- Table structure for table `city_transport`
--

CREATE TABLE `city_transport` (
  `transport_id` bigint(20) UNSIGNED NOT NULL,
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `price_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `city_transport`
--

INSERT INTO `city_transport` (`transport_id`, `city_id`, `price_id`) VALUES
(18, 1, 50),
(19, 1, 49),
(17, 2, 44),
(18, 2, 48),
(17, 3, 45),
(18, 3, 45),
(17, 5, 46);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commentable_id` bigint(20) UNSIGNED NOT NULL,
  `commentable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `approved` int(11) NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `name`, `email`, `commentable_id`, `commentable_type`, `parent_id`, `approved`, `comment`, `created_at`, `updated_at`) VALUES
(2, 'میلاد', 'fasihi.67@gmail.com', 1, 'App\\Product', 0, 1, 'سلام قیمت این تخمه توی بازار ارزونتره', '2020-12-08 07:00:00', '2020-12-08 07:01:00'),
(3, 'احمد علوی', 'tehranseda.com@gmail.com', 1, 'App\\Product', 0, 1, 'سلام خسته نباشید شما برای ایرانشهر هم ارسال خواهید داشت؟', '2020-12-08 07:00:50', '2020-12-08 07:02:18'),
(5, 'میثم', 'fasihi.67@gmail.com', 1, 'App\\Product', 3, 1, 'سلام احمد عزیز بله امکان ارسال به مکان مورد نظر شما هست', '2020-12-08 08:05:27', '2020-12-08 08:05:37'),
(6, 'باربری', 'fasihi.67@gmail.com', 1, 'App\\Product', 5, 1, 'نه ما ایرانشهر برو نیستسم الکی بار نفروشیا', '2020-12-08 08:18:41', '2020-12-08 08:18:46');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `items` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `items`, `created_at`, `updated_at`) VALUES
(1, '[{\"text\":\"خشکبار\",\"href\":\"dried-goods\",\"target\":\"\",\"class\":\"\",\"children\":[{\"text\":\"تخمه\",\"href\":\"seed\",\"target\":\"\",\"class\":\"\",\"children\":[{\"text\":\"تخمه جابانی\",\"href\":\"Jabani-seeds\",\"target\":\"\",\"class\":\"\"},{\"text\":\"تخمه کدو\",\"href\":\"pumpkin-seeds\",\"target\":\"\",\"class\":\"\"}]},{\"text\":\"پسته\",\"href\":\"Pistachio\",\"target\":\"\",\"class\":\"\"},{\"text\":\"مغز بادام\",\"href\":\"Almond \",\"target\":\"\",\"class\":\"\"}]}]', NULL, '2020-11-27 12:27:52');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(9, '2014_10_12_000000_create_users_table', 1),
(10, '2019_08_19_000000_create_failed_jobs_table', 1),
(11, '2020_10_22_230002_create_active_codes_table', 1),
(13, '2020_10_25_095603_create_permission_role_table', 2),
(17, '2020_10_28_114842_create_stock_statuses_table', 3),
(18, '2020_10_28_120445_create_statuses_table', 4),
(21, '2020_10_28_123336_create_products_table', 5),
(22, '2020_10_28_174005_create_categories_table', 6),
(23, '2020_10_28_193238_create_category_product_table', 7),
(24, '2020_10_29_050835_create_comments_table', 8),
(26, '2020_10_31_104718_create_categories_table', 9),
(27, '2020_11_03_071659_create_attributes_table', 10),
(28, '2020_11_07_191433_create_user_groups_table', 11),
(29, '2020_11_07_191935_add_grou_id_to_users_table', 12),
(32, '2020_11_07_225103_create_sellers_table', 13),
(34, '2020_11_09_164811_create_cities_table', 14),
(41, '2020_11_10_122937_create_marketers_table', 15),
(46, '2020_11_11_004048_add_seller_info_to_users_table', 16),
(49, '2020_11_11_124708_create_product_users_table', 17),
(50, '2020_11_21_082312_add_city_id__and_state_id_to_users_table', 17),
(51, '2020_11_22_084212_add_image_to_products_table', 18),
(52, '2020_11_22_214215_create_product_images_table', 19),
(53, '2020_11_23_113730_add_credit_to_users_table', 20),
(54, '2020_11_25_215116_add_image_field_to_categories_table', 21),
(56, '2020_11_25_233629_create_settings_table', 22),
(57, '2020_11_27_071338_create_menus_table', 23),
(60, '2020_11_29_184559_create_banners_table', 24),
(61, '2020_11_30_182255_create_additional_pages_table', 25),
(62, '2020_11_30_215033_add_location_to_banners_table', 26),
(69, '2020_12_02_004359_create_supports_table', 27),
(79, '2020_12_04_182929_create_transports_table', 28);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `label`, `created_at`, `updated_at`) VALUES
(1, 'show-user', 'نمایش کاربران', '2020-10-25 21:09:26', '2020-10-25 21:09:26'),
(2, 'delete-user', 'حذف کاربران', '2020-10-25 21:09:35', '2020-10-25 21:09:35'),
(3, 'create-user', 'ایجاد کاربر', '2020-10-27 20:20:27', '2020-10-27 20:20:27'),
(4, 'edit-user', 'ویرایش کاربر', '2020-10-27 20:20:46', '2020-10-27 20:20:46'),
(5, 'show-permission', 'نمایش دسترسی', '2020-10-27 20:21:13', '2020-10-27 20:25:53'),
(6, 'edit-permission', 'ویرایش دسترسی', '2020-10-27 20:25:06', '2020-10-27 20:25:46'),
(7, 'delete-permission', 'حذف دسترسی', '2020-10-27 20:25:36', '2020-10-27 20:25:36'),
(8, 'create-permission', 'ایجاد دسترسی', '2020-10-27 20:26:23', '2020-10-27 20:26:23'),
(9, 'show-role', 'نمایش گروه بندی دسترسی', '2020-10-27 20:26:42', '2020-10-27 20:26:42'),
(10, 'create-role', 'ایجاد گروه بندی دسترسی', '2020-10-27 20:26:58', '2020-10-27 20:27:24'),
(11, 'delete-role', 'حذف گروه بندی دسترسی', '2020-10-27 20:27:16', '2020-10-27 20:27:16'),
(12, 'edit-role', 'ویرایش گروه بندی دسترسی', '2020-10-27 20:27:45', '2020-10-27 20:27:45'),
(13, 'show-product', 'نمایش محصولات', '2020-10-30 13:48:07', '2020-10-30 13:48:07'),
(14, 'edit-product', 'ویرایش محصول', '2020-10-30 13:48:29', '2020-10-30 13:48:29'),
(15, 'delete-product', 'حذف محصول', '2020-10-30 13:48:46', '2020-10-30 13:48:46'),
(16, 'create-product', 'ایجاد محصول', '2020-10-30 13:49:22', '2020-10-30 13:49:22'),
(17, 'show-comment', 'نمایش نظرات', '2020-10-30 13:49:46', '2020-10-30 13:49:46'),
(18, 'edit-comment', 'ویرایش نظر', '2020-10-30 13:50:15', '2020-10-30 13:50:15'),
(19, 'delete-comment', 'حذف نظر', '2020-10-30 13:50:37', '2020-10-30 13:50:37'),
(20, 'show-seller', 'نمایش فروشنده', '2020-11-08 07:28:25', '2020-11-08 07:28:25'),
(21, 'delete-seller', 'حذف فروشنده', '2020-11-08 07:28:47', '2020-11-08 07:28:47'),
(22, 'edit-seller', 'ویرایش فروشنده', '2020-11-08 07:29:10', '2020-11-08 07:29:10');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(9, 3),
(10, 3),
(11, 3),
(12, 3),
(5, 4),
(6, 4),
(7, 4),
(8, 4),
(13, 6),
(14, 6),
(15, 6),
(16, 6);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_user`
--

INSERT INTO `permission_user` (`permission_id`, `user_id`) VALUES
(3, 21),
(4, 21);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` bigint(20) NOT NULL DEFAULT '0',
  `inventory` bigint(20) NOT NULL DEFAULT '0',
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `user_id`, `name`, `slug`, `price`, `inventory`, `short_description`, `description`, `meta_title`, `meta_description`, `image`, `status_id`, `created_at`, `updated_at`) VALUES
(1, 13, 'تخمه محبوبی', 'mahboubi-sunflower', 32500, 1000, 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.', '<h2>متن تست</h2>\r\n\r\n<p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.</p>\r\n\r\n<h3>متن تست دوم</h3>\r\n\r\n<p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد <a href=\"\">گذشته</a> حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.</p>\r\n\r\n<p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.</p>', 'عنوان تخمه محبوبی', 'توضیحات گوگلی تخمه محبوبی', '/images/product1/ProdS2.jpg', 1, '2020-11-09 11:28:52', '2020-12-07 20:59:18'),
(2, 13, 'تخمه گلرخ', 'golrokh-sunflower', 31200, 1000, NULL, NULL, NULL, NULL, '', 1, '2020-11-10 13:26:51', '2020-11-10 13:26:51'),
(4, 13, 'محصول شماره ۴', 'product-4', 3200, 35000, NULL, NULL, NULL, NULL, '/images/hoy/Screen Shot 2020-11-22 at 19.07.14.png', 1, '2020-11-24 15:26:33', '2020-11-24 15:26:33'),
(5, 13, 'محصول شماره ۵', 'product-5', 1000, 1000, NULL, NULL, 'qwertyuik', NULL, '/images/hoy/Screen Shot 2020-11-22 at 19.07.14.png', 2, '2020-11-24 15:31:53', '2020-11-24 15:31:53');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `image`, `created_at`, `updated_at`) VALUES
(20, 4, '/images/hoy/1212.png', '2020-11-24 15:26:33', '2020-11-24 15:26:33'),
(33, 1, '/images/product1/ProdS7.jpg', '2020-12-07 20:59:18', '2020-12-07 20:59:18'),
(34, 1, '/images/product1/ProdS5.jpg', '2020-12-07 20:59:18', '2020-12-07 20:59:18'),
(35, 1, '/images/product1/ProdS.jpg', '2020-12-07 20:59:18', '2020-12-07 20:59:18');

-- --------------------------------------------------------

--
-- Table structure for table `product_users`
--

CREATE TABLE `product_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inventory` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `minimum` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `approved` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_users`
--

INSERT INTO `product_users` (`id`, `user_id`, `product_id`, `state_id`, `city_id`, `price`, `inventory`, `minimum`, `approved`, `created_at`, `updated_at`) VALUES
(1, 30, 1, 2, 2, '1170', '11000', '500', '1', '2020-11-21 05:07:15', '2020-11-21 05:07:15'),
(2, 28, 1, 2, 3, '1200', '5000', '500', '1', '2020-11-21 05:49:39', '2020-11-21 05:49:39'),
(3, 27, 1, 2, 4, '1850', '1000', '500', '0', '2020-11-21 16:37:01', '2020-11-21 16:37:01'),
(4, 13, 1, 2, 2, '38400', '11000', '500', '0', '2020-12-01 17:54:02', '2020-12-01 17:54:02'),
(5, 13, 2, 2, 3, '38400', '1000', '500', '0', '2020-12-01 17:54:15', '2020-12-01 17:54:15'),
(6, 27, 2, 4, 20, '1000', '1000', '200', '1', '2020-12-07 09:55:18', '2020-12-07 09:55:18');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `label`, `created_at`, `updated_at`) VALUES
(2, 'user-maneger', 'مدیریت کاربران', '2020-10-27 19:58:33', '2020-10-27 19:58:50'),
(3, 'roles-maneger', 'مدیریت گروه بندی دسترسی ها', '2020-10-27 20:28:45', '2020-10-27 20:28:45'),
(4, 'permissions-manager', 'مدیریت دسترسی ها', '2020-10-27 20:29:22', '2020-10-27 20:29:22'),
(5, 'comment-maneger', 'مدیریت نظرات', '2020-10-30 13:51:22', '2020-10-30 13:51:22'),
(6, 'product-maneger', 'مدیریت محصولات', '2020-10-30 13:52:02', '2020-10-30 13:52:02');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`) VALUES
(5, 21);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_owner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_phone2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `limit_admin` tinyint(4) NOT NULL DEFAULT '20',
  `review_guest` tinyint(4) NOT NULL DEFAULT '1',
  `store_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `maintenance` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `meta_title`, `meta_description`, `store_name`, `store_owner`, `store_phone`, `store_phone2`, `store_image`, `address`, `email`, `open_time`, `comment`, `limit_admin`, `review_guest`, `store_logo`, `store_icon`, `maintenance`, `created_at`, `updated_at`) VALUES
(1, 'مرجع تخصصی خرید فروش انواع خشکبار | فروشگاه سُهانوین', 'فروشگاه سُهانوین مرجع تخصصی خرید فروش انواع خشکبار, آجیل, حبوبات', 'فروشگاه سٌهانوین', 'مسعود احمدی', '02155657891', '09128897377', NULL, 'تهران خیابان صاحب جمع روبروی خیابان اردستانی کاروانسرای خانات پلاک ۱۱۱۱', NULL, '۹:۳۰ الی ۱۷', 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.', 20, 1, '/images/site/logo120.jpg', '/images/site/favicon.png', '1', NULL, '2020-12-08 12:14:03');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'تهران', '2020-11-09 15:42:37', '2020-11-09 15:42:37'),
(3, 'البرز', '2020-11-09 15:42:52', '2020-11-09 15:42:52'),
(4, 'اصفهان', '2020-11-09 15:43:07', '2020-11-09 15:43:07'),
(5, 'خراسان شمالی', '2020-11-09 15:43:27', '2020-11-09 15:43:27'),
(6, 'خراسان رضوی', '2020-11-09 15:43:39', '2020-11-09 15:43:39'),
(7, 'خراسان جنوبی', '2020-11-09 15:43:54', '2020-11-09 15:43:54'),
(8, 'گلستان', '2020-11-09 15:44:02', '2020-11-09 15:44:02'),
(9, 'گیلان', '2020-11-09 15:44:17', '2020-11-09 15:44:17'),
(10, 'مازندران', '2020-11-09 15:44:25', '2020-11-09 15:44:25'),
(11, 'ایلام', '2020-11-09 15:44:33', '2020-11-09 15:44:33'),
(12, 'یزد', '2020-11-09 15:44:38', '2020-11-09 15:44:38'),
(13, 'فارس', '2020-11-09 15:44:45', '2020-11-09 15:44:45'),
(14, 'خوزستان', '2020-11-09 15:44:53', '2020-11-09 15:44:53'),
(15, 'چهارمحال بختیاری', '2020-11-09 15:45:33', '2020-11-09 15:45:33'),
(16, 'آذربایجان شرقی', '2020-11-09 15:45:51', '2020-11-09 15:45:51'),
(17, 'آذربایجان غربی', '2020-11-09 15:46:02', '2020-11-09 15:46:02'),
(18, 'کهکیلویه و بویراحمد', '2020-11-09 15:46:24', '2020-11-09 15:46:24'),
(19, 'قم', '2020-11-09 15:46:34', '2020-11-09 15:46:34'),
(21, 'همدان', '2020-11-09 15:46:48', '2020-11-09 15:46:48'),
(22, 'قزوین', '2020-11-09 15:47:07', '2020-11-09 15:47:07'),
(23, 'زنجان', '2020-11-09 15:47:14', '2020-11-09 15:47:14');

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `name`) VALUES
(1, 'موجود'),
(2, 'ناموجود'),
(3, 'تماس برای خرید');

-- --------------------------------------------------------

--
-- Table structure for table `supports`
--

CREATE TABLE `supports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `supportable_id` bigint(20) UNSIGNED NOT NULL,
  `supportable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `part_id` tinyint(4) NOT NULL,
  `status_id` tinyint(4) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL,
  `priority` tinyint(4) DEFAULT NULL,
  `approved` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supports`
--

INSERT INTO `supports` (`id`, `supportable_id`, `supportable_type`, `subject`, `text`, `part_id`, `status_id`, `parent_id`, `priority`, `approved`, `created_at`, `updated_at`) VALUES
(5, 27, 'App\\User', 'واریز به حساب', 'سلام و احترام لطفا مبلغ درخواستی را در روز یکشنبه به حساب اینجانب واریز کنید', 1, 2, 0, 1, 0, '2020-12-02 12:05:33', '2020-12-02 21:20:10'),
(6, 13, 'App\\User', NULL, 'سلام جناب آقای صفا \r\nچشم درخواست شما انجام خواهد شد', 1, 0, 5, NULL, 1, '2020-12-02 14:45:21', '2020-12-02 14:45:21'),
(7, 27, 'App\\User', 'افزودن محصول جدید', 'سلام لطفا محصول زیر را اضافه کنید\r\nپسته احمد آقایی ۱۸ دانه', 2, 1, 0, 1, 0, '2020-12-02 14:59:23', '2020-12-02 21:20:28'),
(8, 13, 'App\\User', NULL, 'سلام آقای صفا\r\nدر اولین فرصت اضافه خواهد شد', 2, 0, 7, NULL, 1, '2020-12-02 15:00:59', '2020-12-02 15:00:59'),
(9, 27, 'App\\User', NULL, 'سپاس از شما', 2, 0, 7, NULL, 0, '2020-12-02 20:27:32', '2020-12-02 20:27:32'),
(10, 13, 'App\\User', NULL, 'باشه', 2, 0, 7, NULL, 1, '2020-12-02 20:28:57', '2020-12-02 20:28:57'),
(17, 27, 'App\\User', NULL, 'ققط ۱۲۰۰۰۰ تومنشو بزنید به حساب علی', 1, 0, 5, NULL, 0, '2020-12-02 21:05:23', '2020-12-02 21:05:23'),
(18, 13, 'App\\User', NULL, 'علی کیه؟', 1, 0, 5, NULL, 1, '2020-12-02 21:13:43', '2020-12-02 21:13:43'),
(19, 27, 'App\\User', NULL, 'همون که ایژ چیه رو داره', 1, 0, 5, NULL, 0, '2020-12-02 21:14:11', '2020-12-02 21:14:11'),
(21, 13, 'App\\User', NULL, 'باشه دادا', 1, 0, 5, NULL, 1, '2020-12-02 21:20:10', '2020-12-02 21:20:10'),
(22, 27, 'App\\User', 'واریز به حساب', 'سلام اینو پولو بده', 1, 2, 0, 1, 0, '2020-12-07 09:55:47', '2020-12-07 09:56:48'),
(23, 13, 'App\\User', NULL, 'ریدن برات', 1, 0, 22, NULL, 1, '2020-12-07 09:56:48', '2020-12-07 09:56:48');

-- --------------------------------------------------------

--
-- Table structure for table `transports`
--

CREATE TABLE `transports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transports`
--

INSERT INTO `transports` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(17, 'تیپاکس', 1, '2020-12-05 21:52:30', '2020-12-05 21:52:30'),
(18, 'باربری', 1, '2020-12-05 22:04:12', '2020-12-05 22:04:12'),
(19, 'پیک موتوری', 1, '2020-12-05 22:12:30', '2020-12-05 22:12:30');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0',
  `is_staff` tinyint(1) NOT NULL DEFAULT '0',
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `shop_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `state_id` int(11) NOT NULL DEFAULT '0',
  `city_id` int(11) NOT NULL DEFAULT '0',
  `shop_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_account_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_cart_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_shaba_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `is_superuser`, `is_staff`, `group_id`, `shop_name`, `address`, `state_id`, `city_id`, `shop_phone`, `bank_name`, `bank_account_number`, `bank_cart_number`, `bank_shaba_number`, `credit`, `approved`, `remember_token`, `created_at`, `updated_at`) VALUES
(13, 'میثم', '09127164611', 1, 0, 1, NULL, 'پیروزی', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-12-07 09:36:37'),
(21, 'سعید محمدی', '09127164123', 0, 0, 2, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '2020-11-08 07:37:27', '2020-11-08 07:37:27'),
(27, 'نادر صفا', '09127164618', 0, 0, 6, 'فروشگاه صفا', 'تهران پیروزی پرستار', 0, 0, '02133369240', 'ملت', '1870464695', '1212121298761212', 'IR062960000000100324200001', NULL, 1, NULL, '2020-11-10 21:32:08', '2020-11-10 21:32:08'),
(28, 'رضا فصیحی', '09126582660', 0, 0, 5, 'خشکبار فصیحی', 'تهران پیروزی پاسدار گمنام', 2, 4, '02133369240', 'ملت', '1870464695', '1212121298761212', 'IR062960000000100324200001', NULL, 1, NULL, '2020-11-18 07:21:40', '2020-11-29 05:04:16'),
(29, 'مسعود احمدی', '09128897377', 0, 0, 6, 'سهانوین', 'تهران خانات', 0, 0, '02133369240', 'ملت', '1870464695', '1212121298761212', 'IR062960000000100324200001', NULL, 1, NULL, '2020-11-18 08:59:37', '2020-11-18 08:59:37'),
(30, 'رسول توکلی', '09137991017', 0, 0, 6, 'توکلی', 'تهران غیاثی', 4, 19, '02133369240', 'ملی', '1870464695', '1212121298761212', 'IR062960000000100324200001', NULL, 1, NULL, '2020-11-18 12:30:25', '2020-11-29 05:06:02'),
(35, 'میثم فصیحی', '09127164619', 0, 0, 5, NULL, 'پیروزی میدان بروجردی', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '2020-12-04 14:24:50', '2020-12-04 14:44:48');

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'مدیر کل', '2020-11-06 20:30:00', '2020-11-06 20:30:00'),
(2, 'کارمند', '2020-11-06 20:30:00', '2020-11-06 20:30:00'),
(5, 'کاربر', '2020-11-06 20:30:00', '2020-11-06 20:30:00'),
(6, 'فروشنده', '2020-11-06 20:30:00', '2020-11-06 20:30:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `active_codes`
--
ALTER TABLE `active_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `additional_pages`
--
ALTER TABLE `additional_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_product`
--
ALTER TABLE `attribute_product`
  ADD PRIMARY KEY (`product_id`,`attribute_id`,`value_id`),
  ADD KEY `attribute_product_attribute_id_foreign` (`attribute_id`),
  ADD KEY `attribute_product_value_id_foreign` (`value_id`);

--
-- Indexes for table `attribute_values`
--
ALTER TABLE `attribute_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attribute_values_attribute_id_foreign` (`attribute_id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner_images`
--
ALTER TABLE `banner_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `banner_images_banner_id_foreign` (`banner_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_product`
--
ALTER TABLE `category_product`
  ADD PRIMARY KEY (`category_id`,`product_id`),
  ADD KEY `category_product_product_id_foreign` (`product_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities_state_id_foreign` (`state_id`);

--
-- Indexes for table `city_prices`
--
ALTER TABLE `city_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `city_prices_city_id_foreign` (`city_id`);

--
-- Indexes for table `city_transport`
--
ALTER TABLE `city_transport`
  ADD PRIMARY KEY (`transport_id`,`city_id`,`price_id`),
  ADD KEY `city_transport_city_id_foreign` (`city_id`),
  ADD KEY `city_transport_price_id_foreign` (`price_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`permission_id`,`user_id`),
  ADD KEY `permission_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_user_id_foreign` (`user_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_users`
--
ALTER TABLE `product_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_users_user_id_foreign` (`user_id`),
  ADD KEY `product_users_product_id_foreign` (`product_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supports`
--
ALTER TABLE `supports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transports`
--
ALTER TABLE `transports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`),
  ADD KEY `users_group_id_foreign` (`group_id`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `active_codes`
--
ALTER TABLE `active_codes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `additional_pages`
--
ALTER TABLE `additional_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `attribute_values`
--
ALTER TABLE `attribute_values`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `banner_images`
--
ALTER TABLE `banner_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `city_prices`
--
ALTER TABLE `city_prices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `product_users`
--
ALTER TABLE `product_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `supports`
--
ALTER TABLE `supports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `transports`
--
ALTER TABLE `transports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `attribute_product`
--
ALTER TABLE `attribute_product`
  ADD CONSTRAINT `attribute_product_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `attribute_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `attribute_product_value_id_foreign` FOREIGN KEY (`value_id`) REFERENCES `attribute_values` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attribute_values`
--
ALTER TABLE `attribute_values`
  ADD CONSTRAINT `attribute_values_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `banner_images`
--
ALTER TABLE `banner_images`
  ADD CONSTRAINT `banner_images_banner_id_foreign` FOREIGN KEY (`banner_id`) REFERENCES `banners` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `category_product`
--
ALTER TABLE `category_product`
  ADD CONSTRAINT `category_product_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `city_prices`
--
ALTER TABLE `city_prices`
  ADD CONSTRAINT `city_prices_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `city_transport`
--
ALTER TABLE `city_transport`
  ADD CONSTRAINT `city_transport_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `city_transport_price_id_foreign` FOREIGN KEY (`price_id`) REFERENCES `city_prices` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `city_transport_transport_id_foreign` FOREIGN KEY (`transport_id`) REFERENCES `transports` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `user_groups` (`id`) ON DELETE CASCADE;
