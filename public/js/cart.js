function updateTotal() {
    var sumtotal;
    var sum = 0;
    $(".product").each(function() {
      var price = $(this).data('price');
      var quantity = $('.quantityTxt', this).val();
      var subtotal = price*quantity;
      $('.productTotal', this).html(subtotal);
      $('.allPrice', this).html(thousands_separators(subtotal)+" تومان");
    });
    
    $('.productTotal').each(function() {
      sum += Number($(this).html());
    }); 
    
    $('#sum').html(thousands_separators(sum)+ " تومان");
  };
  updateTotal();
  function thousands_separators(num)
  {
    var num_parts = num.toString().split(".");
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return num_parts.join(".");
  } 
  