<?php

use App\Setting;
use Illuminate\Support\Facades\Route;

function activeMenu($route,$value = "active"){
    

    if (is_array($route)) {
        return in_array(Route::currentRouteName(),$route) ? $value : "";
    }
    return Route::currentRouteName() == $route ? $value : "";

}
function getSetting(){

    return Setting::find(1)->first();
}

function errorView($code){
     return view('catalog.errors.'.$code);
}

function getPrice($price = 0){
    if($price == 0 )return;
    return number_format($price).__('app.currency');
}

function getThumbImage($path)
    {

        $str_to_insert = '/thumbs';
        $oldstr = $path;
        $pos = strrpos($path,"/");
        return substr_replace($oldstr, $str_to_insert, $pos, 0);

    }