<?php
namespace App\Helpers\Cart;

use Illuminate\Database\Eloquent\Model;

class CartService
{
    protected $cart;

    public function __construct()
    {
        $this->cart = session()->get('cart') ?? collect([]);
    }

    public function put($value , $object = null){
        
    //    dd($value);
    if(!is_null($object) && $object instanceof Model){
        $value = array_merge([
            'id' => rand(1,9999999),
            'object_id' => $object->id,
            'object_type' => get_class($object),
        ],$value);
    }
    
        $this->cart->put($value['id'] , $value);
        session()->put('cart' , $this->cart);

        return $this;
    }
    public function get($key,$withRelationShip = true)
    {
        if(!is_null($key) && $key instanceof Model){
            $item = $this->cart->where('object_id',$key->id)->where('object_type',get_class($key))->first();
        }else{
            $item = $this->cart->firstWhere('id',$key);
        }
        return $withRelationShip ? $this->withRelationshipIfExist($item): $item ;
    }

    public function all(){

        $cart = $this->cart;
        $cart = $cart->map(function($item){
            return $this->withRelationshipIfExist($item);
        });
        return $cart;
    }

    public function has($key){
        
        if(!is_null($key) && $key instanceof Model){
            return ! is_null(
                $this->cart->where('object_id' ,$key->id)->where('object_type',get_class($key))->first()
            );
        }
        if(!is_null($key)){

            return $this->cart->firstWhere('id',$key);
        }

        return false;

    }

    public function count($object){
        
        if(is_null($object)) return 0;
        return $this->get($object)['quantity'];
    }

    public function update($object , $quantity){
        
        $item = collect($this->get($object,false));

        if(is_numeric($quantity)){
            $item = $item->merge([
                'quantity'=> $quantity + $item['quantity'],
            ]);
        }
        $this->put($item->toArray());
        
        return $this;

    }
    public function liveUpdate($object , $quantity){
        $item = collect($object,false);
        
        $cart = $this->withRelationshipIfExist(collect($object,false));
        
        $productUser = $cart->toArray()['productUser'];

        if(($quantity >= $productUser['minimum']) && $quantity <= $productUser['inventory']){
            
            if(is_numeric($quantity)){
                $item = $item->merge([
                    'quantity'=> $quantity,
                ]);
            }            
            $this->put($item->toArray());
            return $this;

        }
        return false;


    }
    public function delete($item){
       
        if($this->has($item)){

            $this->cart = $this->cart->filter(function ($value,$key) use ($item) {

            return $key != $item;
        });
        session()->put('cart' , $this->cart);

        return true;
        }
        return false;

    }

    public function changeQuantity($quantity){
        
    }

    private function withRelationshipIfExist($item){

        // dd($item);
        if(isset($item['object_id']) && isset($item['object_type'])){
            $class = $item['object_type'];
            
            
            $object = (new $class())->find( $item['object_id']);
            $item['productUser'] = $object;
            $item['seller'] = $object->user;
            $item['free_shipping'] = $object->user->moreVendorInfo->free_shipping_threshold;
            $item['near_shipping'] = $object->user->moreVendorInfo->near_shipping_price;
            $item['far_shipping'] = $object->user->moreVendorInfo->far_shipping_price;
            $item['product'] = $object->product;
            
            unset($item['object_id']);
            unset($item['object_type']);
            
            return $item;

        }
        return $item;
                
    }

}