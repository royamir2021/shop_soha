<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name','price','slug','meta_title','status_id','meta_description','image','short_description','description','inventory'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
    
    public function categories(){
        
        return $this->belongsToMany(Category::class);
    }
    public function productUsers(){
        
        return $this->hasMany(ProductUser::class);
    }

    public function productImages()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function productSeller(){
        
        return $this->hasMany(ProductUser::class,'product_id');
    }

    public function featured(){
        return $this->belongsToMany(Featured::class);
    }
 
    public function comments(){
        return $this->morphMany(Comment::class,'commentable');
    }

    public function attributes(){

        return $this->belongsToMany(Attribute::class)->withPivot('value_id');
        
    }

}
