<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class ActiveCode extends Model
{
    protected $fillable =['code','phone','expired_at'];

    public $timestamps = false;

    public function scopeGenerateCode($query,$phone){
        
        if ($code = $this->getAliveCodeForPhone($phone)) {
            $code = $code->code;

        }else{
            do {
                $code = mt_rand(10000,99999);
            } while ($this->checkCodeisUnique($code));
            
            ActiveCode::create([
                'code'       => $code,
                'phone'      => $phone,
                'expired_at' => now()->addMinute(5),
            ]);
        }
        return $code;

    }

    private function getAliveCodeForPhone($phone){

        return ActiveCode::wherePhone($phone)->where('expired_at','>',now())->first();
    }

    private function checkCodeisUnique($code){

        return !! ActiveCode::whereCode($code)->first();
    }
}
