<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['name','email','comment','commentable_id','commentable_type','parent_id','approved'];

    public function commentable(){
        return $this->morphTo();
    }
    public function child(){

        return $this->hasMany(Comment::class,'parent_id','id');
        
    }
}
