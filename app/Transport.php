<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transport extends Model
{
    protected $fillable = ['name','status'];
   

    public function cities(){

        return $this->belongsToMany(City::class)->withPivot('price_id');
        
    }
    
}
