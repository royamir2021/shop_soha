<?php

namespace App\Http\Controllers\Catalog\Auth;

use App\ActiveCode;
use App\Http\Controllers\Controller;
use App\Notifications\TwoFactorLogin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    
    public function showLogin(){
        
        session(['redirect' => url()->previous()]);

        return view('catalog.auth.login');
    }
    public function login(Request $request){
        
        $data = $request->validate([
            'phone' => ['required','regex:/^09(1[0-9]|3[1-9]|2[1-9])-?[0-9]{3}-?[0-9]{4}$/']
        ]);
        
        $user = User::firstOrCreate(['phone' => $data['phone'],'group_id'=>5]);
        
        $code = ActiveCode::generateCode($data['phone']);

        $user->notify(new TwoFactorLogin($code,$data['phone']));

        $request->session()->flash('token',[
            'code'      => $code,
            'user_name' => $user->name,
            'redirect'  => session()->get('redirect')]);
        
        return redirect(route('auth.token'));

        // if ($user = $this->checkIsUserByPhone($data['phone'])) {
        //     if(!$this->checkCustomer($user)){
        //         alert()->error('با این شماره به عنوان فروشنده ثبت نام شده','ورود ناموفقیت')->persistent();
        //         return back();
        //     }
        //     $code = ActiveCode::generateCode($data['phone']);

        //     $user->notify(new TwoFactorLogin($code,$data['phone']));
        // }else{
            
        //     $user = User::create([
        //         'phone' => $data['phone'],
        //         'group_id' => 5
        //     ]);
            
        //     $code = ActiveCode::generateCode($data['phone']);

        //     $user->notify(new TwoFactorLogin($code,$data['phone']));

        // }
        // if(session()->has('redirect')){
        //     $request->session()->flash('token',['code'=> $code,'redirect'=>session()->get('redirect')]);
        // }else{
        //     $request->session()->flash('token',['code'=> $code]);
        // }
        

        // return redirect(route('auth.token'));

    }

    // private function checkIsUserByPhone($phone){

    //     $user = User::wherePhone($phone)->first();
    //     if ($user) {
    //         return $user;
    //     }
    //     return false;
    // }
    // private function checkCustomer($user){
    //     return $user->group_id == 5;
    // }
    
    

}
