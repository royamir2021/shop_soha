<?php

namespace App\Http\Controllers\Catalog\Auth;

use App\ActiveCode;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class TokenController extends Controller
{
    public function getToken(Request $request){
        if(!$request->session()->has('token'))
            return redirect(route('login'));

        $request->session()->reflash();

        return view('catalog.auth.token');   
    }
    public function postToken(Request $request){
        
        $data = $request->validate([
            'token'=> ['required','numeric'],
            'name' => ['required','string']
        ]);
        $code = $this->veryfyCode($data['token']);
        
        $user = User::wherePhone($code['phone'])->first();
        
        //dd($user);
        
        if(!$user){
            alert()->error('کد وارد شده صحیح نمی باشد');
            return back();
        }
        $user->update($data);
        auth()->loginUsingId($user->id); 

        ActiveCode::wherePhone($code['phone'])->delete();
        $session = $request->session()->get('token');
        if(isset($session['redirect'])) return redirect($session['redirect']);

        return redirect('/');
        
        
    }

    private function veryfyCode($code){
        $code = ActiveCode::whereCode($code)->where('expired_at','>',now())->first();
        return $code;
    }
}
