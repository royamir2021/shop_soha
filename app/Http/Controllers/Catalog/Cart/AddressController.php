<?php

namespace App\Http\Controllers\Catalog\Cart;

use App\Address;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AddressController extends Controller
{
    
    /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $this->seo()
        ->setTitle('ویرایش آدرس | سهانوین');

        return view('catalog.cart.address',compact('user'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $user = auth()->user();
        
        $data = $request->validate([
            'name'      => 'required',
            'phone'     => ['required','regex:/^09(1[0-9]|3[1-9]|2[1-9])-?[0-9]{3}-?[0-9]{4}$/',Rule::unique('addresses')->ignore($user->id)],
            'state_id'  => 'required',
            'city_id'   => 'required',
            'postcode'  => 'required|digits:10|numeric',
            'address'   => 'required',
            'approved'  => 'required'
            
        ]);

        $user->update(['name'=>$data['name']]);

        $address = $user->addresses()->create($data);

        $user->addresses()->where('id','<>',$address->id)->update(['approved' => 0]);

        alert()->success('آدرس جدید با موفقیت ثبت شد');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function show(Address $address)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function edit(Address $address)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Address $address)
    {
        $address->update(['approved' => 1]);
        $address->where('id','<>',$address->id)->update(['approved' => 0]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function destroy(Address $address)
    {
        //
    }
}
