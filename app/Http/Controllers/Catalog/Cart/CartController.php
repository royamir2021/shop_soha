<?php

namespace App\Http\Controllers\Catalog\Cart;

use App\Helpers\Cart\Cart;
use App\Http\Controllers\Controller;
use App\ProductUser;
use App\Setting;
use Illuminate\Http\Request;

class CartController extends Controller
{
    
    public function index(){

        $carts = Cart::all();
        // dd($carts); 
        $this->seo()
        ->setTitle('سبد خرید | سهانوین');
        if($carts->count()<1) return redirect('/');

        return view('catalog.cart.cart',compact('carts'));
    }

    public function add(ProductUser $productUser,Request $request){
        
        $validate = $request->validate([
            'quantity'   => ['required',function($attribute, $value,$fail) use ($productUser){
                if($value < $productUser->minimum || $value > $productUser->inventory) return $fail('حداقل مقدار، نامعتبر است');
            }],
        ]);
        if(Cart::has($productUser)) { 
            if((Cart::count($productUser) + $validate['quantity']) < $productUser->inventory)
                
            Cart::update($productUser,$validate['quantity']);
        }else{
            Cart::put([
                'quantity'    => $validate['quantity']
            ],$productUser);
        }
    
        return redirect('/cart');   
    }

    public function delete(Request $request)
    {
        Cart::delete($request->id);

        return true;
    }

    public function update(Request $request){
                
        $data = $request->validate([
            'quantity' => 'required',
            'id'       => 'required',
         ]);
        
         if($cart = Cart::has($data['id'])){
            
            Cart::liveUpdate($cart,$data['quantity']);
            
            return response([
                'status'=>'success',
                'quantity' => Cart::get($data['id'])['quantity']
            ]);

         }
         return response(['status'=>'error'],404);

        ;
    }

    public function cartGetApi(){
        
        $carts = Cart::all();
        $allCarts =[];
        
        foreach($carts as $cart){
            $allCarts[] = [
                'id'        => $cart['id'],
                'name'      => $cart['product']->name,
                'image'     => getThumbImage($cart['product']->image),
                'slug'      => $cart['product']->slug,
                'quantity'  => $cart['quantity'],
                'seller'    => $cart['seller']->shop_name,
                'city'      => $cart['productUser']->city->name,
                'state'     => $cart['productUser']->state->name,
                'minimum'   => $cart['productUser']->minimum,
                'price'     => $cart['productUser']->price,
                'inventory' => $cart['productUser']->inventory,
            ];
        }
        return response($allCarts);
    }

}