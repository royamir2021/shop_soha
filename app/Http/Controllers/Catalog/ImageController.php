<?php

namespace App\Http\Controllers\Catalog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Intervention\Image\ImageManager;
use Intervention\Image\Exception\NotWritableException;

class ImageController extends Controller
{
    
    public function __construct( Request $input, ImageManager $image)
    {
        $this->input = $input;
        $this->image = $image;
    }
    
    public function thumbnail()
    {
        
        //آدرس تصویر اصلی
        $url = $this->input->get('url', null);
        $url = public_path($url);
        //پهنای جدید برای تصویر
        $width = $this->input->get('w', 100);
        
        //ارتفاع جدید برای تصویر
        $height = $this->input->get('h', null);
        
        try {
            //محل ذخیره سازی تصاویر تغییر سایز شده
            $thumbsPath = public_path('resizeimage/products');
            
            //چک کردن وجود و داشتن دسترسی نوشتن برای دارکتوری تصاویر بندانگشتی
            if (!is_writable($thumbsPath)) {
                throw new NotWritableException;
            }
            
            //در صورت عدم وجود آدرس تصویر، تصویری خالی با رنگ پس زمینه تعریف شده در خورجی نمایش داده شود
            if (is_null($url)) {
                
                if (is_null($height)) {
                    $height = $width;
                }
                //آدرس فایل جدید
                $filePath = public_path('resizeimage/products/' . sha1($width . $height));
                
                //در صورت وجود فایل آن را نمایش بدهد و در غیر انصورت تصویری جدید ایجاد شود
                if (!file_exists($filePath)) {
                    $image = $this->image->canvas($width, $height, '#dddddd');
                    return $image->save($filePath)->response();
                }
            } else {
                //آدرس ذخیره سازی تصویر جدید
                $filePath = public_path('resizeimage/products/' . sha1($url . $width . $height));
                
                //dd($filePath);
                //در صورت عدم وجود تصویر :
                if (!file_exists($filePath)) {

                    //تصویری جدید به وسیله آدرس تصویر اصلی ساخته شود.
                    $image = $this->image->make($url);

                    $newHeight = $height;

                    //در صورت عدم وجود ارتفاع جدید، ارتفاع به نسب پهانی جدید ایجاد شود
                    if (is_null($height)) {
                        $scale = $image->width() / $image->height();
                        $newHeight = $width / $scale;
                    }

                    //تغییر سایز تصویر ایجاد شده
                    $image->resize($width, $newHeight);

                    //بازگشت دادن تصویر ساخته و ذخیره شده
                    return $image->save($filePath)->response();
                }
            }

            //در صورت وجود تصویر از قبل همان را نمایش بده
            return $this->image->make($filePath)->response();
        } catch (\Exception $ex) {
            dd('fuc');
            return $ex->getMessage();
        }
    }
}
