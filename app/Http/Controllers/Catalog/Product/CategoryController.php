<?php

namespace App\Http\Controllers\Catalog\Product;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    
    
    public function index(){
        $categries = Category::all();
        return view('catalog.products.allCategory',compact('categries'));

    }

    public function single($categories)
    { 
        $categories = explode('/',$categories);
        $category = Category::whereSlug(array_pop($categories))->first();
        
        $this->seo()
        ->setCanonical(url($category->slug))
        ->setTitle($category->meta_title)
        ->setDescription($category->meta_description);
        
        return view('catalog.products.singleCategory',compact('category'));
        
    }

    private function getParentSlug($category,$slugs = []){
       
        $parent = $category->parent;  
        dd($parent);
        if($parent->parent_id == 0){
            $slugs[] = $parent->slug;
            $reverse_can = array_reverse($slugs);
            $canonical = implode('/', $reverse_can);
            return  $canonical;
        }else{
            $slugs[] = $parent->slug;
           return $this->getParentSlug($parent,$slugs);
        }
        
    }

   
}
