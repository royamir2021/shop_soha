<?php

namespace App\Http\Controllers\Catalog\Product;

use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index($slug = null){

        $product = Product::whereSlug($slug)->first();
        if(!$product) return abort(404);

        // SEO Manager
        $this->seo()
        ->setCanonical(url($product->slug))
        ->setTitle($product->meta_title)
        ->setDescription($product->meta_description);

        $sellerActive = $product->productSeller()
            ->get()
            ->filter(function($item){ 
                return $item->user->approved == 1;
        });
        
        $informations = [
            'max_price' => $sellerActive->count() > 1 ?$sellerActive->max('price'):0,
            'min_price' => $sellerActive->min('price'),
            'comments'  =>$product->comments()->where('approved',1)->where('parent_id',0)->get(),
        ];
        

        return view('catalog.products.single',compact('product','informations'));

    }
}
