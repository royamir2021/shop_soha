<?php

namespace App\Http\Controllers\Catalog\Page;

use App\AdditionalPage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdditionalPageController extends Controller
{
    
    public function show($slug){
        
        $information = AdditionalPage::whereSlug($slug)->first();

        return view('catalog.additional_page.index',compact('information'));
    }
}
