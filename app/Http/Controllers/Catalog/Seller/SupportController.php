<?php

namespace App\Http\Controllers\Catalog\Seller;

use App\Http\Controllers\Controller;
use App\Support;
use Illuminate\Http\Request;

class SupportController extends Controller
{
    /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->middleware('auth.seller');
    }
    public function index()
    {
       
        $this->seo()->setTitle('پشتیبانی | پنل فروشنده');
        
        $tikets = auth()
        ->user()
        ->supports()
        ->where('parent_id',0)
        ->latest()
        ->get();

        return  view('catalog.seller.allSupport',compact('tikets'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->seo()->setTitle('ثبت درخواست پشتیبانی | پنل فروشنده');
        return  view('catalog.seller.createSupport');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'supportable_id'   => 'required|numeric',
            'supportable_type' => 'required',
            'parent_id'        => 'required|numeric',
            'subject'          => 'required|string',
            'part_id'          => 'required|numeric',
            'priority'         => 'required|numeric',
            'text'             => 'required|string',    
        ]);
        
        Support::create($data);
        alert()->success('در روزهای غیر تعطیل کمتر از ۴ ساعت و در روز های تعطیل حداکثر ۸ ساعت بعد به درخواست شما پاسخ خواهیم داد','درخواست شما با موفقیت ارسال شد')->persistent();        

        return redirect(route('support.index'));
    }

    public function edit(Support $support)
    {
        
        $this->seo()->setTitle('مشاهده درخواست پشتیبانی | پنل فروشنده');

        return  view('catalog.seller.answerSupport',compact('support'));

    }

    public function answer(Request $request)
    {
        $data = $request->validate([
            'supportable_id'   => 'required|numeric',
            'supportable_type' => 'required',
            'parent_id'        => 'required|numeric',
            'text'             => 'required|string', 
            'part_id'          => 'required|numeric',   
        ]);
        Support::find($data['parent_id'])->update(['status_id'=>0]);
        Support::create($data);
        alert()->success('در روزهای غیر تعطیل کمتر از ۴ ساعت و در روز های تعطیل حداکثر ۸ ساعت بعد به درخواست شما پاسخ خواهیم داد','درخواست شما با موفقیت ارسال شد')->persistent();        return redirect(route('support.index'));
        return back();
    }
}
