<?php

namespace App\Http\Controllers\Catalog\Seller;

use App\Brand;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductUser;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->middleware('auth.seller');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->seo()->setTitle('محصولات | پنل فروشنده');
        $user = auth()->user();

        return  view('catalog.seller.products',compact('user'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->seo()->setTitle('ایجاد محصول جدید | پنل فروشنده');
        return view('catalog.seller.addProduct');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $validData = $request->validate([
            'product_id' => 'required',
            'price'      => 'required',
            'state_id'   => 'required',
            'city_id'    => 'required',
            'brand'      => 'required',
            'inventory'  => 'required',
            'user_id'    => 'required',
            'minimum'    => 'required',
            
        ]);
        $brand = Brand::firstOrCreate(['brand'=> $validData['brand']]);

        $product = Product::find($validData['product_id']);
        
        $productUser = $product->productUsers()->create($validData);

        $productUser->update(['brand_id' => $brand->id]);

        alert()->success('محصول با موفقیت اضافه شد');
        return redirect(route('seller-products.index'));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->seo()->setTitle('ویرایش محصول | پنل فروشنده');
        $productUser = ProductUser::findOrfail($id);

        return view('catalog.seller.editProduct', compact('productUser'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $productUser = ProductUser::findOrfail($id);
        
        $validData = $request->validate([
            'product_id' => 'required',
            'price'      => 'required',
            'state_id'   => 'required',
            'city_id'    => 'required',
            'brand'      => 'required',
            'inventory'  => 'required',
            'user_id'    => 'required',
            'minimum'    => 'required',
            
        ]);

        $productUser->update($validData);

        $brand = Brand::firstOrCreate(['brand'=> $validData['brand']]);
        
        $productUser->update(['brand_id' => $brand->id]);
                    
        
        alert()->success('محصول با موفقیت ویرایش شد');
        return redirect(route('seller-products.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $productUser = ProductUser::find($id);

        $productUser->delete($id);
        alert()->success('حذف با موفقیت اضافه شد');
        return back();

    }

}
