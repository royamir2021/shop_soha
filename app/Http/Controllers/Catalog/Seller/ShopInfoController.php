<?php

namespace App\Http\Controllers\Catalog\Seller;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ShopInfoController extends Controller
{
    public function index(){
        
        $this->seo()->setTitle('ویراش فروشگاه | پنل فروشنده');
        $user = auth()->user();
        return view('catalog.seller.shop_info',compact('user'));
    }
    public function update(Request $request){
        $user = User::find($request->user_id);
        //dd($user);
        $userData = $request->validate([
            'name'                    => 'required',
            'phone'                   => ['required','regex:/^09(1[0-9]|3[1-9]|2[1-9])-?[0-9]{3}-?[0-9]{4}$/',Rule::unique('users')->ignore($user->id)],
            'shop_name'               => 'required',
            'shop_phone'              => 'required',
            'approved'                => 'required',
            'state_id'                => 'required',
            'city_id'                 => 'required',
            'address'                 => 'required',
            'bank_name'               => 'required',
            'bank_account_number'     => 'required',
            'bank_cart_number'        => 'required',
            'bank_shaba_number'       => 'required',
        ]);
        $moreData = $request->validate([
            'free_shipping_threshold' => 'required',
            'near_shipping_price'     => 'required',
            'far_shipping_price'      => 'required',
            'description'             => 'required',
            'shipping_terms'          => 'required',
            'return_terms'            => 'required',
        ]);
        $user->update($userData);
        $user->moreVendorInfo()->update($moreData);
        alert()->success('ویرایش با موفقیت انجام شد');
        return back();
     
     
    }
    public function activeVendor(){
        if (auth()->user()->bank_account_number) {
            return response('success');
        }
        return response('error');

    }
}
