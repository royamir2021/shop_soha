<?php

namespace App\Http\Controllers\Catalog\Common;

use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function send(Request $request){
        
        $data = $request->validate([
            'name'             => 'required|min:3',
            'email'            => 'email|required',
            'comment'          => 'required|min:25',
            'commentable_id'   => 'required|numeric',
            'commentable_type' => 'required',
            'parent_id'        => 'required',

        ]);
        Comment::create($data);
        
        alert()->success('نظر شما با موفقیت ثبت شد');
        return back();
    }

}
