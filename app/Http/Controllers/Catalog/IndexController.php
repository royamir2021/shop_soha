<?php

namespace App\Http\Controllers\Catalog;

use App\Banner;
use App\Featured;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){
        
        $this->seo()
        ->setTitle(getSetting()->meta_title)
        ->setDescription(getSetting()->meta_description)
        ->addImages(url(getSetting()->store_logo))
        ->setCanonical(url('/'));
        auth()->loginUsingId(13);
        //auth()->logout();

        $information = array(
            'slider'              => Banner::whereLocation('home-banner')->first(),
            'topBanner1'          => Banner::whereLocation('home-top-banner-1')->first(),
            'topBanner2'          => Banner::whereLocation('home-top-banner-2')->first(),
            'topProductSlider'    => Featured::whereStatus(1)->whereLocation('1')->first(),
            'centerProductSlider' => Featured::whereStatus(1)->whereLocation('2')->first(),
            'bottomProductSlider' => Featured::whereStatus(1)->whereLocation('3')->first(),
        );

        return view('catalog.index',compact('information'));
    }
}
