<?php

namespace App\Http\Controllers\Admin\Product;

use App\Attribute;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        
        $attributes = Attribute::whereStatus(0)->get();

        return view('admin.attribute.index',compact('attributes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function show(Attribute $attribute)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function edit(Attribute $attribute)
    {
        return view('admin.attribute.edit',compact('attribute'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attribute $attribute)
    {
        $data = $request->validate([
            'name'     => 'required',
        ]);

        
        $newAttr = Attribute::firstOrCreate([
            'name'   => $data['name'],
            'status' => 1
            ]);
        //dd($attribute->values);
        
        DB::table('attribute_product')->where('attribute_id',$attribute->id)->update([
            'attribute_id' =>$newAttr->id
        ]);
        
        DB::table('attribute_values')->where('attribute_id',$attribute->id)->update([
            'attribute_id' =>$newAttr->id
        ]);
        
        $attribute->delete();
        
        alert()->success('ویرایش ویژگی با موفقیت انجام شد');
        return redirect(route('admin.attribute.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attribute $attribute)
    {
        //
    }


    public function getValues(Request $request ){
        
        $data = $request->validate([
            'name' =>'required',
        ]);
        $attribute = Attribute::whereName($data['name'])->first();
        if(is_null($attribute)) return response(['data'=>[]]);
        return response($attribute->values->pluck('value'));
    }
}
