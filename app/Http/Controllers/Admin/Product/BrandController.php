<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Brand;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class BrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:show-brand')->only('index');
        $this->middleware('can:create-brand')->only(['create','store']);
        $this->middleware('can:edit-brand')->only(['update','edit']);
        $this->middleware('can:delete-brand')->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $brands = Brand::query();
        
        if ($keyword = $request->search) {
            
            $brands = Brand::where('name','LIKE',"%{$keyword}%");  
        }
        
        $brands = $brands->latest()->paginate(20);
        return view('admin.brand.all',compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'brand'         => ['required','unique:brands'],
            'english_brand' => ['required','unique:brands']
        ]);
        
        Brand::create($data);
        alert()->success('برند مورد نظر با موفقیت ایجاد شد');
        return redirect(route('admin.brand.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        return view('admin.brand.edit',compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        $data = $request->validate([
            'brand'         => ['required',Rule::unique('brands')->ignore($brand->id)],
            'english_brand' => ['required',Rule::unique('brands')->ignore($brand->id)]
        ]);
        
        $brand->update($data);
        alert()->success('برند مورد نظر با موفقیت ویرایش شد');
        return redirect(route('admin.brand.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        $brand->delete();
        alert()->success('برند مورد نظر با موفقیت حذف شد');
        return back();
    }
}
