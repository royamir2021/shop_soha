<?php

namespace App\Http\Controllers\Admin\System;

use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('can:show-setting')->only('index');
        $this->middleware('can:edit-setting')->only('update');
    }
    
    public function index(){
        $info = Setting::find(1)->first();
        return view('admin.setting.index',compact('info'));
    }
    public function update(Request $request, Setting $setting){
        $data = $request->validate([

            'store_name'       => 'required',
            'store_owner'      => 'required',
            'store_phone'      => 'required',
            'store_phone2'     => 'nullable',
            'meta_title'       => 'required',
            'meta_description' => 'required',
            'store_logo'       => 'required',
            'address'          => 'required',
            'store_icon'       => 'nullable',
            'maintenance'      => 'required',
            'review_guest'     => 'required',
            'limit_admin'      => 'required',
            'open_time'        => 'required',
            'comment'          => 'nullable',
        ]);
        
        $setting->update($data);
        alert()->success('تنظیمات بروز رسانی شد');
        return back();
    }
}
