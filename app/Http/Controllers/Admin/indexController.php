<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class indexController extends Controller
{
    public function index(){
        
        $customer = User::where('group_id',5)->get();
        $seller = User::where('group_id',6)->get();
        
        $information = [
            'customer' => $customer,
            'seller'   => $seller
        ];
        
        return view('admin.index',compact('information'));
    }
}
