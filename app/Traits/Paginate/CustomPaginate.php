<?php
namespace App\Traits\Paginate;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

trait CustomPaginate{

    public function paginate($items,$perPage = 50,$path = "/"){
        $options = ['path'=>$path];
        $page = Paginator::resolveCurrentPage();
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
        
    }
}