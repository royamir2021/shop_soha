<?php
namespace App\Traits\Images;

/**
 * 
 */
trait Thumb
{
    public function  getThumb($path)
    {
        $str_to_insert = '/thumbs';
        $oldstr = $path;
        $pos = strrpos($path,"/");
        return substr_replace($oldstr, $str_to_insert, $pos, 0);

    
    }
}
