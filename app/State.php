<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $fillable = ['name'];
    
    public function addresses(){
        return $this->belongsToMany(Address::class);
        
    }
}
