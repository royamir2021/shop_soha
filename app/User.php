<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone', 'group_id','shop_name','state_id','city_id','address','shop_phone','bank_name','bank_account_number','bank_cart_number','vendor','bank_shaba_number','approved','credit'
    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
   
    // public function activeCodes(){

    //     return $this->hasMany(ActiveCode::class);
        
    // }
    // public function seller(){
    //     return $this->hasOne(Seller::class);
    // }

    public function productUsers(){
        return $this->hasMany(ProductUser::class);
    }

    public function roles(){
        return $this->belongsToMany(Role::class);
    }
    
    public function permissions(){
        return $this->belongsToMany(Permission::class);
    }
    public function products(){
        return $this->hasMany(Product::class);
    }
    
    public function supports(){
        return $this->morphMany(Support::class,'supportable');
    }

    public function addresses(){
        return $this->hasMany(address::class)->with(['city','state']);
        
    }
    public function moreVendorInfo(){
        
        return $this->hasOne(MoreVendorInfo::class);
    }
    

    public function groupId(){
        
        return $this->group_id;
    }

    public function hasRole($permission){
        
        return !! $permission->roles->intersect($this->roles)->all();
        
    }

    public function hasPermission($permission){
        return $this->permissions->contains('name',$permission->name);
    }




}
