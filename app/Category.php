<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    protected $fillable = [
        'name','slug','meta_title','image','meta_description','description','parent_id','top','sort_order','status'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function allSubCategory()
    {
        $collection = collect();
        foreach ($this->children as $child) {
            $collection->add($child);
            $collection = $collection->merge($child->allSubCategory());
        }
        return $collection;
    }

    public function allProducts($categories){
        
        return DB::table('category_product AS cp')
            ->join('products', 'cp.product_id', '=', 'products.id')
            ->select('products.id')
            ->whereIn('cp.category_id', $categories)
            ->get()
            ->unique();
        
    }
    public function children() {

        return $this->hasMany(Self::class,'parent_id','id');
        
    }
    public function parent(){

        return $this->belongsTo(Self::class,'parent_id');
        
    }

}


    

