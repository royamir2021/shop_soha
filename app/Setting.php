<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['meta_title','meta_description','store_name','store_owner','store_phone','store_phone2','store_image','address','email','open_time','comment','limit_admin','review_guest','store_logo','store_icon','maintenance',
    ];
}
