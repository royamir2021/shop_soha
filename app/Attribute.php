<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    
    protected $fillable = ['name','status'];

    public function products(){

        return $this->belongsToMany(Product::class);
        
    }
    public function values(){

        return $this->hasMany(AttributeValue::class);
        
    }
}
