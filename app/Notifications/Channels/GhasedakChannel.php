<?php 

namespace App\Notifications\Channels;

use Illuminate\Notifications\Notification;
use Ghasedak\Exceptions\ApiException;
use Ghasedak\Exceptions\HttpException;



class GhasedakChannel
{
	
	public function send($notifiable,Notification $notification){

			
		$data = $notification->toOneParamTemplate($notifiable);
		
		$receptor   = $data['phone'];
		$param1     = $data['code'];
		$template   = $data['template'];
		
		$ghasedakApiKey = config('services.ghasedak.key');
		
		$api = new \Ghasedak\GhasedakApi($ghasedakApiKey);
						
		$api->Verify($receptor, 1, $template, $param1); 

		
	}
	
}