<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdditionalPage extends Model
{
    protected $fillable = ['name','slug','status','meta_title','meta_description','description'];
}
