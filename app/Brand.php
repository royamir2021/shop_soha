<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable = ['brand','english_brand'];

    public function ProductUsers(){
        
        return $this->hasMany(ProductUser::class);
    }
}
