<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Comments extends Component
{
    public $answer;
    public $comments;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($comments,$answer = null)
    {
        $this->comments = $comments;
        $this->answer = $answer;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.comments');
    }
}
