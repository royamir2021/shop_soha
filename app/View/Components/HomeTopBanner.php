<?php

namespace App\View\Components;

use Illuminate\View\Component;

class HomeTopBanner extends Component
{
    
    public $bannerInfo;
    
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($bannerInfo = null)
    {
        $this->bannerInfo = $bannerInfo;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.home-top-banner');
    }
}
