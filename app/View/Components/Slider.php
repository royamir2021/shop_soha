<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Slider extends Component
{
    
    public $slider;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($slider = null)
    {
        $this->slider = $slider;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.slider');
    }
}
