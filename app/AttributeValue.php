<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeValue extends Model
{
    
    protected $fillable = ['value'];
    
    public function attribute(){
        
        $this->belongsTo(Attribute::class);
        
    }
}
