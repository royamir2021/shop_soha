<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerImage extends Model
{
    protected $fillable = ['images','url'];

    public function banner(){
        return $this->belongsTo(Banner::class);
    }
}
