<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MoreVendorInfo extends Model
{
    protected $fillable = ['description','shipping_terms','return_terms','free_shipping_threshold','near_shipping_price','far_shipping_price'];
    
    public function user(){
        return $this->BelongsTo(User::class);
        
    }
}
