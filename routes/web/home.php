<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IndexController@index');

// Cart
Route::namespace('Cart')->group(function(){
    Route::get('cart','CartController@index')->name('show.cart');
    Route::post('addCart/{productUser}','CartController@add')->name('add.cart');
    Route::get('getCart','CartController@cartGetApi')->name('get.cart');
    Route::post('removeCart','CartController@delete')->name('delete.cart');
    Route::post('updateCart','CartController@update');
   Route::resource('address', 'AddressController')->except(['show','edit','create']);
});

// Profile
Route::get('dashboard','Profile\DashboardController@index');
Route::get('logout','Profile\DashboardController@logout')->name('logout');

// login
Route::namespace('Auth')->group(function () {
    Route::get('login','LoginController@showLogin')->name('login');
    Route::Post('login','LoginController@login');
    Route::get('token','TokenController@getToken')->name('auth.token');
    Route::post('token','TokenController@postToken');
  //  Route::get('logout','LoginController@logout')->name('logout');

});

//Additional Page
Route::namespace('Page')->group(function(){
    route::get('info/{slug}','AdditionalPageController@show');
});

// Common
Route::namespace('Common')->group(function(){
    Route::post('comment','CommentController@send')->name('send.comment');
    Route::post('cities','CityController@getCitiesByStateId')->name('get.cities');
});
Route::get('thumb',[
    'uses' => 'ImageController@thumbnail',
    'as'   => 'thumb'
]);

// Seller
Route::namespace('Seller')->group(function () {
    Route::resource('seller-register','RegisterController')->only(['index','store']);
    Route::get('seller-dashboard','DashboardController@index')->name('dashboard.index');
    Route::resource('seller-products','ProductController');
    Route::resource('support','SupportController')->except(['show','destroy','update']);
    Route::post('seller-support/answer','SupportController@answer')->name('seller-support.answer');
    Route::get('shop-info','ShopInfoController@index')->name('shop-info');
    Route::post('shop-info','ShopInfoController@update');
    Route::get('active-vendor','ShopInfoController@activeVendor');

});

// Product Category
Route::namespace('Product')->group(function () {
    Route::get('category','CategoryController@index');
    Route::get('category/{categories}','CategoryController@single')->where('categories','^[a-zA-z0-9-_\/]+$');
    Route::get('{slug}','ProductController@index')->name('show.product');
});