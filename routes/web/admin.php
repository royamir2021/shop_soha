<?php

use Illuminate\Support\Facades\Route;

Route::get('/','indexController@index');

Route::resource('seller', 'Seller\SellerController')->except(['store','create','show']);


Route::resource('comment', 'CommentController')->only(['index','edit','destroy','update']);
Route::patch('approved/{comment}','CommentController@approved')->name('comment.approved');

Route::namespace('Product')->group(function () {  
    Route::resource('product', 'ProductController');
    Route::resource('category', 'CategoryController');
    Route::resource('brand', 'BrandController');
    Route::resource('attribute', 'AttributeController');
    Route::post('attribute/values','AttributeController@getValues')->name('attribute.value');
});

Route::namespace('Customer')->group(function () { 
    Route::resource('customer', 'CustomerController')->except(['show','create','store']); 
});

Route::namespace('Show')->group(function () {  
    Route::get('menu', 'MenuController@index')->name('menu.index');
    Route::Post('menu/update', 'MenuController@update')->name('menu.update');
    Route::resource('banner', 'BannerController');
    Route::resource('additionalPage', 'AdditionalPageController');
    Route::resource('featured', 'FeaturedController');

});
Route::namespace('Support')->group(function () {  
    Route::resource('support', 'SupportController');

});
Route::namespace('System')->group(function () {  
    Route::resource('state', 'StateController')->except('show');
    Route::resource('city', 'CityController');
    Route::resource('user', 'UserController');
    Route::resource('role', 'RoleController');
    Route::resource('transport', 'TransportController');
    Route::resource('permission', 'PermissionController');
    Route::get('setting','SettingController@index')->name('setting');
    Route::post('setting/{setting}','SettingController@update')->name('setting.update');

});