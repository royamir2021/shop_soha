import Vue from 'vue';

import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
    state:{
        products:[],
        user:[]
    },
    getters:{
        allProducts : state=> state.products,

        cartCount: state=>{
            return state.products.length},

        totalSum:state=>{
            var totalPayment = 0;
            state.products.forEach(function(products){
                totalPayment+=products.quantity*products.price;
            });
            return totalPayment;
        },
        isLoggedIn(state) {
            return state.user !== null;

        }
    },

    mutations:{
        receiveProduct(state,products){

            state.products = products
        },
        removeCart(state,index){
            const options = {
                headers : {
                    'X-CSRF-TOKEN' : document.head.querySelector('meta[name="csrf-token"]').content,
                    'Content-Type' : 'application/json'
                  }
            };
            axios.post('/removeCart', { id: index}, options)
            .then((response) => {
                if(response){
                  var itemId;
                  state.products.forEach(function(products,id){
                      if(index == products.id){
                          itemId = id;
                      }
                  });
                  state.products.splice(itemId,1)
                  if(state.products.length == 0){
                    window.location.href = '/';
                  }
                  
                  
                }
            });
        },
        updateCart(state,{value,index}){
            console.log(index);
        },
        setAuthUser(state, user) {
            state.user = user;
        }
    },

    actions: {
        receiveProduct ({ commit }) {
            axios
                .get('/getCart')
                .then(response => response.data)
                .then(items => {
                commit('receiveProduct', items)
            })
        }
    },

});

export default store;
