/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('sweetalert');


import vue from 'vue';
import store from './stores/CartStore';
import cart from './components/cartPage/Cart.vue';
import address from './components/cartPage/Address.vue';
import CartResult from './components/cartPage/CartResult.vue';
import TopCart from './components/cartPage/TopCart.vue';
// import VueRouter from 'vue-router';
// import axios from 'axios'

window.Vue = vue;

// vue.use(VueRouter);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);


Vue.component('myCart',cart);
Vue.component('myAddress',address);
Vue.component('cart-result',CartResult);
Vue.component('top-cart',TopCart);

// vue.use(axios);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
// const router = new VueRouter({
//     mode: 'history',
//     routes: [
//         { path: '/cart', component: require('./components/cartPage/Cart.vue').default },
//         { path: '/address', component: require('./components/cartPage/Address.vue').default },
//     ],
// });

const app = new Vue({
    el: '#app',
    store,
    // router
});
