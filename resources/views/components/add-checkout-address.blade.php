@push('child-script')
    <script>
      $('#meysam').click(function(){
        var bodyFormData = new FormData();
        bodyFormData.append('userName', 'Fred');

        const options = {
          
        };
        // axios.post({
        //   url:'/address',
        //   headers : {
        //     'X-CSRF-TOKEN' : document.head.querySelector('meta[name="csrf-token"]').content,
        //     'Content-Type' : 'multipart/form-data'
        //   },
        //   data: bodyFormData,
          
        // });
        axios({
          method: 'post',
          url: '/address',
          data: bodyFormData,
          headers: {'Content-Type': 'multipart/form-data' }
          })
          .then(function (response) {
              //handle success
              console.log(response);
          })
          .catch(function (response) {
              //handle error
              console.log(response);
          });
      });
    </script>
@endpush

<div class="modal fade" id="addAddress" tabindex="-1" role="dialog" aria-labelledby="addAddressLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">ثبت آدرس جدید</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="POST" id="adress_form" action="{{route('address.store')}}" class="harizonal-form">
            @csrf
            <input type="hidden" name="approved" value="1">
             <div class="form-group col-md-6 col-12 float-right">
              <label for="name">{{__('app.name')}}</label>
              <input type="text" name="name" id="name" value="{{$user->name?$user->name:old('name')}}" class="form-control @error('name') is-invalid @enderror ">
              @error('name')
                <span class="invalid-feedback" role="alert">{{$message}}</span>
              @enderror
            </div>
            <div class="form-group col-md-6 col-12 float-right">
              <label for="phone">{{__('app.mobile_number')}}</label>
              <input type="text" name="phone" id="phone" value="{{old('phone')}}" class="form-control @error('phone') is-invalid @enderror ">
              @error('phone')
                <span class="invalid-feedback" role="alert">{{$message}}</span>
              @enderror
            </div> 
            <x-state-city/>
            <div class="clear"></div>
            <div class="form-group">
              <label for="postcode">کد پستی</label>
              <input type="text" name="postcode" id="postcode" value="{{old('postcode')}}" class="form-control @error('postcode') is-invalid @enderror ">
              @error('postcode')
                <span class="invalid-feedback" role="alert">{{$message}}</span>
              @enderror
            </div>
            <div class="form-group col-12">
              <label for="address">{{__('app.address')}}</label>
              <textarea name="address" id="address" class="form-control @error('address') is-invalid @enderror" cols="30" rows="3">{{old('address')}}</textarea>
              @error('address')
                <span class="invalid-feedback" role="alert">{{$message}}</span>
              @enderror
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">لفو</button>
          <button type="submit" id="meysam" class="btn btn-success">ذخیره</button>
        </div>
      </div>
    </div>
  </div>