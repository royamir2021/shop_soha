@push('child-script')
<script>
   function AddressSelect (value){
        $('#editAdress').attr('action', `address/${value}`);
        $('#editAdress').submit();
    }
    
</script>
@endpush

<div class="modal fade" id="editAddress" tabindex="-1" role="dialog" aria-labelledby="editAddressLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">ثبت آدرس جدید</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            @foreach ($user->addresses as $address)
                <div class="list-address @if($address->approved == 1) selected @endif mt-2 text-right" onclick="AddressSelect({{$address->id}})">
                    <div class="box mr-5">
                        <p class="name">
                        <label>گیرنده : </label> <span class="name">{{$user->name}}</span>
                        </p> 
                        <p class="mobile d-inline-block ml-3">
                        <label>شماره موبایل : </label> <span class="name">{{$address->phone}}</span>
                        </p> | 
                        <p class="postcode d-inline-block mr-3">
                        <label>کد پستی : </span> <span class="name">{{$address->postcode}}</span>
                        </p> 
                        <p class="address">
                        استان: {{$address->state->name}} شهرستان {{$address->city->name}} <span>{{$address->address}} </span>
                        </p>
                    </div>
                    <form action="{{route('address.destroy',$address->id)}}" method="POST">@method('DELETE')
                        <button class="btn ">x</button>
                    </form>
                </div>
              @endforeach
              <form id="editAdress" action="" method="POST">@csrf @method('PATCH')</form>
        </div>
      </div>
    </div>
</div>