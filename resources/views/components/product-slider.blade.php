@push('child-script')
<script> 
    $("#productGroup_{{$productInfo->id}}").owlCarousel({
        items:4,
        loop:!0,
        margin:10,
        pagination:!0,
        autoplay:!0,
        dots:1,
        autoplayTimeout:3000,
        autoplayHoverPause:!0,
        responsiveClass:!0,
        responsive:{
            0:{
                items:1,
                nav:!1,
                stagePadding:0
            },
            400:{
                items:1,
                nav:!1,
                stagePadding:0
            },768:{
                items:4,
                nav:1,
                pagination:1,
                stagePadding:50
            }
        }
    });
</script>
  @endpush       
<div id="productGroup_{{$productInfo->id}}" dir="ltr" class="owl-carousel owl-theme product-group owl-loaded"  style="opacity: 1;">
    @foreach ($productInfo->products as $product)
        <div class="item">
            <a href="{{$product['slug']}}" class="d-block text-center pb-3">
                <img src="{{route('thumb',['url'=>$product['image'],'w'=>270,'h'=>270])}}" alt="" class="img-fluid">
                <h4 class="mt-3 mb-3">{{$product['name']}}</h4>
            </a>
            {{-- @if (!is_null($sellerActive->min('price')))
                <h5 class="price">{{getPrice($sellerActive->min('price'))}}</h5>
            @else
                <h5 class="unavailable">ناموجود</h5>
            @endif --}}
        </div>
    @endforeach
  </div>
