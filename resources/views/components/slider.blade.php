@section('link')
    <link rel="stylesheet" href="{{asset('plugins/owlcarousel/assets/owl.carousel.min.css')}}">
@endsection
@section('script')
    <script src="{{asset('plugins/owlcarousel/owl.carousel.min.js')}}"></script>
    <script>
        $("#slider_{{$slider->id}}").owlCarousel({
            // animateOut: 'slideOutDown',
            // animateIn: 'flipInX',
            items:1,
            // stagePadding:50,
            singleItem:!0,
            navigation:!0,
            navigationText:['<i class="fa fa-chevron-left fa-5x"></i>','<i class="fa fa-chevron-right fa-5x"></i>'],
            pagination:!0,
            loop:!0,
            margin:10,
            autoplay:!0,
            autoplayTimeout:3000,
            autoplayHoverPause:!0,
            responsiveClass:!0,
            responsive:{
                0:{
                    items:1,
                    nav:!1,
                    stagePadding:0
                },
                400:{
                    items:1,
                    nav:!1,
                    stagePadding:0
                },768:{
                    items:1,
                    nav:!1,
                    pagination:!0,
                    // stagePadding:50
                }
            }
        });
    </script>   
@endsection
<div id="slider_{{$slider->id}}" dir="ltr" class="owl-carousel owl-theme home_slider"  style="opacity: 1;">
    @foreach ($slider->bannerImages as $banners )
        <div class="item">
            @if ($banners['url'])
                <a href="{{$banners['url']}}" class="d-block">
                    <img src="{{$banners['images']}}" alt="" class="img-fluid">
                </a>
            @else
                <img src="{{$banners['images']}}" alt="" class="img-fluid">
            @endif
        </div>
    @endforeach
    </div>

        