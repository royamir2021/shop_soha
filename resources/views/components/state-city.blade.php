@push('child-style')
  <link rel="stylesheet" href={{asset('plugins/select2/select2.css')}}>   
@endpush
@push('script-library')
   <script src="{{asset('plugins/select2/select2.js')}}"></script>
@endpush
@push('child-script')
  <script>
    $('#addStateSelect').select2({
      'palceholder': 'استان را انتخاب کنید',
      'dir': 'rtl',
    });
    $('#addCitySelect').select2({
      'palceholder': 'محصول در کدام شهر قرار دارد',
      'dir': 'rtl',
    });
    function changeCityValue(event){
      var valueBox = $('#addCitySelect');
      var stateId = $('#addStateSelect').val();
      $.ajaxSetup({
        headers : {
          'X-CSRF-TOKEN' : document.head.querySelector('meta[name="csrf-token"]').content,
          'Content-Type' : 'application/json'
        }
      });
      $.ajax({
        url:'/cities',
        type:'POST',
        data : JSON.stringify({
          id : stateId
        }),
        success : function(data) {
          $("#addCitySelect option").remove();
          var options;

          $.each( data, function( key, value ) {  
            options = options + '<option value="'+key+'">'+value+'</option>';
          });
          $("#addCitySelect").html(options);

        }
      });
    }
  </script>
@endpush
<div class="form-group col-6 float-right">
  <label for="addStateSelect">نام استان</label>
  <select class="form-control @error('state_id') is-invalid @enderror " name="state_id" id="addStateSelect" onchange="changeCityValue()">
    <option value="{{old('state_id')}}"> --  انتخاب  کنید --</option>
    @foreach (\App\State::all() as $state)
    <option value="{{$state->id}}" {{$state->id == $state_id ? "selected" : ""}}>{{$state->name}}</option>
    @endforeach
  </select>
  @error('state_id')
    <span class="invalid-feedback" role="alert">{{$message}}</span>
  @enderror
</div>
<div class="form-group col-6 float-right">
  <label for="addCitySelect">نام شهر</label>
  <select class="form-control @error('city_id') is-invalid @enderror " name="city_id" id="addCitySelect">
    <option value="">{{__('app.select')}}</option>
    @foreach (\App\City::all() as $city)
    <option value="">{{__('app.select')}}</option>
    <option value="{{$city->id}}" {{$city_id == $city->id ?"selected": ""}}>{{$city->name}}</option>
    @endforeach
  </select>
  @error('city_id')
    <span class="invalid-feedback" role="alert">{{$message}}</span>
  @enderror
</div>