@component('catalog.layouts.content')
    @section('content')
        <div class="container mb-3">
            <div class="row">
                <div class="col-4 float-right">
                    <div class="col-12 p-0 box-shadow mb-3">
                        <x-homeTopBanner :bannerInfo="$information['topBanner1']"/>
                </div>
                    <div class="col-12 p-0 box-shadow">
                        <x-homeTopBanner :bannerInfo="$information['topBanner2']"/>
                    </div>
                </div>
                <div class="col-8">
                    <x-slider :slider="$information['slider']"/>
                </div>
            </div>
        </div>
        @if(!is_null($information['topProductSlider']))
            <div class="container product-slide mb-3">
                <div class="row">
                    <div class="col-12">
                        <div class="col-12 box-shadow pt-3 pb-3 rounded text-right">
                            <h3><i class="fas fa-chevron-circle-left"></i>{{$information['topProductSlider']->name}}</h3>
                            <x-productSlider :productInfo="$information['topProductSlider']"/>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="home-cat-icon mb-5 mt-5 pt-5 pb-5 ">
            <div class="container">
                <div class="row">
                    <ul class="p-0 w-100 m-0">
                        <li class="col-md-2 float-right text-center"><a data-toggle="tooltip" title="" data-original-title="افزودن محصول جدید" class="d-block" href=""><i class="fi flaticon-nut"></i></a></li>
                        <li class="col-md-2 float-right text-center"><a class="d-block" href=""><i class="fi flaticon-cashew"></i></a></li>
                        <li class="col-md-2 float-right text-center"><a class="d-block" href=""><i class="fi flaticon-walnut"></i></a></li>
                        <li class="col-md-2 float-right text-center"><a class="d-block" href=""><i class="fi flaticon-seed"></i></a></li>
                        <li class="col-md-2 float-right text-center"><a class="d-block" href=""><i class="fi flaticon-almond"></i></a></li>
                        <li class="col-md-2 float-right text-center"><a class="d-block" href=""><i class="fi flaticon-peanut"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        @if(!is_null($information['centerProductSlider']))
            <div class="container product-slide mb-3">
                <div class="row">
                    <div class="col-12">
                        <div class="col-12 box-shadow pt-3 pb-3 rounded text-right">
                            <h3><i class="fas fa-chevron-circle-left"></i>{{$information['centerProductSlider']->name}}</h3>
                            <x-productSlider :productInfo="$information['centerProductSlider']"/>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if(!is_null($information['bottomProductSlider']))
            <div class="container product-slide mb-3">
                <div class="row">
                    <div class="col-12">
                        <div class="col-12 box-shadow pt-3 pb-3 rounded text-right">
                            <h3><i class="fas fa-chevron-circle-left"></i>{{$information['bottomProductSlider']->name}}</h3>
                            <x-productSlider :productInfo="$information['bottomProductSlider']"/>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endsection 
@endcomponent


 