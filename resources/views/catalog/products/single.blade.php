@extends('catalog.layouts.app',['body_class' => 'product-page'])

@inject('thumb', '\App\Traits\Images\ThumbClassForBlade')

@section('link')
<link rel="stylesheet" href={{asset('plugins/select2/select2.css')}}>
<link rel="stylesheet" href={{asset('plugins/fancybox/fancybox.min.css')}}>
@endsection

@section('script')
<script src="/plugins/select2/select2.full.js"></script>
<script src="/plugins/fancybox/fancybox.min.js"></script>
<script>
  $(document).on("click",'.comment_answer',function(e){
    commentId = $(this).data("id");
    $('input[name=parent_id]').val(commentId);
  });
</script>
@endsection
@section('breadcrumb')
  <li class="breadcrumb-item"><a href="/">{{__('app.home')}}</a></li>
  <li class="breadcrumb-item ">{{$product->name}} </li>
  @endsection
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-12 text-right">
        <div class="card border-0 box-shadow">
          <div class="card-body product-details pt-0 pb-0">
            <div class="col-md-6 col-12 float-right" >
              <a data-fancybox="gallery" class="d-block" href="{{$product->image}}">
                <img class="img-fluid"  src="{{$product->image}}">
              </a>            
              <div class="thumbs">
                @foreach ($product->productImages as $images)
                  <a data-fancybox="gallery" class="d-inline-block" href="{{$images->image}}">
                    <img class="img-fluid" src="{{$thumb->getThumb($images->image)}}">
                  </a>
                @endforeach
              </div>
            </div>
            <div class="col-md-6 col-12 pt-5 float-right">
              <h1>{{$product->name}}</h1>
              <div class="categories">
                <span class="float-right pl-1">دسته بندی:</span>
                <ul class="p-0 pl-1">
                  @foreach ($product->categories()->get() as $category)
                    <li class="d-inline-block"><a href="/category/{{$category->slug}}">{{$category->name}}</a></li>
                  @endforeach
                </ul>
              </div>
              <div class="short-desc">
                <h6>توضیحات کوتاه درباره محصول: </h6>
                <p>{{$product->short_description}}</p>
              </div>
              @if ($product->productseller->isNotEmpty())
                <div class="price text-center mt-4 mb-3 pb-3">
                  <ul class=" number p-0 pt-4 pb-4 mb-3">
                    @if ($informations['min_price'])<li class="d-inline-block">{{getPrice($informations['min_price'])}}</li>@endif
                    @if (!empty($informations['max_price']))<li class="d-inline-block max">{{getPrice($informations['max_price'])}}</li>@endif
                  </ul>
                  <a href="#vendor" class="d-inline-block">فروشندگان</a>
                </div>
                @else
                  <div class="unavailable d-block mt-5 text-center">
                    <p>ناموجود</p>
                  </div>
                @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-12 text-right"> 
          <div id="vendor" class="card mt-3 border-0 box-shadow vendor">
            <div class="card-body">
              <table class="table">
                <tbody>
                  @foreach ($product->productseller as $seller)
                  @if ($seller->user->approved)
                    <tr>
                      <th>
                        <span class="d-block">{{$seller->user->shop_name}}</span>
                        <small>{{$seller->state->name}}/{{$seller->City->name}}</small>
                      </th>
                      <th class="text-center">
                        <span class="d-block">موجودی</span>
                        <small class="number">{{number_format($seller->inventory)}} کیلو</small>
                      </th>
                      <th class="text-center">
                        <span class="d-block">قیمت هر کیلو</span>
                        <small class="number">{{getPrice($seller->price)}}</small>
                      </th>
                      <th class="text-center">
                        <form action="{{route('add.cart',$seller->id)}}"  id="addCart-{{$seller->id}}" method="POST">
                          @csrf
                          <input type="number" step="10"  name="quantity" value="{{$seller->minimum}}" min="{{$seller->minimum}}" max="{{$seller->inventory}}">
                          <input type="hidden" name="seller_id" value="{{$seller['seller_id']}}">
                        </form>
                      </th>
                      <th class="text-center">
                        <span class="btn-success hoverEffect border-0 btn" onclick="document.getElementById('addCart-{{$seller->id}}').submit()">{{__('app.add-to-cart')}}</span>
                      </th>
                    </tr>                        
                    @endif
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        <div class="product-info mt-3 mb-3">
          <ul class="nav nav-tabs p-0 m-0 bg-light border-0 box-shadow" id="myTab" role="tablist">
            @if(!is_null($product->description))
              <li class="nav-item">
                <a class="nav-link active" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true">توضیحات</a>
              </li>
            @endif
            @if(!is_null($product->attributes))
              <li class="nav-item">
                <a class="nav-link" id="attribute-tab" data-toggle="tab" href="#attribute" role="tab" aria-controls="attribute" aria-selected="false">مشخصات</a>
              </li>
            @endif
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane description fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
              @if(!is_null($product->description))
                  <div class="card border-0 box-shadow-h">
                    <div class="card-body">{!! $product->description !!}</div>    
                </div>
              @endif
            </div>
            <div class="tab-pane fade attribute" id="attribute" role="tabpanel" aria-labelledby="attribute-tab">
              <div class="card border-0 pt-3 pb-3 box-shadow-h" style="display: inherit">
                @foreach ($product->attributes as $attribute)
                  <div class="col-12 p-0 mb-1 d-flex">
                    <div class="col-md-4 float-right">
                      <p>{{$attribute->name}}</p>
                    </div>
                    <div class="col-md-8 float-right">
                      <p>{{\App\AttributeValue::findorFail($attribute->pivot->value_id)->value}}</p>
                    </div>
                  </div>
                  <div class="clear"></div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
        @if($informations['comments'])
          <div class="comments mt-4 col-12 p-0">
            <h3 class="title-box"><i class="fas fa-chevron-circle-left"></i> دیدگاه کاربران</h3>
            <x-comments :comments="$informations['comments']"></x-comments>
          </div>
        @endif

        <div class="card mt-3 border-0 box-shadow">
          <div class="card-footer border-0 mt-2">
          <x-comment-form :product="$product"></x-comment-form>
        </div>
        </div>
      </div>
    </div>
  </div>
@endsection
