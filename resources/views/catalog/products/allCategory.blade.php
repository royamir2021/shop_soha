@extends('catalog.layouts.app')
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-12 text-right">
        <div class="clearfix"></div>
        @foreach ($categries as $category)
            <div class="col-md-4">
            <h3>{{$category->name}}</h3>
            <a href="/category/{{$category->slug}}">مشاهده همه محصولات</a>
            </div>
        @endforeach
      </div>
    </div>
  </div>
@endsection
