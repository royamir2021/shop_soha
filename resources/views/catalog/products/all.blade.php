@extends('catalog.layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 text-right">
                @foreach ($products->chunk(4) as $row )
                    <div class="row">
                        @foreach ($row as $product)
                            <div class="col-3">    
                                <div class="card">
                                    <h3>{{$product->name}}</h3>
                                    <a href="/product/{{$product->id}}">نمایش</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach

            </div>
        </div>
    </div>
@endsection