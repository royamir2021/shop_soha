@extends('catalog.layouts.app',['body_class' => 'category-page'])

@inject('productsWithPaginate', '\App\Traits\Paginate\CustomPaginateForBlade')

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="/">{{__('app.home')}}</a></li>
  <li class="breadcrumb-item ">{{$category->name}} </li>
  @endsection
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-3 mt-3 category-filter text-right">
        <div class="card border-0">
          <h3>مرتب سازی</h3>
          <div class="card-body">
            @php $parentCategory = $category->parent_id == 0 ? $category : $category->parent @endphp
            <ul class="p-2">
              <li class="parent">
                <i class="fa fa-angle-down"></i><a href="/category/{{$parentCategory->slug}}">{{$parentCategory->name}}</a>
              </li>
              @foreach ($parentCategory->children as $categories)
                <li>
                  @if(($categories->children)->isNotEmpty())
                    <i class="fa fa-angle-down"></i>
                  @endif
                  <a class="{{$category->id == $categories->id ? "selected" : ""}}" href="/category/{{$parentCategory->slug}}/{{$categories->slug}}">{{$categories->name}}</a>
                  @if(($categories->children)->isNotEmpty())
                    <ul class="subcat p-0">
                      @foreach ($categories->children as $cat)
                        <li>
                          <a class="{{$category->id == $cat->id ? "selected" : ""}}" href="/category/{{$parentCategory->slug}}/{{$categories->slug}}/{{$cat->slug}}">{{$cat->name}}</a>
                        </li>
                      @endforeach
                    </ul>
                  @endif
                </li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
      <div class="col-md-9 category-list mt-3 text-right p-0">
        <h1>{{$category->name}}</h1>
        <div class="clearfix"></div>

        @php 
          $ids = $category->allProducts($category->allSubCategory()->add($category)->pluck('id')); 
          $productIds = $productsWithPaginate->paginate($ids, 30,url()->current())
        @endphp

       @foreach ($productIds as $productid)
          @php 
            $product = (new \App\Product)->find($productid->id);
            $sellerActive = $product->productSeller()->get()->filter(function($item){ 
              return $item->user->approved == 1;
            });
          @endphp
           <div class="col-md-4 mb-4 float-right text-center product-thumb  {{is_null($sellerActive->min('price'))? "prodcut-unavailable":''}} ">      
            <div class="card border-0 box-shadow">
              <div class="card-body">
                <a href="/{{$product->slug}}">
                  <img src="{{route('thumb',['url'=>$product->image,'w'=>250,'h'=>250])}}" class="img-fluid" width="250px" alt="">    
                <h3>{{$product->name}}</h3></a>
                @if (!is_null($sellerActive->min('price')))
                  <h5 class="price">{{getPrice($sellerActive->min('price'))}}</h5>
                @else
                  <h5 class="unavailable">ناموجود</h5>
                @endif
              </div>
            </div>     
          </div>
        @endforeach 
        <div class="clear"></div>
        <div class="card border-0 mt-3">
          <div class="card-footer border-0 bg-transparent text-center">
            {{$productIds->links()}}
          </div>
        </div>
      </div>   
    </div>
  </div>
@endsection
