@extends('catalog.layouts.app')
@section('content')
<main class="py-4">
    @if(!empty(trim($__env->yieldContent('breadcrumb'))))
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            @yield('breadcrumb')
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    @endif
</main>
@endsection
