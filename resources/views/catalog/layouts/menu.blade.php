<ul class=" text-right mb-0 p-0">
    @foreach ($categories as $category)
        <li class="d-inline-block">
            <a class="d-block" href="/category/{{$category->slug}}">{{$category->name}}</a>
            @if ($category->children->count())
                @include('catalog.layouts.menu',['categories' => $category->children->where('top',1)])
            @endif 
        </li>
    @endforeach
    <li class="d-inline-block pannel-link float-left mt-3">
        @if (auth()->user() && auth()->user()->vendor)
            <a href="/seller-dashboard">ورود به پنل فروشگاه</a>
        @else
        <a href="{{auth()->user()?'/seller-register':'/login'}}">ساخت پنل فروشگاه</a>
    </li>
    @endif
  </ul>