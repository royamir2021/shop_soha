<!doctype html>
<html  lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="{{app()->getLocale()=='fa'? 'rtl':'ltr'}}">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
{!! SEO::generate() !!}
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/font-awesome/v5-15/all.css') }}" rel="stylesheet">
<link href="{{url(getSetting()->store_icon)}}" rel="icon" />
@yield('link')
@stack('child-style')
</head>
<body
@unless(empty($body_class))
class="{{$body_class}}"
@endunless
>
    <div id="app">
        <header class="mb-3">            
            <nav class="top-nav p-3">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7 text-right"><p class="m-0"><i class="fas fa-map-marker-alt"></i> {{getSetting()->address}}</p></div>
                        <div class="col-5 text-left">
                            <ul class="m-0">
                                <li><i class="fas fa-phone-alt"></i>{{getSetting()->store_phone}}</li>
                            <li><i class="fas fa-mobile-alt"></i>{{getSetting()->store_phone2}}</li>
                            </ul>
                        </div>
                    </div>
                </div>    
            </nav>
            <div class="center-nav bg-white dark-text pt-1 pb-1 shadow-sm">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 float-right text-right">
                            <a class="d-block" href="{{ url('/') }}">
                                <img src="{{ getSetting()->store_logo }}" alt="">
                            </a>
                        </div>
                        <div class="@guest col-md-6 @else col-md-5 @endguest float-right search-box text-center">
                            <form class="form-inline my-3 my-lg-0">
                                <input class="form-control w-100 mr-sm-2" type="search" placeholder="جستجو" aria-label="Search">
                                <button class="btn my-2 my-sm-0" type="submit"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                        <div class="@guest col-md-2 @else col-md-3 @endguest user-maneger  float-right text-center">
                            @guest
                                <a class="nav-link login hoverEffect" href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i> وارد شوید</a>
                            @else
                            <a class="logout hoverEffect" href="{{ route('logout') }}"><i class="fas fa-sign-out-alt"></i> خروج</a>
                                <a href="/profile" class="profile" ><i class="fas fa-user-alt"></i> حساب کاربری</a> 
                            @endguest
                        </div>
                        <top-cart></top-cart>
                    </div>
                </div>
            </div>
            <div class="main-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            @include('catalog.layouts.menu',['categories'=>\App\Category::all()->where('top','==',1)->where('parent_id','==',0)])
                        </div>
                    </div>
                </div>
            </div>

        </header>