
<footer class=" text-dark mt-5 pb-2" dir="rtl">
    <div class="shop-info mb-4 pb-4 pt-4 ">
        <div class="container">
            <div class="row">
                <div class="col-12 text-right p-0">
                    <div class="col-8 float-right"><p><span>نشاني: </span>{{getSetting()->address}} </p></div>
                    <div class="col-2 float-right"><p><span>تلفن: </span><span class="number">{{getSetting()->store_phone}}</span></p></div>
                    <div class="col-2 float-right"><p><span>موبایل: </span><span class="number">{{getSetting()->store_phone2}}</span></p></div>
    
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 text-right info p-0">
                <div class="col-md-6 col-12 float-right about">
                    <h2>درباره سٌهانوین</h2>
                    <p class="m-0">{{getSetting()->comment}}</p>
                </div>
                <div class="col-md-6 col-12 float-right address mt-4">
                    <p><i class="fa fa-phone-square"></i></p>
                    <p><i class="fa fa-map"></i> نشاني: {{getSetting()->address}} </p>
                    <p><i class="fa fa-at"></i> پست الکترونیک info [at] sohanovin.com </p>
                </div>
            </div>
            <hr class="border-white"> 
            <div class="col-12 text-right">
                تمام حقوق این سایت متعلق به شرکت سهانوین می باشد
            </div>
        </div>
    </div>
</footer>
</div>
 
<script src="{{ asset('js/app.js') }}"></script>
{{-- <script src="{{ asset('js/cart.js') }}"></script> --}}
@stack('script-library')
@include('sweet::alert')
@yield('script')
{{$script ?? ""}}
@stack('child-script')
</body>
</html>