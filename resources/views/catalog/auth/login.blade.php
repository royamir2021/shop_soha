@extends('catalog.layouts.landing')

@section('content')

<div class="limiter">
    <div class="login w-100 login-display">
        <div class="wrap-login login-display box-shadow">
            <form method="POST" action="{{ route('login') }}" class="login-form validate-form">
                @csrf
                <span class="title w-100 d-block text-center mb-5"> شماره موبایل خود را وارد کنید</span>

                <div class="wrap-input position-relative w-100">
                    <input id="phone" type="phone" class="input form-control @error('phone') is-invalid @enderror" name="phone" placeholder="{{ __('app.mobile_number') }}" value="{{ old('phone') }}" required autocomplete="phone" autofocus>

                    @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <span class="focus-input d-block position-absolute w-100 h-100"></span>
                        <i class="fa fa-mobile position-absolute" aria-hidden="true"></i>
                </div>
                
                <div class="form-btn w-100 login-display">
                    <button type="submit" class="login-display">ورود </button>
                </div>

            </form>
            <div class="pic js-tilt" data-tilt>
                <img src="{{asset('img/login.png')}}" alt="IMG">
            </div>
        </div>
    </div>
</div>
@endsection
