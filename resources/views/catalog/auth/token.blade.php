
@extends('catalog.layouts.landing')

@section('content')

<div class="limiter">
    <div class="login w-100 login-display">
        <div class="login-display wrap-login  box-shadow">
            <form method="POST" action="{{ route('auth.token') }}" class="login-form validate-form">
                @csrf
                <span class="title w-100 d-block text-center mb-5">{{__('app.verify_token')}}</span>
                <div class="wrap-input position-relative w-100">
                    <input name="name" id="name" type="text" class="input form-control @error('name') is-invalid @enderror"  placeholder=" نام و نام خانوادگی" value="{{session()->get('token')['user_name']? session()->get('token')['user_name'] : old('name') }}" required autofocus>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <span class="focus-input d-block position-absolute w-100 h-100"></span>
                        <i class="fa fa-user position-absolute" aria-hidden="true"></i>
                </div>
                <div class="wrap-input position-relative w-100">
                    <input name="token" id="token" type="text" class="input form-control @error('token') is-invalid @enderror"  placeholder="کد تایید" value="{{ old('phone') }}" required autofocus>
                    @error('token')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <span class="focus-input d-block position-absolute w-100 h-100"></span>
                        <i class="fa fa-mobile position-absolute" aria-hidden="true"></i>
                </div>
                
                <div class="form-btn w-100 login-display">
                    <button type="submit" class="login-display">تایید </button>
                </div>

            </form>
            <div class="pic js-tilt" data-tilt>
                <img src="{{asset('img/login.png')}}" alt="IMG">
            </div>
        </div>
    </div>
</div>
@endsection
