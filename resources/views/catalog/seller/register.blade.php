@extends('catalog.seller.app')

@section('title','صفحه ثبت نام فروشنده | فروشگاه سًهانوین')
@section('bodyClass','register-landing')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-12 offset-md-2 text-right pt-5 pb-3">
                <div class="register-box">
                    <div class="card-heading text-center rounded-top">
                        <h1> ساخت پنل فروشگاه </h1>
                    </div>
                    <div class="cart-body">
                        <form action="{{route('seller-register.store')}}" method="POST">
                            @csrf
                            <input type="hidden" value="1" name="vendor">
                            <div class="form-group col-md-6 col-12 float-right">
                                <label for="shop_name">{{__('seller.label.shop_name')}}</label>
                                <input type="text" name="shop_name" value="{{old('shop_name')}}" class="form-control @error('shop_name') is-invalid @enderror" id="shop_name" placeholder="{{__('seller.placeholder.shop_name')}}">
                                @error('shop_name')
                                    <span class="invalid-feedback" role="alert">
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 col-12 float-right">
                                <label for="shop_phone">{{__('seller.label.shop_phone')}}</label>
                                <input type="text" name="shop_phone" value="{{old('shop_phone')}}" class="form-control @error('shop_phone') is-invalid @enderror" id="shop_phone" placeholder="{{__('seller.placeholder.shop_phone')}}">
                                @error('shop_phone')
                                <span class="invalid-feedback" role="alert">
                                    {{$message}}
                                </span>
                                @enderror
                            </div>
                            <x-state-city />
                            <div class="form-group col-12 float-right">
                                <label for="address">{{__('seller.label.address')}}</label>
                                <textarea type="text" name="address" cols="3" rows="3"  class="form-control @error('address') is-invalid @enderror" id="address" placeholder="{{__('seller.placeholder.address')}}">{{old('address')}}</textarea>
                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                            <div class="clear"></div>
                            <div class="form-group col-12">
                                <a href="/" class="btn btn-secondary"><i class="fa fa-arrow-right"></i></a>
                                <button type="submit" class="btn btn-success float-left">{{__('app.text_register')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> 
@endsection