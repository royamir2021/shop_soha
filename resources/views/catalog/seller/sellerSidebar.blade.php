<aside class="main-sidebar col-md-2 bg-dark text-white text-right box-shadow elevation-4">
  <div class="sidebar" style="direction: ltr">
    <div style="direction: rtl">
      <div class="user-panel border-bottom mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <i class="fa fa-user d-inline-block ml-3"></i>
          <p class="text-center d-inline-block"> {{auth()->user()->name}} <br> {{auth()->user()->phone}}</p>
        <a href="#" class="d-block"></a>
        </div>
      </div>
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar p-0 flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview">
            <a href="{{route('dashboard.index')}}" class="nav-link {{activeMenu('dashboard.index')}}">
              <i class="nav-icon fa fa-dashboard" aria-hidden="true"></i>داشبورد</a>
          </li>
          <li class="nav-item has-treeview ">
            <a href="{{route('seller-products.index')}}" class="nav-link {{activeMenu('seller-products.index')}}">
              <i class="nav-icon fa fa-product-hunt" aria-hidden="true"></i> محصولات
            </a>
          </li>
          <li class="nav-item has-treeview ">
            <a href="" class="nav-link ">
              <i class="nav-icon fa fa-shopping-cart fw"></i> سفارشات 
            </a>
          </li>
          <li class="nav-item has-treeview ">
            <a href="" class="nav-link ">
              <i class="nav-icon fa fa-dollar fw"></i> درخواست وجه 
            </a>
          </li>
          <li class="nav-item has-treeview ">
            <a href="{{route('shop-info')}}" class="nav-link {{activeMenu('shop-info')}}">
              <i class="nav-icon fa fa-info-circle"></i> ویرایش اطلاعات
            </a>
          </li>
          <li class="nav-item has-treeview ">
            <a href="{{route('support.index')}}" class="nav-link {{activeMenu('support.index')}}">
              <i class="nav-icon fa fa-life-ring"></i> پشتیبانی
            </a>
          </li>
          <li class="nav-item has-treeview ">
            <a href="{{route('logout')}}" class="nav-link ">
              <i class="nav-icon fa fa-sign-out fa-lg"></i> خروج 
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
</aside>