@extends('catalog.seller.dashboardContent')
@section('bodyClass','dashboard')

@section('dashboardContent')
  <div class="card text-right mt-2">

    <div class="card-header pb-3">
      <h3 class="card-title d-inline-block">لیست درخواست ها </h3>
      <div class="card-tools d-inline-block float-left text-left">
        <div class="position-relative">
          <a class="btn btn-primary" href="{{route('support.create')}}">ایجاد درخواست جدید</a>
        </div>
      </div>
    </div>
    <div class="card-body">
      <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">شماره تیکت</th>
            <th scope="col">موضوع</th>
            <th scope="col">تاریخ</th>
            <th scope="col">الویت</th>
            <th scope="col">وضعیت</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($tikets as $support)
          <tr>
            <th scope="row">#SNTN-{{$support->id}}</th>
            <td>{{$support->subject}}</td>
            <td>{{jdate($support->created_at)->format('%A, %d %B %y')}}</td>
            <td>
              <span class="{{$support->priority == 3 ?:"d-none"}}">کم</span>
              <span class="{{$support->priority == 2 ?:"d-none"}}">متوسط</span>
              <span class="{{$support->priority == 1 ?:"d-none"}}">زیاد</span>
            </td>
            <td>
              <span class="alert-info {{$support->status_id == 0 ?:"d-none"}}">در انتظار پاسخ</span>
              <span class="alert-warning {{$support->status_id == 1 ?:"d-none"}}">در حال بررسی</span>
              <span class="alert-success {{$support->status_id == 2 ?:"d-none"}}">پاسخ داده شد</span>
            </td>
            <td>
              <a href="{{route('support.edit',$support->id)}}" class="btn btn-primary">مشاهده درخواست</a>
            </td>
          </tr>  
          @endforeach
        </tbody>
      </table>
    </div>
    
  </div>                    
@endsection