@extends('catalog.seller.dashboardContent')
@section('bodyClass','dashboard')

@section('dashboardContent')
<div class="card text-right mt-2">
    <div class="card-body">
      <form method="POST" action="{{route('support.store')}}">
        @csrf
        <input type="hidden" name="supportable_id" value="{{auth()->user()->id}}">
        <input type="hidden" name="supportable_type" value="{{get_class(auth()->user())}}">
        <input type="hidden" name="parent_id" value="0">
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="subject">{{__('app.subject')}}</label>
            <input type="text" name="subject" value="{{old('subject')}}" class="form-control @error('subject') is-invalid @enderror" id="subject">
            @error('subject')
                <span class="invalid-feedback" role="alert">{{$message}}</span>
            @enderror
          </div>
          <div class="form-group col-md-3">
            <label for="part">بخش مربوطه</label>
            <select name="part_id" class="form-control @error('part_id') is-invalid @enderror" id="part">
              <option value="">{{__('app.select')}}</option>
              <option value="1">حسابداری</option>
              <option value="2">فنی</option>
            </select>
            @error('part_id')
                <span class="invalid-feedback" role="alert">{{$message}}</span>
            @enderror
          </div>
          <div class="form-group col-md-3">
            <label for="priority">الویت</label>
            <select name="priority" class="form-control @error('priority') is-invalid @enderror" id="priority">
                <option value="3">کم</option>
                <option value="2" selected>متوسط</option>
                <option value="1">زیاد</option>
            </select>
            @error('priority')
                <span class="invalid-feedback" role="alert">{{$message}}</span>
            @enderror
          </div>
        
            <div class="form-group col-md-12">
                <label for="text">متن درخواست</label>
                <textarea name="text" class="form-control @error('text') is-invalid @enderror" id="text" cols="30" rows="5">{{old('text')}}</textarea>
                @error('text')
                <span class="invalid-feedback" role="alert">{{$message}}</span>
            @enderror
            </div>
        </div>
        <button type="submit" class="btn float-left btn-success">{{__('app.send')}}</button>
      </form>
    </div>
</div>
@endsection