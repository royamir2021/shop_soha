@extends('catalog.seller.dashboardContent')
@section('bodyClass','dashboard')
@section('script')
<script>
    $('#addProductSelect').select2({
      'palceholder': 'لطفا محصول مورد نظر را انتخاب کنید',
    });
    $('#addbrand').select2({
      'palceholder': 'لطفا برند را انتخاب کنید',
      tags: true
    });

  </script>
@endsection
@section('dashboardContent')
<div class="seller-add-product col-8 offset-2">
    <div class="card text-right mt-2">
        <div class="card-header">افزودن محصول جدید
            <input type="submit" class="btn btn-success float-left" form="form-add-product" value="افزودن محصول" placeholder="{{__('app.quantity')}}">

        </div>
        <div class="card-body">
        <form method="POST" action="{{route('seller-products.store')}}" id="form-add-product" class="harizonal-form">
            @csrf
            <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
            <div class="row mb-3">
                <div class="col">
                    <label for="addProductSelect">{{__('seller.label.product_name')}}</label>
                    <select class="form-control @error('product_id') is-invalid @enderror" name="product_id" id="addProductSelect">
                    <option value="{{old('product_id')}}">{{__('seller.text.add_product')}}</option>            
                    @foreach (\App\Product::all() as $product)
                        <option value="{{$product->id}}">{{$product->name}}</option>
                    @endforeach
                    </select>
                    @error('product_id')
                    <span class="invalid-feedback" role="alert">{{$message}}</span>
                    @enderror
                </div>
            </div>
            <div class="row mb-3">
                <div class="col"><x-state-city/></div>
            </div>
            <div class="row mb-3">
                <div class="col">
                    <label for="inventory">{{__('app.quantity')}}</label>
                    <input type="text" value="{{old('inventory')}}" name="inventory" class="form-control @error('inventory') is-invalid @enderror " id="inventory" placeholder="{{__('app.quantity')}}">
                    @error('inventory')
                        <span class="invalid-feedback" role="alert">{{$message}}</span>
                    @enderror
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">
                    <label for="minimum">{{__('app.text_minimum')}}</label>
                    <input type="text" value="{{old('minimum')}}" name="minimum" class="form-control @error('minimum') is-invalid @enderror " id="minimum" placeholder="{{__('app.text_minimum')}}">
                    @error('minimum')
                        <span class="invalid-feedback" role="alert">{{$message}}</span>
                    @enderror
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">
                    <label for="brand">{{__('app.brand')}}</label>
                    <select name="brand" id="addbrand" class="form-control">
                        <option value="">{{__('app.select')}}</option>
                        @foreach (\App\Brand::all() as $brand)
                        <option value="{{$brand->brand}}" >{{$brand->brand}}</option>
                    @endforeach
                    </select>
                    @error('brand')
                        <span class="invalid-feedback" role="alert">{{$message}}</span>
                    @enderror
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">
                    <label for="price">{{__('app.price')}}</label>
                    <input type="text" value="{{old('price')}}" name="price" class="form-control @error('price') is-invalid @enderror " id="price" placeholder="{{__('app.price')}}">
                    @error('price')
                    <span class="invalid-feedback" role="alert">{{$message}}</span>
                    @enderror
                </div>
            </div>
            
        </form>
        </div>
    </div>
</div>
@endsection

      
      