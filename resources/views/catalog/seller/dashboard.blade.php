@extends('catalog.seller.dashboardContent')
@section('bodyClass','dashboard')
@section('dashboardContent')

<div class="pannel-shop d-flex mb-3">
    <div class="col-4 float-right text-center">
        <div class="bow">
            <h5>تعداد کالا</h5>
            <p class="number">{{auth()->user()->productUsers()->count()}}</p>
            <i class="fa fa-cart-plus"></i>
            <a href="/seller-products" class="d-block">مشاهده...</a> 
        </div>
    </div>
    <div class="col-4 float-right text-center">
        <div class="bow">
            <h5>تعداد سفارشات</h5>
            <p class="number">{{auth()->user()->productUsers()->count()}}</p>
            <i class="fa fa-bar-chart"></i>
            <a href="/seller-products" class="d-block">مشاهده...</a> 
        </div>
    </div>
    <div class="col-4 float-right text-center">
        <div class="bow">
            <h5> سفارشات تایید نشده</h5>
            <p class="number">{{auth()->user()->productUsers()->count()}}</p>
            <i class="fa fa-check-square-o"></i>
            <a href="/seller-products" class="d-block">مشاهده...</a> 
        </div>
    </div>

</div>
<div class="clear"></div>


@endsection