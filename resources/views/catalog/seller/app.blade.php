<!doctype html>
<html  lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="{{app()->getLocale()=='fa'? 'rtl':'ltr'}}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! SEO::generate() !!}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
    @yield('link')
    @stack('child-style')
</head>
<body class="@yield('bodyClass')">
    <main>
        @yield('content')
    </main>
    <script src="{{ asset('js/app.js') }}"></script>
    @stack('script-library')
    @include('sweet::alert')
    @yield('script')
    @stack('child-script')
</body>
</html>