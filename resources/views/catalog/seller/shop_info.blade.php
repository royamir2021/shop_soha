@extends('catalog.seller.dashboardContent')
@section('bodyClass','dashboard')
@section('script')
  <script>
    $('#approved').select2({});
  </script>
@endsection

@section('dashboardContent')
    <div class="card shipping-info">
         <div class="card-header">
            <ul class="nav nav-pills float-right " id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="pills-shop-tab" data-toggle="pill" href="#pills-shop" role="tab" aria-controls="pills-shop" aria-selected="true">اطلاعات فروشگاه</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="pills-fiscal-tab" data-toggle="pill" href="#pills-fiscal" role="tab" aria-controls="pills-fiscal" aria-selected="false">اطلاعات مالی</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="pills-shipping-tab" data-toggle="pill" href="#pills-shipping" role="tab" aria-controls="pills-shipping" aria-selected="false">اطلاعات ارسال</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">معرفی فروشگاه</a>
                </li>
            </ul>
            <div class="float-left">
                <button type="submit" form="form-shop-info" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Save"><i class="fa fa-save"></i></button>
            </div>
        </div> 
        <div class="card-body">
            <form action="{{route('shop-info')}}" method="POST" id="form-shop-info">
                @csrf
                <input type="hidden" name="user_id" value="{{$user->id}}">
                <div class="tab-content" id="pills-tabContent">      
                    <div class="tab-pane fade show active" id="pills-shop" role="tabpanel" aria-labelledby="pills-shop-tab">
                        <div class="row mb-3">
                            <div class="col">
                                <label class="col-form-label" for="name">{{__('app.name')}} </label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-user"></i></div>
                                    </div>
                                    <input type="text" name="name" value="{{$user->name}}" id="name" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="col">
                                <label for="phone" class="col-form-label">شماره موبایل مالک فروشگاه</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-mobile"></i></div>
                                    </div>
                                    <input type="text" name="phone" value="{{$user->phone}}" id="phone" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                                <div class="col">
                                    <label for="shop_name" class="col-form-label">{{__('app.shop_name')}} </label>
                                    <input type="text" name="shop_name" value="{{$user->shop_name}}" id="shop_name" class="@error('shop_name') is-invalid @enderror form-control">
                                    @error('shop_name')
                                        <span class="invalid-feedback" role="alert">
                                            {{$message}}
                                        </span>
                                    @enderror
                                </div>
                                <div class="col">
                                    <label for="shop_phone" class="col-form-label">{{__('app.shop_phone')}}</label>
                                    <input type="text" name="shop_phone" value="{{$user->shop_phone}}" id="shop_phone" class="@error('shop_phone') is-invalid @enderror form-control">
                                    @error('shop_phone')
                                        <span class="invalid-feedback" role="alert">
                                            {{$message}}
                                        </span>
                                    @enderror
                                </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col">
                                <label for="approved" class="col-form-label">{{__('app.status')}} </label>
                                <select name="approved" id="approved" class="form-control @error('approved') is-invalid @enderror">
                                    <option value="">{{__('app.select')}}</option>
                                    <option value="1" {{$user->approved == 1 ? "selected" : ""}} >{{__('app.active')}}</option>
                                    <option value="0" {{$user->approved == 0 ? "selected" : ""}}>{{__('app.deactive')}}</option>
                                </select>
                                @error('approved')
                                    <span class="invalid-feedback" role="alert">
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                            <div class="col">
                                <x-state-city :state="$user->state_id" :city="$user->city_id"/>
                            </div>
                        </div>
                        <div class="row mb-3">
                                <div class="col">
                                    <label for="address" class="col-form-label">{{__('app.address')}}</label>
                                    <textarea name="address" id="address" class="form-control @error('address') is-invalid @enderror" cols="30" rows="3" placeholder="{{__('app.address')}}">{{$user->address}}</textarea>
                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        {{$message}}
                                    </span>
                                @enderror
                                </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-fiscal" role="tabpanel" aria-labelledby="pills-fiscal-tab">
                        <div class="row mb-3">
                            <div class="col">
                                <label for="bank_name" class="col-form-label">{{__('seller.label.bank_name')}}</label>
                                <input type="text" name="bank_name" value="{{$user->bank_name}}" class="form-control @error('bank_name') is-invalid @enderror" id="bank_name" placeholder="{{__('seller.placeholder.bank_name')}}">
                                @error('bank_name')
                                    <span class="invalid-feedback" role="alert">
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                            <div class="col">
                                <label for="bank_account_number" class="col-form-label">{{__('seller.label.bank_account_number')}}</label>
                                <input type="text" name="bank_account_number" value="{{$user->bank_account_number}}" class="form-control @error('bank_account_number') is-invalid @enderror" id="bank_account_number" placeholder="{{__('seller.placeholder.account_number')}}">
                                @error('bank_account_number')
                                    <span class="invalid-feedback" role="alert">
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col">
                                <label for="bank_cart_number" class="col-form-label">{{__('seller.label.bank_cart_number')}}</label>
                                <input type="text" name="bank_cart_number" value="{{$user->bank_cart_number}}" class="form-control @error('bank_cart_number') is-invalid @enderror" id="bank_cart_number" placeholder="{{__('seller.placeholder.bank_cart')}}">
                                @error('bank_cart_number')
                                    <span class="invalid-feedback" role="alert">
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                            <div class="col">
                                <label for="bank_shaba_number" class="col-form-label">{{__('seller.label.bank_shaba_number')}}</label>
                                <input type="text" name="bank_shaba_number" value="{{$user->bank_shaba_number}}" class="form-control @error('bank_shaba_number') is-invalid @enderror" id="bank_shaba_number" placeholder="{{__('seller.placeholder.bank_shaba')}}">
                                @error('bank_shaba_number')
                                    <span class="invalid-feedback" role="alert">
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-shipping" role="tabpanel" aria-labelledby="pills-shipping-tab">
                        <div class="row mb-3">
                            <div class="col">
                                <label class="col-form-label" for="free_shipping_threshold">{{__('app.free_shipping_price')}}</label>
                                <input type="text" id="free_shipping_threshold" value="{{$user->moreVendorInfo->free_shipping_threshold}}" name="free_shipping_threshold" class="form-control @error('free_shipping_threshold') is-invalid @enderror" placeholder="{{__('app.free_shipping_price')}}">
                                @error('free_shipping_threshold')
                                    <span class="invalid-feedback" role="alert">
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                            <div class="col">
                                <label class="col-form-label" for="near_shipping_price">{{__('app.near_shipping_price')}}</label>
                                <input type="text" id="near_shipping_price" value="{{$user->moreVendorInfo->near_shipping_price}}" name="near_shipping_price" class="form-control @error('near_shipping_price') is-invalid @enderror" placeholder="{{__('app.near_shipping_price')}}">
                                @error('near_shipping_price')
                                    <span class="invalid-feedback" role="alert">
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                            <div class="col">
                                <label class="col-form-label" for="far_shipping_price">{{__('app.far_shipping_price')}}</label>
                                <input type="text" id="far_shipping_price" value="{{$user->moreVendorInfo->far_shipping_price}}" name="far_shipping_price" class="form-control @error('far_shipping_price') is-invalid @enderror" placeholder="{{__('app.far_shipping_price')}}">
                                @error('far_shipping_price')
                                    <span class="invalid-feedback" role="alert">
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                        <div class="row mb-3">
                            <div class="col">
                                <label class="col-form-label" for="description">{{__('app.description_shop')}}</label>
                                <textarea name="description" id="description" class="form-control @error('description') is-invalid @enderror" placeholder="{{__('app.description_shop')}}">{{$user->moreVendorInfo->description}}</textarea>
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col">
                                <label class="col-form-label" for="shipping_terms">{{__('app.shipping_terms')}}</label>
                                <textarea name="shipping_terms" class="form-control @error('shipping_terms') is-invalid @enderror" placeholder="{{__('app.shipping_terms')}}">{{$user->moreVendorInfo->shipping_terms}}</textarea>
                                @error('shipping_terms')
                                    <span class="invalid-feedback" role="alert">
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col">
                                <label class="col-form-label" for="return_terms">{{__('app.return_terms')}}</label>
                                <textarea name="return_terms" id="return_terms" class="form-control @error('return_terms') is-invalid @enderror" placeholder="{{__('app.return_terms')}}">{{$user->moreVendorInfo->return_terms}}</textarea>
                                @error('return_terms')
                                    <span class="invalid-feedback" role="alert">
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </form>  
        </div>
        
    </div>
@endsection