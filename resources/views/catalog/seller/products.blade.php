@extends('catalog.seller.dashboardContent')
@inject('productsWithPaginate', '\App\Traits\Paginate\CustomPaginateForBlade')
@section('bodyClass','dashboard')
@section('script')
  <script>
    $.ajaxSetup({
      'Content-Type':'aplication/json'
    });
    $.ajax({
      url:'active-vendor',
      type:'get',
      success : function(json){
        if(json=='error'){
          swal({
            title: "عدم دسترسی!",
            text: "برای افزودن محصول،نیاز به تکمیل اطلاعات دارید!",
            type:'error',
          }).then(function() {
            window.location = "shop-info";
          });
        }
      }
    });
  </script>
@endsection
@section('dashboardContent')
  <div class="card text-right mt-2 offset-1">
    <div class="card-header pb-3">
      <h3 class="card-title d-inline-block"> لیست محصولات </h3>
      <div class="card-tools d-inline-block float-left text-left">
        <div class="d-none position-relative">
          <form action="">
            <input type="text"  value="{{request('search')}}" name="search" class="form-control float-right" placeholder="جستجو">
              <button type="submit" style="left: 0" class="btn btn-default position-absolute"><i class="fa fa-search"></i></button>
          </form>
        </div>
      </div>
      <a href="{{route('seller-products.create')}}" class="btn btn-success float-left">+ افزودن محصول جدید</a>
    </div>
    <div class="card-body table-responsive p-0">
      <table class="table table-hover">
        <tbody>
          <tr>
            <th>#</th>
            <th>{{__('app.just_name')}}</th>
            <th class="text-center">{{__('app.price')}}</th>
            <th>{{__('app.quantity')}}</th>
            <th>{{__('app.minimum')}}</th>
            <th>{{__('app.city_name')}}</th>
            <th>{{__('app.action')}}</th>
          </tr>
          @php
            $products = $productsWithPaginate->paginate($user->productUsers,50,'/seller-products') 
          @endphp
          @foreach ($products as $product)
          <tr>  
            <td>{{$product['id']}}</td>
            <td>{{$product->product->name}}</td>
            <td class="text-center">{{ number_format($product['price']) }} تومان</td>
            <td class="text-center">{{$product['inventory']}}</td>
            <td class="text-center">{{$product['minimum']}}</td>
            <td>
              @foreach (\App\City::all() as $city)
                @if ($city->id == $product['city_id'])
                {{$city->name}}
                @endif
              @endforeach/
              @foreach (\App\State::all() as $state)
                  @if ($state->id == $product['state_id'])
                    <small>{{$state->name}}</small>
                  @endif
              @endforeach   
            </td>
            <td>
              <a href="{{route('seller-products.edit',$product['id'])}}" class="btn-primary btn ml-1 float-right">{{__('app.edit')}}</a>
              <form class="float-right" method="POST" action="{{route('seller-products.destroy',$product['id'])}}">
                @csrf
                @method('DELETE')
                <button class="btn-danger btn ml-1"> {{__('app.remove')}}</button>
              </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="card-footer">
      {{$products->links()}}
    </div>
  </div>                    
@endsection