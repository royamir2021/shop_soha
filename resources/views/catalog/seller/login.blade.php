@extends('catalog.seller.app')


{{-- @section('bodyClass','register-landing') --}}

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card border-0 box-shadow">
                    
                    <div class="cart-body">
                        @if (auth()->user() && auth()->user()->vendor)
                            <a href="/seller/dashboard">ورود به پنل فروشگاه</a>
                        
                        @else
                        <a href="{{auth()->user()?'/seller/register':'/login'}}">ساخت پنل فروشگاه</a>
                        
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div> 
@endsection