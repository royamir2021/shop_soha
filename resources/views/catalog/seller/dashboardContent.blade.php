@extends('catalog.seller.app')
@section('content')
<div class="container-fluid">
  <div class="row">    
    @include('catalog.seller.sellerNav')
    @include('catalog.seller.sellerSidebar')
    <div class="content-wrapper w-100 ml-3 position-absolute pt-5" style="min-height: 661px;">
      @yield('dashboardContent')
    </div>
  </div>
</div>
@endsection