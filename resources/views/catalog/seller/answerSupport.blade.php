@extends('catalog.seller.dashboardContent')
@section('bodyClass','dashboard')

@section('dashboardContent')
<div class="card text-right mt-2">
    <div class="card-header">
        <div class="form-group col-md-12">
            <div class="alert alert-secondary floar-right d-inline-block">
                {{$support->text}}
            </div>
            <div class="clear"></div>
            @foreach ($support->child as $child)
            <div class="alert d-inline-block {{$child->supportable_id == auth()->user()->id ? 'alert-secondary floar-right' : 'alert-info float-left'}}">
                {{$child->text}}
            </div>
            <div class="clear"></div>
            @endforeach
            
        </div>
        
    </div>
    <div class="card-body">
      <form method="POST" action="{{route('seller-support.answer')}}">
        @csrf
        <input type="hidden" name="supportable_id" value="{{auth()->user()->id}}">
        <input type="hidden" name="supportable_type" value="{{get_class(auth()->user())}}">
        <input type="hidden" name="parent_id" value="{{$support->id}}">
        <input type="hidden" name="part_id" value="{{$support->part_id}}">
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="text">متن درخواست</label>
                <textarea name="text" class="form-control @error('text') is-invalid @enderror" id="text" cols="30" rows="5">{{old('text')}}</textarea>
                @error('text')
                    <span class="invalid-feedback" role="alert">{{$message}}</span>
                @enderror
            </div>
        </div>
        <button type="submit" class="btn float-left btn-success">{{__('app.send')}}</button>
      </form>
    </div>
</div>
@endsection