@extends('catalog.layouts.app')
@section('content')
{{-- {{url()->currentRouteName()}} --}}
  <div class="container">
    <div class="row">
        <div class="col-12 address-page cart-page">
          @if($user->addresses->isEmpty())
            <div class="card col-10 offset-1 float-right  p-0 box-shadow no-address">
              <div class="card-body border-0 box-shadow text-center pt-5 pb-5">
                <i class="fa fa-map"></i>
                <p>لطفا یک آدرس ثبت کنید!</p>
                <button type="button" class="btn btn-success hoverEffect" data-toggle="modal" data-target="#addAddress">
                  ثبت آدرس جدید
                </button> 
              </div>
            </div> 
          @else
            <div class="address-box col-9 cart-page float-right">
              <div class="card box-shadow header-nav text-right p-0 mb-3">
                <div class="card-body pt-2 pb-2">
                  <p class="d-inline-block mb-0 mt-2">انتخاب آدرس جهت تحویل سفارش</p>
                  <div class="btn-group float-left" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-success ml-2" data-toggle="modal" data-target="#editAddress">تغییر آدرس</button>
                    <button type="button" class="btn btn-light" data-toggle="modal" data-target="#addAddress">+ افزودن آدرس جدید</button>
                  </div>
                </div>
              </div>
              @foreach ($user->addresses as $address)
                @if ($address->approved == 1)
                  <div class="current-address pb-5 pt-5 text-right">
                    <div class="box mr-5">
                      <p class="name">
                        <label>گیرنده : </label> <span class="name">{{$user->name}}</span>
                      </p> 
                      <p class="mobile d-inline-block ml-3">
                        <label>شماره موبایل : </label> <span class="name">{{$address->phone}}</span>
                      </p> | 
                      <p class="postcode d-inline-block mr-3">
                        <label>کد پستی : </span> <span class="name">{{$address->postcode}}</span>
                      </p> 
                      <p class="address">
                        استان: {{$address->state->name}} شهرستان {{$address->city->name}} <span>{{$address->address}} </span>
                      </p>
                    </div>
                  </div>
                @endif
              @endforeach   
            </div>
            <cart-result :auth_user='@json(auth()->user())' ></cart-result> 
          @endif
        </div>
          <x-addCheckoutAddress :user="$user"/>
          <x-editCheckoutAddress :user="$user"/>
        </div>
    </div>
  </div>
@endsection