@extends('catalog.layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="cart-page col-12">
      <my-cart></my-cart>

      <cart-result :auth_user='@json(auth()->user()->id)'></cart-result>
    </div>
  </div>
</div>
@endsection

