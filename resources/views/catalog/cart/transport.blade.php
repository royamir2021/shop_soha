@extends('catalog.layouts.app')
@section('content')
  <div class="container">
    <div class="row">
        <div class="col-12 card">
            <div class="card-body">
                <div class="col-md-12 float-right text-right">
                    @foreach ($transports as $transport)
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="transport" id="transport-{{$transport->id}}" value="{{$transport->id}}">
                        <label class="form-check-label" for="transport-{{$transport->id}}">
                            {{$transport->name}}
                        </label>
                      </div>
                    @endforeach
                </div>
                <div class="col-md-3 text-right float-right">
                </div>
            </div>
          </div>
    </div>
  </div>
@endsection