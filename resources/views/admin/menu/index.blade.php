@component('admin.layouts.content',['title'=>'فهرست'])
  @slot('breadcrumb')
    <li class="breadcrumb-item"><a href="admin">خانه</a></li>
    <li class="breadcrumb-item active"><a href="#">فهرست</a></li>
  @endslot
  @slot('buttonBox')
    <a href="{{ route('admin.menu.index') }}" class="btn btn-default"><i class="fa fa-reply"></i></a>
    <button type="submit" id="menu_form"  class="btn btn-primary"><i class="fa fa-save"></i></button>
@endslot

@slot('link')
    <!-- the css in the <head> -->
<link rel="stylesheet" href="/plugins/menu-editor/iconpicker/css/bootstrap-iconpicker.min.css">
@endslot
@slot('script')
<script type="text/javascript" src="/plugins/menu-editor/iconpicker/js/bootstrap-iconpicker.min.js"></script>
<script type="text/javascript" src="/plugins/menu-editor/jquery-menu-editor.min.js"></script>
<script>
// sortable list options
var sortableListOptions = {
    placeholderCss: {'background-color': "#cccccc"}
};
var editor = new MenuEditor('myEditor',{ 
    maxLevel: 2 // (Optional) Default is -1 (no level limit)
    // Valid levels are from [0, 1, 2, 3,...N]
    });
    editor.setForm($('#frmEdit'));
    editor.setUpdateButton($('#btnUpdate'));
    //Calling the update method
    $("#btnUpdate").click(function(){
        editor.update();
    });
    // Calling the add method
    $('#btnAdd').click(function(){
        editor.add();
    });
    $('#menu_form').on('click', function () {
        let str = editor.getString();
        $("#out").text(str);
        var items = $('#out').val();
        $.ajaxSetup({
            headers : {
                'X-CSRF-TOKEN' : document.head.querySelector('meta[name="csrf-token"]').content,
                'Content-Type' : 'application/json'
            }
        })
        $.ajax({
            url:'/admin/menu/update',
            type:'POST',
            data:JSON.stringify({
                items
            })
        })
    });
</script>
@endslot
  <div class="row">
    <div class="col-12">
        <div class=" float-right col-md-4 mb-3">
            <div class="card border-primary mb-3">
                <div class="card-header bg-primary text-white">اضافه و ویرایش</div>
                <div class="card-body">
                    <form id="frmEdit" class="form-horizontal">
                        <div class="form-group">
                            <label for="text">متن</label>
                            <input type="text" class="form-control item-menu" name="text" id="text" placeholder="متن">
                        </div>
                        <div class="form-group">
                            <label for="href">لینک</label>
                            <input type="text" class="form-control item-menu" id="href" name="href" placeholder="http://">
                        </div>
                        <div class="form-group">
                            <label for="target">نوع لینک</label>
                            <select name="target" id="target" class="form-control item-menu">
                                <option value="">-- انتخاب کنید --</option>
                                <option value="_self">معمولی</option>
                                <option value="_blank">در صفحه جدا</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="class">کلاس</label>
                            <input type="text" class="form-control item-menu" id="class" name="class" placeholder="کلاس">
                        </div>
                    </form>
                </div>
                <div class="card-footer">
                    <button type="button" id="btnUpdate" class="btn btn-primary" disabled="disabled"><i class="fas fa-sync-alt"></i> ویرایش</button>
                    <button type="button" id="btnAdd" class="btn btn-success"><i class="fas fa-plus"></i> افزودن</button>
                </div>
            </div>
        </div>
        <div class=" float-right col-md-8 mb-3">
            <div class="card border-primary mb-3">
                <div class="card-header bg-primary text-white">ایجاد زیر منو</div>
                <div class="card-body">
                    <ul id="myEditor" class="sortableLists list-group">
                        <li class="list-group-item pr-0 sortableListsOpen" style="width: auto; position: relative; top: 0px; left: 0px;">
                            <div style="overflow: auto;">
                                <span class="sortableListsOpener btn btn-success btn-sm" style="margin-right: 10px; float: none;">
                                    <i class="fa fa-minus"></i>
                                </span>
                                <i></i>&nbsp;
                                <span class="txt">خشکبار</span>
                                <div class="btn-group float-right">
                                    <a class="btn btn-secondary btn-sm btnUp btnMove clickable" href="#" style="display: none;">
                                        <i class="fa fa-angle-up clickable"></i>
                                    </a>
                                    <a class="btn btn-secondary btn-sm btnDown btnMove clickable" href="#" style="display: none;">
                                        <i class="fa fa-angle-down clickable"></i>
                                    </a>
                                    <a class="btn btn-secondary btn-sm btnIn btnMove clickable" href="#" style="display: none;">
                                        <i class="fa fa-level-up clickable"></i>
                                    </a>
                                    <a class="btn btn-secondary btn-sm btnOut btnMove clickable" href="#" style="display: none;">
                                        <i class="fa fa-level-down clickable"></i>
                                    </a>
                                    <a class="btn btn-primary btn-sm btnEdit clickable" href="#">
                                        <i class="fa fa-edit clickable"></i>
                                    </a>
                                    <a class="btn btn-danger btn-sm btnRemove clickable" href="#">
                                        <i class="fa fa-trash clickable"></i>
                                    </a>
                                </div>
                            </div>
                            <ul class="pl-0" style="padding-top: 10px; display: block;">
                                <li class="list-group-item pr-0 sortableListsOpen">
                                    <div style="overflow: auto;">
                                        <span class="sortableListsOpener btn btn-success btn-sm" style="float: none; display: inline-block; background-position: center center; background-repeat: no-repeat; margin-right: 10px;">
                                            <i class="fa fa-minus"></i>
                                        </span>
                                        <i></i>&nbsp;
                                        <span class="txt">تخمه</span>
                                        <div class="btn-group float-right">
                                            <a lass="btn btn-secondary btn-sm btnUp btnMove clickable" href="#" style="display: none;">
                                                <i class="fa fa-angle-up clickable"></i>
                                            </a>
                                            <a class="btn btn-secondary btn-sm btnDown btnMove clickable" href="#" style="">
                                                <i class="fa fa-angle-down clickable"></i>
                                            </a>
                                            <a class="btn btn-secondary btn-sm btnIn btnMove clickable" href="#" style="display: none;">
                                                <i class="fa fa-level-up clickable"></i>
                                            </a>
                                            <a class="btn btn-secondary btn-sm btnOut btnMove clickable" href="#" style="">
                                                <i class="fa fa-level-down clickable"></i>
                                            </a>
                                            <a class="btn btn-primary btn-sm btnEdit clickable" href="#">
                                                <i class="fa fa-edit clickable"></i>
                                            </a>
                                            <a class="btn btn-danger btn-sm btnRemove clickable" href="#">
                                                <i class="fa fa-trash clickable"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <ul class="pl-0" style="padding-top: 10px; display: block;">
                                        <li class="list-group-item pr-0" style="width: auto; position: relative; top: 0px; left: 0px; visibility: visible;">
                                            <div style="overflow: auto;">
                                                <i></i>&nbsp;
                                                <span class="txt">تخمه جابانی</span>
                                                <div class="btn-group float-right">
                                                    <a class="btn btn-secondary btn-sm btnUp btnMove clickable" href="#" style="display: none;">
                                                        <i class="fa fa-angle-up clickable"></i></a>
                                                ‍	<a class="btn btn-secondary btn-sm btnDown btnMove clickable" href="#" style="">
                                                        <i class="fa fa-angle-down clickable"></i></a>
                                                ‍	<a class="btn btn-secondary btn-sm btnIn btnMove clickable" href="#" style="display: none;">
                                                        <i class="fa fa-level-up clickable"></i></a>
                                                ‍	<a class="btn btn-secondary btn-sm btnOut btnMove clickable" href="#" style="">
                                                        <i class="fa fa-level-down clickable"></i></a>
                                                ‍	<a class="btn btn-primary btn-sm btnEdit clickable" href="#">
                                                        <i class="fa fa-edit clickable"></i></a>
                                                ‍	<a class="btn btn-danger btn-sm btnRemove clickable" href="#">
                                                        <i class="fa fa-trash clickable"></i></a>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item pr-0" style="width: auto; position: relative; top: 0px; left: 0px; visibility: visible;">
                                            <div style="overflow: auto;">
                                                <i></i>&nbsp;<span class="txt">تخمه کدو</span>
                                                <div class="btn-group float-right">
                                                    <a class="btn btn-secondary btn-sm btnUp btnMove clickable" href="#" style="">
                                                        <i class="fa fa-angle-up clickable"></i>
                                                    </a>
                                                    <a class="btn btn-secondary btn-sm btnDown btnMove clickable" href="#" style="display: none;">
                                                        <i class="fa fa-angle-down clickable"></i>
                                                    </a>
                                                    <a class="btn btn-secondary btn-sm btnIn btnMove clickable" href="#" style="">
                                                        <i class="fa fa-level-up clickable"></i>
                                                    </a>
                                                    <a class="btn btn-secondary btn-sm btnOut btnMove clickable" href="#" style="">
                                                        <i class="fa fa-level-down clickable"></i>
                                                    </a>
                                                    <a class="btn btn-primary btn-sm btnEdit clickable" href="#">
                                                        <i class="fa fa-edit clickable"></i>
                                                    </a>
                                                    <a class="btn btn-danger btn-sm btnRemove clickable" href="#">
                                                        <i class="fa fa-trash clickable"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li class="list-group-item pr-0 sortableListsOpen">
                                    <div style="overflow: auto;">
                                        <i></i>&nbsp;<span class="txt">پسته</span>
                                        <div class="btn-group float-right">
                                            <a class="btn btn-secondary btn-sm btnUp btnMove clickable" href="#">
                                                <i class="fa fa-angle-up clickable"></i>
                                            </a>
                                            <a class="btn btn-secondary btn-sm btnDown btnMove clickable" href="#" style="">
                                                <i class="fa fa-angle-down clickable"></i>
                                            </a>
                                            <a class="btn btn-secondary btn-sm btnIn btnMove clickable" href="#">
                                                <i class="fa fa-level-up clickable"></i>
                                            </a>
                                            <a class="btn btn-secondary btn-sm btnOut btnMove clickable" href="#" style="">
                                                <i class="fa fa-level-down clickable"></i>
                                            </a>
                                            <a class="btn btn-primary btn-sm btnEdit clickable" href="#">
                                                <i class="fa fa-edit clickable"></i>
                                            </a>
                                            <a class="btn btn-danger btn-sm btnRemove clickable" href="#">
                                                <i class="fa fa-trash clickable"></i>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item pr-0 sortableListsOpen">
                                    <div style="overflow: auto;">
                                        <i></i>&nbsp;<span class="txt">مغز بادام</span>
                                        <div class="btn-group float-right">
                                            <a class="btn btn-secondary btn-sm btnUp btnMove clickable" href="#">
                                                <i class="fa fa-angle-up clickable"></i>
                                        ‍	</a>
                                            <a class="btn btn-secondary btn-sm btnDown btnMove clickable" href="#" style="display: none;">
                                                <i class="fa fa-angle-down clickable"></i>
                                        ‍	</a>
                                            <a class="btn btn-secondary btn-sm btnIn btnMove clickable" href="#">
                                                <i class="fa fa-level-up clickable"></i>
                                        ‍	</a>
                                            <a class="btn btn-secondary btn-sm btnOut btnMove clickable" href="#" style="">
                                                <i class="fa fa-level-down clickable"></i>
                                        ‍	</a>
                                            <a class="btn btn-primary btn-sm btnEdit clickable" href="#">
                                                <i class="fa fa-edit clickable"></i>
                                        ‍	</a>
                                            <a class="btn btn-danger btn-sm btnRemove clickable" href="#">
                                                <i class="fa fa-trash clickable"></i>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <textarea id="out" class="form-control d-none" cols="50" rows="10"></textarea>
    </div>
  </div>
  @endcomponent