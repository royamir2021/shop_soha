@component('admin/layouts.content',['title'=>'فرم ایجاد گروه بندی جدید'])

@slot('breadcrumb')
<li class="breadcrumb-item "> <a href="/admin">پنل مدیریت</a> </li>
<li class="breadcrumb-item "><a href="{{route('admin.role.index')}}"> لیست گروه بندی دسترسی ها</a> </li>
<li class="breadcrumb-item active">گروه بندی جدید </li>
@endslot    
@slot('script')
<script>
$('#permissions').select2({});
</script>
@endslot
<div class="row">
    <div class="col-md-6 offset-md-3">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">فرم ایجاد گروه بندی </h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form class="form-horizontal" method="POST"  action="{{ route('admin.role.store') }}">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="input-name" class="col-sm-4 control-label">نام گروه بندی</label>
    
                        <div class="col-sm-12">
                            <input type="text" name="name" value="{{ old('name') }}" class="@error('name') is-invalid @enderror form-control" id="input-name" placeholder="نام گروه بندی به انگلیسی ">
                            @error('name')
                                <span class="invalid-feedback" role="alert"> 
                                    {{$message}}
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-label" class="col-sm-4 control-label">توضیحات</label>
                        <div class="col-sm-12">
                            <input type="text" name="label" value="{{ old('label') }}" class=" @error('label') is-invalid @enderror  form-control" id="input-label" placeholder="توضیح کوتاه ">
                            @error('label')
                                <span class="invalid-feedback" role="alert"> 
                                    {{$message}}
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="permissions" class="col-sm-4 control-label">دسترسی ها </label>
                        <select name="permissions[]" id="permissions" class="form-control @error('permissions') is-invalid @enderror " multiple>
                            @foreach (\App\Permission::all() as $permission)
                        <option value="{{ $permission->id }}">{{$permission->label}}</option>
                            @endforeach
                        </select>
                        @error('permissions')
                            <span class="invalid-feedback" role="alert"> 
                                {{$message}}
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-info">ثبت</button>
                    <a href="{{ route('admin.role.index') }}" class="btn btn-default float-left">لغو</a>
                </div>
              <!-- /.card-footer -->
            </form>
        </div>
    </div>
</div>

@endcomponent