@component('admin/layouts.content',['title'=>'ویرایش گروه بندی'])

@slot('breadcrumb')
<li class="breadcrumb-item "> <a href="/adminco">پنل مدیریت</a> </li>
<li class="breadcrumb-item "><a href="{{route('admin.role.index')}}"> لیست گروه بندی ها</a> </li>
<li class="breadcrumb-item active">ویرایش گروه بندی </li>
@endslot    
@slot('script')
    <script>
        $('#permissions').select2({
            'placeholder':'دسترسی مورد نظر را انتخاب کنید'
        });
    </script>
@endslot
<div class="row">
    <div class="col-md-8 offset-md-2">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">فرم ویرایش گروه بندی </h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            
            <form class="form-horizontal" method="POST"  action="{{ route('admin.role.update',$role->id) }}">
                @csrf
                @method('PATCH')
                <div class="card-body">
                    <div class="form-group">
                        <label for="input-name" class="col-sm-4 control-label"> نام گروه بندی </label>
    
                        <div class="col-sm-12">
                            <input type="text" name="name" value="{{ $role->name }}" class="@error('name') is-invalid @enderror form-control" id="input-name" placeholder="نام دسترسی">
                            @error('name')
                                <span class="invalid-feedback" role="alert"> 
                                    {{$message}}
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-label" class="col-sm-4 control-label">توضیح کوتاه</label>
                        <div class="col-sm-12">
                            <input type="label" name="label" value="{{ $role->label }}" class=" @error('label') is-invalid @enderror  form-control" id="input-label" placeholder="توضیح کوتا ">
                            @error('label')
                                <span class="invalid-feedback" role="alert"> 
                                    {{$message}}
                                </span>
                            @enderror
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="permissions" class="col-sm-2 control-label">دسترسی ها</label>
                        <select class="form-control @error('permissions') is-invalid @enderror " name="permissions[]" id="permissions" multiple>
                            @foreach(\App\Permission::all() as $permission)
                                <option value="{{ $permission->id }}" {{ in_array($permission->id , $role->permissions->pluck('id')->toArray()) ? 'selected' : '' }}>{{ $permission->label }}</option>
                            @endforeach
                        </select>
                        @error('permissions')
                            <span class="invalid-feedback" role="alert"> 
                                {{$message}}
                            </span>
                        @enderror
                    </div>

                </div>
                <div class="card-footer">
                    <a href="{{ route('admin.role.index') }}" class="btn btn-default">لغو</a>
                    <button type="submit" class="btn btn-success float-left">ثبت</button>
                </div>
              <!-- /.card-footer -->
            </form>
        </div>
    </div>
</div>

@endcomponent