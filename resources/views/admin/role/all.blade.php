@component('admin.layouts.content',['title'=>'لیست گروه بندی ها'])
  @slot('breadcrumb')
    <li class="breadcrumb-item"><a href="admin">خانه</a></li>
    <li class="breadcrumb-item active"><a href="#">لیست گروه بندی ها</a></li>
  @endslot
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header pb-3">
          <h3 class="card-title">لیست گروه بندی ها </h3>
          <div class="card-tools admin-search-box d-flex">
            <div class="input-group input-group-sm">
              <form action="">
                <input type="text"  value="{{request('search')}}" name="search" class="form-control float-right" placeholder="جستجو">
                <div class="input-group-append">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </form>
            </div>
            <div class="btn-group-sm mr-1">
              <a href="{{route('admin.role.create')}}" class="p-2 btn btn-info">ایجاد گروه بندی جدید</a>
          </div>
          </div>
        </div>
        <div class="card-body table-responsive p-0">
          <table class="table table-hover">
            <tbody>
              <tr>
                <th>نام گروه بندی</th>
                <th>توضیحات</th>
                <th>{{__('app.action')}}</th>
              </tr>
              @foreach ($roles as $role)
                <tr>  
                  <td>{{$role->name}}</td>
                  <td>{{$role->label}}</td>
                  <td>
                    <a href="{{route('admin.role.edit',$role->id)}}" class="btn-primary btn ml-1 float-right">{{__('app.edit')}}</a>
                      <form class="float-right" method="POST" action="{{route('admin.role.destroy',$role->id)}}">
                        @csrf
                        @method('DELETE')
                        <button class="btn-danger btn ml-1"> {{__('app.remove')}}</button>
                      </form>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="card-footer">
          {{$roles->render()}}
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
  @endcomponent