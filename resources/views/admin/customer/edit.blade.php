@component('admin.layouts.content',['title'=>'ویرایش مشتری'])
    
  @slot('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
    <li class="breadcrumb-item "><a href="{{route('admin.customer.index')}}">لیست مشتری ها</a></li>
    <li class="breadcrumb-item active">ویرایش مشتری</li>
  @endslot
  <div class="row">
    <div class="col-md-8 offset-md-2">
      <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">فرم ویرایش مشتری </h3>
        </div>
        <form action="{{route('admin.customer.update',$customer->id)}}" method="POST" class="form-horizontal">
          @csrf
          @method('PATCH')
          <div class="card-body">
            <div class="form-group col-md-6 col-12 float-right">
              <label for="input-name" class="col-12 control-label">{{__('seller.label.name')}}</label>
              <div class="col-12">
                <input type="text" name="name" value="{{$customer->name}}" class="form-control @error('name') is-invalid @enderror" id="input-name" placeholder="{{__('seller.placeholder.name')}}">
                @error('name')
                  <span class="invalid-feedback" role="alert">
                    {{$message}}
                  </span>
                @enderror
              </div>
            </div>
            <div class="form-group col-md-6 col-12 float-right">
              <label for="input-phone" class="col-12 control-label">{{__('seller.label.mobile')}} </label>
              <div class="col-12">
                <input type="text" name="phone" value="{{$customer->phone}}" class="form-control @error('phone') is-invalid @enderror " id="input-phone" placeholder="{{__('seller.placeholder.mobile')}}">
                @error('phone')
                  <span class="invalid-feedback" role="alert">
                    {{$message}}
                  </span>
                @enderror
              </div>
            </div>
            <x-state-city :state="$customer->state_id" :city="$customer->city_id"/>
            <div class="form-group">
                <label for="input-address" class="col-12 control-label">{{__('seller.label.address')}}</label>
                <div class="col-12">
                  <textarea type="text" name="address" class="form-control @error('address') is-invalid @enderror " id="input-address" placeholder="{{__('seller.placeholder.address')}}">{{$customer->address}}</textarea>
                  @error('address')
                    <span class="invalid-feedback" role="alert">
                      {{$message}}
                    </span>
                  @enderror
                </div>
            </div>
            <div class="form-group">
                <label class="col-12 control-label">{{__('app.status')}}</label>
                <div class="col-12">
                    <input type="radio" name="approved" class="form-controll" value="1" {{$customer->approved == 1 ? 'checked':""}}>
                    <label for="input-address" class="control-label">{{__('app.active')}}</label><br>
                    <input type="radio" name="approved" class="form-controll" value="0" {{$customer->approved == 0 ? 'checked':""}}>
                    <label for="input-address" class="control-label">{{__('app.deactive')}}</label>

                </div>
          </div>
          <div class="card-footer">
            <a href="{{route('admin.customer.index')}}" type="submit" class="btn btn-default">{!!__('app.back')!!}</a>
              <button type="submit" class="btn btn-info float-left">{{__('app.update')}}</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endcomponent