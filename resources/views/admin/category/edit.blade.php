@component('admin.layouts.content',['title'=>'ویرایش دسته بندی'])
    
  @slot('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
    <li class="breadcrumb-item "><a href="{{route('admin.category.index')}}">لیست دسته بندی ها</a></li>
    <li class="breadcrumb-item active">ویرایش دسته بندی</li>
  @endslot
  @slot('buttonBox')
    <a href="{{ route('admin.category.index') }}" class="btn btn-default"><i class="fa fa-reply"></i></a>
    <button type="submit" form="edit-form" class="btn btn-primary"><i class="fa fa-save"></i></button>
@endslot
  @slot('script')
    <script src="{{asset('vendor/laravel-filemanager/js/stand-alone-button.js')}}"></script>
    <script>
      $('#parent').select2({});
     
        var route_prefix = "/filemanager"; 
        $('.lfm').filemanager('image', {prefix: route_prefix});
        
        {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/stand-alone-button.js')) !!}
    </script>
  @endslot
  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex p-0">
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link active show" href="#general" data-toggle="tab">عمومی </a></li>
                  <li class="nav-item"><a class="nav-link " href="#desc" data-toggle="tab"> توضیحات</a></li>
                </ul>
            </div>
            <form action="{{route('admin.category.update',$category->id)}}" id="edit-form" method="POST" class="form-horizontal">
                @csrf
                @method('PATCH')
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane active show" id="general">
                            <div class="form-group">
                                <label for="input-name" class="col-12 control-label">{{__('app.just_name')}}</label>
                                <div class="col-12">
                                    <input type="text" name="name" value="{{$category->name}}" class="form-control @error('name') is-invalid @enderror" id="input-name">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        {{$message}}
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                            <label for="input-slug" class="col-12 control-label">لینک</label>
                            <div class="col-12">
                                <input type="text" name="slug" value="{{$category->slug}}" class="form-control @error('slug') is-invalid @enderror " id="input-slug">
                                @error('slug')
                                <span class="invalid-feedback" role="alert">
                                    {{$message}}
                                </span>
                                @enderror
                            </div>
                            </div>
                            <div class="form-group">
                                <label for="input-parent" class="col-12 control-label">دسته بندی مادر</label>
                                <div class="col-6">
                                <select name="parent_id" id="parent"  class="form-control">
                                    <option value="0">-- دسته بندی مادر -- </option>  
                                    @foreach (App\Category::all() as $cat)
                                        <option value="{{$cat->id}}" {{$cat->id == $category->parent_id ? 'selected' : ""}}>{{$cat->name}}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="input-group" style="align-items: center;">
                                <span class="input-group-btn flo ">
                                  <a class="lfm d-block" data-input="thumbnail" data-preview="holder">
                                    <div id="holder">
                                        <img style="height: 5rem;" class="img-fluid" src="{{$category->image?$category->image:asset('/img/no-image.jpg')}}" alt="">
                                    </div>
                                  </a>
                                </span>
                                <input dir="ltr" id="thumbnail" value="{{$category->image?$category->image: old('image')}}" class="form-control" type="text" name="image">
                            </div>
                            <div class="form-group">
                                <label for="input-sort_order" class="col-12 control-label">ترتیب</label>
                                <div class="col-12">
                                    <input type="text" name="sort_order" value="{{$category->sort_order}}" class="form-control @error('sort_order') is-invalid @enderror" id="input-sort_order">
                                    @error('sort_order')
                                    <span class="invalid-feedback" role="alert">
                                        {{$message}}
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-status" class="col-12 control-label">{{__('app.status')}}</label>
                                <div class="col-6">
                                    <select name="status" class="form-control">
                                        <option value="0" {{$category->status == 0 ? 'selected':""}}> غیر فعال</option>
                                        <option value="1" {{$category->status == 1 ? 'selected':""}}>فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-top" class="col-12 control-label">نمایش در منو</label>
                                <div class="col-6">
                                    <select name="top" class="form-control">
                                        <option value="0" {{$category->top == 0 ? 'selected':""}}> خیر </option>
                                        <option value="1" {{$category->top == 1 ? 'selected':""}}>بله</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane show" id="desc">
                            <div class="form-group">
                                <label for="input-description" class="col-12 control-label">توضیحات</label>
                                <div class="col-12">
                                <textarea type="text" cols="30" rows="10" name="description" class="form-control" id="input-description">{{$category->description}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-meta_title" class="col-12 control-label">متای عنوان</label>
                                <div class="col-12">
                                <input type="text" name="meta_title" value="{{$category->meta_title}}" class="form-control" id="input-meta_title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-meta_description" class="col-12 control-label">متای توضیحات</label>
                                <div class="col-12">
                                <textarea type="text" cols="3" rows="10" name="meta_description" class="form-control" id="input-meta_description">{{$category->meta_description}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
  </div>
@endcomponent