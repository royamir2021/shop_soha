@component('admin.layouts.content',['title'=>'لیست دسته بندی'])
  @slot('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
  <li class="breadcrumb-item ">لیست دسته بندی</li>
  @endslot
  @can('create-category')
    @slot('buttonBox')
        <a href="{{route('admin.category.create')}}" class="p-2 btn btn-info">ایجاد دسته بندی جدید</a>
    @endslot
@endcan
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header pb-3">
          <h3 class="card-title">لیست دسته بندی </h3>
          <div class="card-tools admin-search-box d-flex">
            <div class="input-group input-group-sm">
              <form action="">
                <input type="text"  value="{{request('search')}}" name="search" class="form-control float-right" placeholder="جستجو">
                <div class="input-group-append">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="card-body table-responsive p-0">
          @include('admin.category.subcategory',['categories'=>$categories->where('parent_id',0)])
        </div>
        <div class="card-footer">
          {{$categories->render()}}
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
  @endcomponent