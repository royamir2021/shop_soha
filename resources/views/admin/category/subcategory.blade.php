<ul class="list-group list-group-flush">
    @foreach ($categories as $category)
        <li class="list-group-item">
            <div class="d-flex">
                <p>{{$category->name}}</p>
                <div class="actions mr-2">
                    <form class="d-inline-block" action="{{route('admin.category.destroy',$category->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="border-0 badge badge-danger">{{__('app.remove')}}</button>
                    </form>
                    <a href="{{route('admin.category.edit',$category->id)}}" class="badge badge-primary">{{__('app.edit')}}</a>
                </div>
            </div>
            @if ($category->children->count())
                @include('admin.category.subcategory',['categories' => $category->children])
            @endif 
        </li>
    @endforeach
  </ul>