@component('admin.layouts.content',['title'=>'ویرایش نظر'])
    
  @slot('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
    <li class="breadcrumb-item "><a href="{{route('admin.comment.index')}}">لیست نظرات</a></li>
    <li class="breadcrumb-item active">ویرایش نظر</li>
  @endslot
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">فرم ویرایش نظر </h3>
        </div>
        <form action="{{route('admin.comment.update',$comment->id)}}" method="POST" class="form-horizontal">
          @csrf
          @method('PATCH')
          <div class="card-body">
            <div class="form-group">
              <label for="input-comment" class="col-12 control-label">متن نظر</label>
              <div class="col-12">
                <textarea name="comment" class="form-control @error('comment') is-invalid @enderror" id="input-comment">{{$comment->comment}}</textarea>
                @error('comment')
                  <span class="invalid-feedback" role="alert">
                    {{$message}}
                  </span>
                @enderror
              </div>
            </div>
          </div>
          <div class="card-footer">
            <a href="{{route('admin.comment.index')}}" type="submit" class="btn btn-default">لغو</a>
              <button type="submit" class="btn btn-info float-left">بروزرسانی</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endcomponent