@component('admin.layouts.content',['title'=>'لیست نظرات'])
  @slot('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
  <li class="breadcrumb-item ">لیست نظرات</li>
  @endslot
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header pb-3">
          <h3 class="card-title">لیست نظرات </h3>
          <div class="card-tools admin-search-box d-flex">
            <div class="input-group input-group-sm">
              <form action="">
                <input type="text"  value="{{request('search')}}" name="search" class="form-control float-right" placeholder="جستجو">
                <div class="input-group-append">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="card-body table-responsive p-0">
          <table class="table table-hover">
            <tbody>
              <tr>
                <th>{{__('app.just_name')}}</th>
                <th>متن</th>
                <th>{{__('app.status')}}</th>
                <th>{{__('app.action')}}</th>
              </tr>
              @foreach ($comments as $comment)
                <tr>  
                  <td>{{$comment->name}}</td>
                  <td>{{$comment->comment}}</td>
                  <td>
                    @if ($comment->approved == 1)
                       <span class="badge badge-success"> {{__('app.approved')}}</span> 
                    @else
                       <span class="badge badge-danger"> {{__('app.not_approved')}}</span> 
                    @endif
                  </td>
                  <td>
                    @can('edit-comment')
                      <a href="{{route('admin.comment.edit',$comment->id)}}" class="btn-primary btn ml-1 float-right">{{__('app.edit')}}</a>
                    @endcan
                    @can('delete-comment')
                        <form class="float-right" method="POST" action="{{route('admin.comment.destroy',$comment->id)}}">
                          @csrf
                          @method('DELETE')
                          <button class="btn-danger btn ml-1"> {{__('app.remove')}}</button>
                        </form>
                    @endcan
                    @can('edit-comment')
                        <form class="float-right" method="POST" action="{{route('admin.comment.approved',$comment->id)}}">
                          @csrf
                          @method('PATCH')
                          <button class="btn-success btn ml-1"> تایید</button>
                        </form>
                    @endcan
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="card-footer">
          {{$comments->render()}}
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
  @endcomponent