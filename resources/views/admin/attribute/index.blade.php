@component('admin.layouts.content',['title'=>'لیست ویژگی های تایید نشده'])
  @slot('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
  <li class="breadcrumb-item ">لیست ویژگی های تایید نشده</li>
  @endslot
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header pb-3">
          <h3 class="card-title">لیست ویژگی های تایید نشده </h3>
        </div>
        <div class="card-body table-responsive p-0">
          <table class="table table-hover">
            <tbody>
              <tr>
                <th>#</th>
                <th>{{__('app.just_name')}}</th>
                <th>{{__('app.status')}}</th>
                <th>{{__('app.action')}}</th>
              </tr>
              @foreach ($attributes as $attribute)
                <tr>  
                  <td>{{$attribute->id}}</td>
                  <td>{{$attribute->name}}</td>
                  <td><span class="border-0 badge badge-danger">تایید نشده</span></td>
  
                  <td>
                    @can('edit-attribute')
                      <a href="{{route('admin.attribute.edit',$attribute->id)}}" class="btn-primary btn ml-1 float-right">{{__('app.edit')}}</a>
                    @endcan
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
  @endcomponent