@component('admin.layouts.content',['title'=>'ویرایش ویژگی'])
  @slot('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
  <li class="breadcrumb-item "><a href="{{route('admin.attribute.index')}}">لیست ویژگی ها</a></li>
    <li class="breadcrumb-item active">ویرایش ویژگی جدید</li>
  @endslot
  @slot('buttonBox')
    <a href="{{ route('admin.attribute.index') }}" class="btn btn-default"><i class="fa fa-reply"></i></a>
    <button type="submit" form="creat-form" class="btn btn-primary"><i class="fa fa-save"></i></button>
@endslot
@slot('script')
  <script>
    $('#attributes').select2({
      'placeholder' : 'ویژگی مورد نظر را انتخاب کنید',
      tags : true
    })
  </script>
@endslot
  <div class="row">
    <div class="col-md-12">
      <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">فرم ویرایش ویژگی </h3>
        </div>
        <form action="{{route('admin.attribute.update',$attribute->id)}}" id="creat-form" method="POST" class="form-horizontal">
          @csrf
          @method('PATCH')
          <div class="card-body">
            <div class="form-group">
              <label for="input-name" class="col-12 control-label">{{__('app.attribute_name')}}</label>
              <div class="col-12">
                <input type="text" value="{{$attribute->name?$attribute->name:old('name')}}" class="form-control @error('name') is-invalid @enderror" id="input-name" placeholder="{{__('app.attribute_name')}} ">
                @error('name')
                  <span class="invalid-feedback" role="alert">
                    {{$message}}
                  </span>
                @enderror
              </div>
            </div>
            <div class="form-group">
                <label for="input-name" class="col-12 control-label">جابجا با:</label>
                <div class="col-12">
                    <select name="name" id="attributes" class="form-control @error('attribute_id') is-invalid @enderror">
                        @foreach (\App\Attribute::all() as $attr)
                          <option value="{{$attr->name}}">{{$attr->name}}</option>
                        @endforeach
                    </select>
                  @error('attribute_id')
                    <span class="invalid-feedback" role="alert">
                      {{$message}}
                    </span>
                  @enderror
                </div>
              </div>
        </div>
        </form>
      </div>
    </div>
  </div>
@endcomponent