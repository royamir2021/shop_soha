@component('admin.layouts.content',['title'=>'ایجاد دسته بندی'])
    
  @slot('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
    <li class="breadcrumb-item "><a href="{{route('admin.featured.index')}}">لیست دسته بندی ها</a></li>
    <li class="breadcrumb-item active">ایجاد دسته بندی</li>
  @endslot
  @slot('buttonBox')
    <a href="{{ route('admin.category.index') }}" class="btn btn-default"><i class="fa fa-reply"></i></a>
    <button type="submit" form="edit-form" class="btn btn-primary"><i class="fa fa-save"></i></button>
@endslot
  @slot('script')
  <script>
      $('#products').select2({});
    </script>
  @endslot
  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <form action="{{route('admin.featured.update',$featured->id)}}" id="edit-form" method="POST" class="form-horizontal">
                @csrf
                @method('PATCH')
                <div class="card-body">
                    <div class="form-group">
                        <label for="input-name" class="col-12 control-label">{{__('app.featured_name')}}</label>
                        <div class="col-12">
                            <input type="text" name="name" placeholder="{{__('app.featured_name')}}" value="{{$featured->name ? $featured->name : old('name')}}" class="form-control @error('name') is-invalid @enderror" id="input-name">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                {{$message}}
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-products" class="col-12 control-label">{{__('app.products')}}</label>
                        <div class="col-12">
                            <select name="products[]" class="form-control @error('products') is-invalid @enderror" id="products" multiple>
                                @foreach (\App\Product::all() as $product)
                                    <option value="{{$product->id}}" {{in_array($product->id,$featured->products()->pluck('id')->toArray()) ? "selected" :""}} >{{$product->name}}</option>
                                @endforeach
                            </select>
                            @error('products')
                            <span class="invalid-feedback" role="alert">
                                {{$message}}
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-status" class="col-12 control-label">{{__('app.status')}}</label>
                        <div class="col-6">
                            <select name="status" class="form-control @error('status') is-invalid @enderror">
                                <option value="0" {{$featured->status == 0 ? "selected" : ""}} > غیر فعال</option>
                                <option value="1" {{$featured->status == 1 ? "selected" : ""}}>فعال</option>
                            </select>
                            @error('status')
                            <span class="invalid-feedback" role="alert">
                                {{$message}}
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-location" class="col-12 control-label">جایگاه</label>
                        <div class="col-6">
                            <select name="location" class="form-control @error('location') is-invalid @enderror">
                                <option value="" >{{__('app.select')}} </option>
                                <option value="1" {{$featured->location == 1 ? "selected" : ""}} > بالا</option>
                                <option value="2" {{$featured->location == 2 ? "selected" : ""}} >وسط</option>
                                <option value="3" {{$featured->location == 3 ? "selected" : ""}} >پایین</option>
                            </select>
                            @error('location')
                            <span class="invalid-feedback" role="alert">
                                {{$message}}
                            </span>
                            @enderror
                        </div>
                    </div>       
                </div>
            </form>
        </div>
    </div>
  </div>
@endcomponent