@component('admin.layouts.content',['title'=>'ویرایش فروشنده'])
    
  @slot('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
    <li class="breadcrumb-item "><a href="{{route('admin.seller.index')}}">لیست فروشنده ها</a></li>
    <li class="breadcrumb-item active">ویرایش فروشنده</li>
  @endslot
  <div class="row">
    <div class="col-md-8 offset-md-2">
      <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">فرم ویرایش فروشنده </h3>
        </div>
        <form action="{{route('admin.seller.update',$user->id)}}" method="POST" class="form-horizontal">
          @csrf
          @method('PATCH')
          <div class="card-body">
            <div class="form-group col-md-6 col-12 float-right">
              <label for="input-name" class="col-12 control-label">{{__('seller.label.name')}}</label>
              <div class="col-12">
                <input type="text" name="name" value="{{$user->name}}" class="form-control @error('name') is-invalid @enderror" id="input-name" placeholder="{{__('seller.placeholder.name')}}">
                @error('name')
                  <span class="invalid-feedback" role="alert">
                    {{$message}}
                  </span>
                @enderror
              </div>
            </div>
            <div class="form-group col-md-6 col-12 float-right">
              <label for="input-phone" class="col-12 control-label">{{__('seller.label.mobile')}} </label>
              <div class="col-12">
                <input type="text" name="phone" value="{{$user->phone}}" class="form-control @error('phone') is-invalid @enderror " id="input-phone" placeholder="{{__('seller.placeholder.mobile')}}">
                @error('phone')
                  <span class="invalid-feedback" role="alert">
                    {{$message}}
                  </span>
                @enderror
              </div>
            </div>
            <div class="form-group col-md-6 col-12 float-right">
                <label for="input-shop_name" class="col-12 control-label">{{__('seller.label.shop_name')}}</label>
                <div class="col-12">
                  <input type="text" name="shop_name" value="{{$user->shop_name}}" class="form-control @error('shop_name') is-invalid @enderror " id="input-shop_name" placeholder="{{__('seller.placeholder.shop_name')}}">
                  @error('shop_name')
                    <span class="invalid-feedback" role="alert">
                      {{$message}}
                    </span>
                  @enderror
                </div>
            </div>
            <div class="form-group col-md-6 col-12 float-right">
                <label for="input-shop_phone" class="col-12 control-label">{{__('seller.label.shop_phone')}}</label>
                <div class="col-12">
                  <input type="text" name="shop_phone" value="{{$user->shop_phone}}" class="form-control @error('shop_phone') is-invalid @enderror " id="input-shop_phone" placeholder="{{__('seller.placeholder.shop_phone')}}">
                  @error('shop_phone')
                    <span class="invalid-feedback" role="alert">
                      {{$message}}
                    </span>
                  @enderror
                </div>
            </div>
            <x-state-city :state="$user->state_id" :city="$user->city_id"/>
            <div class="form-group">
                <label for="input-address" class="col-12 control-label">{{__('seller.label.address')}}</label>
                <div class="col-12">
                  <textarea type="text" name="address" class="form-control @error('address') is-invalid @enderror " id="input-address" placeholder="{{__('seller.placeholder.address')}}">{{$user->address}}</textarea>
                  @error('address')
                    <span class="invalid-feedback" role="alert">
                      {{$message}}
                    </span>
                  @enderror
                </div>
            </div>
            <div class="form-group col-md-6 col-12 float-right">
                <label for="input-bank_name" class="col-12 control-label">{{__('seller.label.bank_name')}}</label>
                <div class="col-12">
                  <input type="text" name="bank_name" value="{{$user->bank_name}}" class="form-control @error('bank_name') is-invalid @enderror " id="input-bank_name" placeholder="{{__('seller.placeholder.bank_name')}}">
                  @error('bank_name')
                    <span class="invalid-feedback" role="alert">
                      {{$message}}
                    </span>
                  @enderror
                </div>
            </div>
            <div class="form-group col-md-6 col-12 float-right">
                <label for="input-bank_account_number" class="col-12 control-label">{{__('seller.label.bank_account_number')}}</label>
                <div class="col-12">
                  <input type="text" name="bank_account_number" value="{{$user->bank_account_number}}" class="form-control @error('bank_account_number') is-invalid @enderror " id="input-bank_account_number" placeholder="{{__('seller.placeholder.bank_account_number')}}">
                  @error('bank_account_number')
                    <span class="invalid-feedback" role="alert">
                      {{$message}}
                    </span>
                  @enderror
                </div>
            </div>
            <div class="form-group col-md-6 col-12 float-right">
                <label for="input-bank_cart_number" class="col-12 control-label">{{__('seller.label.bank_cart_number')}}</label>
                <div class="col-12">
                  <input type="text" name="bank_cart_number" value="{{$user->bank_cart_number}}" class="form-control @error('bank_cart_number') is-invalid @enderror " id="input-bank_cart_number" placeholder="{{__('seller.placeholder.bank_cart_number')}}">
                  @error('bank_cart_number')
                    <span class="invalid-feedback" role="alert">
                      {{$message}}
                    </span>
                  @enderror
                </div>
            </div>
            <div class="form-group col-md-6 col-12 float-right">
                <label for="input-bank_shaba_number" class="col-12 control-label">{{__('seller.label.bank_shaba_number')}}</label>
                <div class="col-12">
                  <input type="text" name="bank_shaba_number" value="{{$user->bank_shaba_number}}" class="form-control @error('bank_shaba_number') is-invalid @enderror " id="input-bank_shaba_number" placeholder="{{__('seller.placeholder.bank_shaba_number')}}">
                  @error('bank_shaba_number')
                    <span class="invalid-feedback" role="alert">
                      {{$message}}
                    </span>
                  @enderror
                </div>
            </div>
            <div class="form-group">
                <label class="col-12 control-label">{{__('app.status')}}</label>
                <div class="col-12">
                    <input type="radio" name="approved" class="form-controll" value="1" {{$user->approved == 1 ? 'checked':""}}>
                    <label for="input-address" class="control-label">{{__('app.active')}}</label><br>
                    <input type="radio" name="approved" class="form-controll" value="0" {{$user->approved == 0 ? 'checked':""}}>
                    <label for="input-address" class="control-label">{{__('app.deactive')}}</label>

                </div>
          </div>
          <div class="card-footer">
            <a href="{{route('admin.seller.index')}}" type="submit" class="btn btn-default">{!!__('app.back')!!}</a>
              <button type="submit" class="btn btn-info float-left">{{__('app.update')}}</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endcomponent