@extends('admin.master')

@section('content')
    
<!-- Content Header (Page header) -->
<div class="content-header pt-4 pb-4">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-12">
          <h1 class="m-0 text-dark d-inline-block float-right ml-2">{{$title}}</h1>
          <ol class="breadcrumb float-right">
            {{$breadcrumb}}
          </ol>
          <div class="button-box float-left">
            {{$buttonBox ?? ''}}
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      {{$slot}}
    </div>
  </section>
  @section('script')
    {{ $script ?? ' ' }}
  @endsection
@endsection