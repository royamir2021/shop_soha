<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">پنل مدیریت</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar" style="direction: ltr">
      <div style="direction: rtl">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="" class="img-circle elevation-2" alt="User Image">
          </div>
          <div class="info">
          <a href="#" class="d-block">{{auth()->user()->name}}</a>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                 with font-awesome or any other icon font library -->
            <li class="nav-item has-treeview ">
              <a href="/admin" class="nav-link {{activeMenu('admin.')}}">
                <i class="nav-icon fa fa-dashboard"></i>
                <p>داشبورد</p>
              </a>
            </li>
            @canany(['show-product','show-brand','show-category'])
              <li class="nav-item has-treeview {{activeMenu(['admin.product.index','admin.brand.index' ,'admin.category.index'],'menu-open')}}">
                <a href="#" class="nav-link {{activeMenu(['admin.product.index','admin.brand.index','admin.category.index'])}}">
                  <i class="nav-icon fa fa-product-hunt" aria-hidden="true"></i>
                  <p>کاتالوگ<i class="right fa fa-angle-left"></i></p>
                </a>
                <ul class="nav nav-treeview">
                  @can('show-category')
                    <li class="nav-item">
                      <a href="{{route('admin.category.index')}}" class="nav-link {{activeMenu('admin.category.index')}}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>دسته بندی</p>
                      </a>
                    </li>
                  @endcan
                  @can('show-product')
                    <li class="nav-item">
                      <a href="{{route('admin.product.index')}}" class="nav-link {{activeMenu('admin.product.index')}}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>محصولات</p>
                      </a>
                    </li>
                  @endcan
                  @can('show-brand')
                    <li class="nav-item">
                      <a href="{{route('admin.brand.index')}}" class="nav-link {{activeMenu('admin.brand.index')}}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>برندها</p>
                      </a>
                    </li>
                  @endcan
                </ul>
              </li> 
            @endcanany
            @canany(['show-menu','show-banner','show-featured'])
              <li class="nav-item has-treeview {{activeMenu(['admin.additionalPage.index','admin.menu.index','admin.banner.index','admin.featured.index'],'menu-open')}}">
                <a href="#" class="nav-link {{activeMenu(['admin.additionalPage.index','admin.menu.index','admin.banner.index','admin.featured.index'])}}">
                  <i class="nav-icon fa fa-user"></i>
                  <p>
                     نمایش
                    <i class="right fa fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  @can('show-show')
                    <li class="nav-item">
                      <a href="{{route('admin.menu.index')}}" class="nav-link {{activeMenu('admin.menu.index')}}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>فهرست ها</p>
                      </a>
                    </li>
                  @endcan
                  @can('show-banner')
                  <li class="nav-item">
                    <a href="{{route('admin.banner.index')}}" class="nav-link {{activeMenu('admin.banner.index')}}">
                      <i class="fa fa-circle-o nav-icon"></i>
                      <p>بنر ها</p>
                    </a>
                  </li>
                @endcan
                @can('show-additionalPage')
                  <li class="nav-item">
                    <a href="{{route('admin.additionalPage.index')}}" class="nav-link {{activeMenu('admin.additionalPage.index')}}">
                      <i class="fa fa-circle-o nav-icon"></i>
                      <p> صفحات اضافه</p>
                    </a>
                  </li>
                @endcan
                @can('show-featured')
                  <li class="nav-item">
                    <a href="{{route('admin.featured.index')}}" class="nav-link {{activeMenu('admin.featured.index')}}">
                      <i class="fa fa-circle-o nav-icon"></i>
                      <p> برجسته ها </p>
                    </a>
                  </li>
                @endcan
                </ul>
              </li>
            @endcanany

            @can('show-customer')
              <li class="nav-item has-treeview {{activeMenu('admin.customer.index','menu-open')}}">
                <a href="#" class="nav-link {{activeMenu('admin.customer.index')}}">
                  <i class="nav-icon fa fa-user"></i>
                  <p>
                     مشتری
                    <i class="right fa fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="{{route('admin.customer.index')}}" class="nav-link {{activeMenu('admin.customer.index')}}">
                      <i class="fa fa-circle-o nav-icon"></i>
                      <p>لیست مشتری ها</p>
                    </a>
                  </li>
                </ul>
              </li>
            @endcan
            @can('show-seller')
              <li class="nav-item has-treeview {{activeMenu('admin.seller.index','menu-open')}}">
                <a href="#" class="nav-link {{activeMenu('admin.seller.index')}}">
                  <i class="nav-icon fa fa-industry"></i>
                  <p>
                    فروشنده ها
                    <i class="right fa fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="{{route('admin.seller.index')}}" class="nav-link {{activeMenu('admin.seller.index')}}">
                      <i class="fa fa-circle-o nav-icon"></i>
                      <p>لیست فروشنده ها</p>
                    </a>
                  </li>
                </ul>
              </li>
            @endcan
            @can('show-comment')
              <li class="nav-item has-treeview {{activeMenu(['admin.comment.index'],'menu-open')}}">
                <a href="#" class="nav-link {{activeMenu(['admin.comment.index'])}}">
                  <i class="nav-icon fa fa-comment" aria-hidden="true"></i>
                  <p> نظرات و سوالها<i class="right fa fa-angle-left"></i></p>
                </a>
                <ul class="nav nav-treeview">
                  @can('show-comment')
                    <li class="nav-item">
                      <a href="{{route('admin.comment.index')}}" class="nav-link {{activeMenu('admin.comment.index')}}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>نظرات</p>
                      </a>
                    </li>
                  @endcan
                </ul>
              </li> 
            @endcan
            @can('show-support')
            <li class="nav-item has-treeview ">
              <a href="{{route('admin.support.index')}}" class="nav-link {{activeMenu('admin.support.index')}}">
                <i class="nav-icon fa fa-life-ring"></i> پشتیبانی
              </a>
            </li>
            @endcan
            @canany(['show-transport','show-city','show-state','show-setting','show-user','show-role','show-permission'])
              <li class="nav-item has-treeview {{activeMenu(['admin.transport.index','admin.role.index','admin.permission.index','admin.city.index','admin.state.index','admin.user.index'],'menu-open')}}">
                <a href="#" class="nav-link {{activeMenu(['admin.transport.index','admin.role.index','admin.permission.index','admin.city.index','admin.state.index','admin.user.index'])}}">
                  <i class="nav-icon fa fa-cog" aria-hidden="true"></i>
                  <p>
                   سیستم
                    <i class="right fa fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  @can('show-setting')
                    <li class="nav-item">
                      <a href="{{route('admin.setting')}}" class="nav-link {{activeMenu('admin.setting')}}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p> تنظیمات</p>
                      </a>
                    </li>
                  @endcan
                  @can('show-user')
                    <li class="nav-item">
                      <a href="{{route('admin.user.index')}}" class="nav-link {{activeMenu('admin.user.index')}}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p> کاربران</p>
                      </a>
                    </li>
                  @endcan
                  @can('show-role')
                    <li class="nav-item">
                      <a href="{{route('admin.role.index')}}" class="nav-link {{activeMenu('admin.role.index')}}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>گروه کاربران</p>
                      </a>
                    </li>
                  @endcan
                  @can('show-state')
                    <li class="nav-item">
                      <a href="{{route('admin.state.index')}}" class="nav-link {{activeMenu('admin.state.index')}}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>استان</p>
                      </a>
                    </li>
                  @endcan
                  @can('show-city')
                    <li class="nav-item">
                      <a href="{{route('admin.city.index')}}" class="nav-link {{activeMenu('admin.city.index')}}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p> شهر</p>
                      </a>
                    </li>
                  @endcan
                  @can('show-transport')
                    <li class="nav-item">
                      <a href="{{route('admin.transport.index')}}" class="nav-link {{activeMenu('admin.transport.index')}}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p> شرکتهای حمل و نقل</p>
                      </a>
                    </li>
                  @endcan
                  @can('show-permission')
                    <li class="nav-item">
                      <a href="{{route('admin.permission.index')}}" class="nav-link {{activeMenu('admin.permission.index')}}">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p> دسترسی ها</p>
                      </a>
                    </li>
                  @endcan
                  
                </ul>
              </li>    
            @endcanany
            
          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
    </div>
    <!-- /.sidebar -->
  </aside>