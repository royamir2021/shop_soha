@component('admin.layouts.content',['title'=>'ایجاد دسترسی'])
    
  @slot('breadcrumb')
    <li class="breadcrumb-item"><a href="admin">خانه</a></li>
    <li class="breadcrumb-item "><a href="admin/permission">لیست دسترسی ها</a></li>
    <li class="breadcrumb-item active"><a href="#">ایجاد دسترسی جدید</a></li>
  @endslot
<div class="row">
    <div class="col-md-6 offset-md-3">
        <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">فرم ایجاد دسترسی جدید</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{route('admin.permission.store')}}" method="POST" class="form-horizontal">
              @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="input-name" class="col-12 control-label">نام دسترسی</label>
                        <div class="col-12">
                            <input type="text" name="name" value="{{old('name')}}" class="form-control @error('name') is-invalid @enderror" id="input-name" placeholder="نام دسترسی">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                {{$message}}
                            </span>
                        @enderror
                        </div>
                        
                  </div>
                  <div class="form-group">
                        <label for="input-label" class="col-12 control-label">توضیح کوتاه</label>
                        <div class="col-12">
                        <input type="text" name="label" value="{{old('label')}}" class="form-control @error('label') is-invalid @enderror " id="input-label" placeholder="توضیح کوتاه">
                            @error('label')
                                <span class="invalid-feedback" role="alert">
                                    {{$message}}
                                </span>
                                
                            @enderror
                        </div>
                  </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <a href="{{route('admin.permission.index')}}" type="submit" class="btn btn-default">لغو</a>
                  <button type="submit" class="btn btn-info float-left">ثبت</button>
              </div>
              <!-- /.card-footer -->
            </form>
        </div>
      <!-- /.card -->
    </div>
</div>
  @endcomponent