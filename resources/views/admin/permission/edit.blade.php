@component('admin.layouts.content',['title'=>'ویرایش دسترسی'])
    
  @slot('breadcrumb')
    <li class="breadcrumb-item"><a href="admin">خانه</a></li>
    <li class="breadcrumb-item "><a href="admin/permission">لیست دسترسی</a></li>
    <li class="breadcrumb-item active"><a href="#">ویرایش دسترسی</a></li>
  @endslot
<div class="row">
    <div class="col-md-6 offset-md-3">
        <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">فرم ویرایش دسترسی </h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{route('admin.permission.update',$permission->id)}}" method="POST" class="form-horizontal">
              @csrf
              @method('PATCH')
                <div class="card-body">
                    <div class="form-group">
                        <label for="input-name" class="col-12 control-label">{{__('app.just_name')}}</label>
                        <div class="col-12">
                            <input type="text" name="name" value="{{$permission->name}}" class="form-control @error('name') is-invalid @enderror" id="input-name" placeholder="نام و نام خانوادگی">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                {{$message}}
                            </span>
                        @enderror
                        </div>
                        
                  </div>
                  <div class="form-group">
                        <label for="input-label" class="col-12 control-label">توضیحات کوتاه </label>
                        <div class="col-12">
                        <input type="text" name="label" value="{{$permission->label}}" class="form-control @error('label') is-invalid @enderror " id="input-label" placeholder="شماره موبایل">
                            @error('label')
                                <span class="invalid-feedback" role="alert">
                                    {{$message}}
                                </span>
                                
                            @enderror
                        </div>
                  </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <a href="{{route('admin.permission.index')}}" type="submit" class="btn btn-default">لغو</a>
                  <button type="submit" class="btn btn-info float-left">بروزرسانی</button>
              </div>
              <!-- /.card-footer -->
            </form>
        </div>
      <!-- /.card -->
    </div>
</div>
  @endcomponent