@component('admin.layouts.content',['title'=>'لیست دسترسی ها'])
  @slot('breadcrumb')
    <li class="breadcrumb-item"><a href="admin">خانه</a></li>
    <li class="breadcrumb-item active"><a href="#">لیست دسترسی ها</a></li>
  @endslot
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header pb-3">
          <h3 class="card-title">لیست دسترسی ها </h3>
          <div class="card-tools admin-search-box d-flex">
            <div class="input-group input-group-sm">
              <form action="">
                <input type="text"  value="{{request('search')}}" name="search" class="form-control float-right" placeholder="جستجو">
                <div class="input-group-append">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </form>
            </div>
            <div class="btn-group-sm mr-1">
              @can('create-permission')
              <a href="{{route('admin.permission.create')}}" class="p-2 btn btn-info">ایجاد دسترسی جدید</a>
              @endcan
          </div>
          </div>
        </div>
        <div class="card-body table-responsive p-0">
          @can('show-permission')
            <table class="table table-hover">
              <tbody>
                <tr>
                  <th>نام دسترسی</th>
                  <th>توضیحات</th>
                  <th>{{__('app.action')}}</th>
                </tr>
                @foreach ($permissions as $permission)
                  <tr>  
                    <td>{{$permission->name}}</td>
                    <td>{{$permission->label}}</td>
                    <td>
                      @can('edit-permission')
                      <a href="{{route('admin.permission.edit',$permission->id)}}" class="btn-primary btn ml-1 float-right">{{__('app.edit')}}</a>
                      @endcan
                      @can('delete-permission')
                        <form class="float-right" method="POST" action="{{route('admin.permission.destroy',$permission->id)}}">
                          @csrf
                          @method('DELETE')
                          <button class="btn-danger btn ml-1"> {{__('app.remove')}}</button>
                        </form>
                      @endcan
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          @endcan
        </div>
        <div class="card-footer">
          {{$permissions->render()}}
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
  @endcomponent