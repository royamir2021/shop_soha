@component('admin.layouts.content',['title'=>'ایجاد استان'])
    
  @slot('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
  <li class="breadcrumb-item "><a href="{{route('admin.state.index')}}">لیست استان ها</a></li>
    <li class="breadcrumb-item active">ایجاد استان جدید</li>
  @endslot
  @slot('buttonBox')
    <a href="{{ route('admin.state.index') }}" class="btn btn-default"><i class="fa fa-reply"></i></a>
    <button type="submit" form="creat-form" class="btn btn-primary"><i class="fa fa-save"></i></button>
@endslot
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">فرم ایجاد استان جدید</h3>
        </div>
        <form action="{{route('admin.state.store')}}" id="creat-form" method="POST" class="form-horizontal">
          @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="input-name" class="col-12 control-label">{{__('app.state_name')}}</label>
              <div class="col-12">
                <input type="text" name="name" value="{{old('name')}}" class="form-control @error('name') is-invalid @enderror" id="input-name" placeholder="{{__('app.state_name')}} ">
                @error('name')
                  <span class="invalid-feedback" role="alert">
                    {{$message}}
                  </span>
                @enderror
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endcomponent