@component('admin.layouts.content',['title'=>'لیست استان ها'])
  @slot('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
  <li class="breadcrumb-item ">لیست استان ها</li>
  @endslot
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header pb-3">
          <h3 class="card-title">لیست استان ها </h3>
          <div class="card-tools admin-search-box d-flex">
            <div class="input-group input-group-sm">
              <form action="">
                <input type="text"  value="{{request('search')}}" name="search" class="form-control float-right" placeholder="جستجو">
                <div class="input-group-append">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </form>
            </div>
            <div class="btn-group-sm mr-1">
              @can('create-state')
              <a href="{{route('admin.state.create')}}" class="p-2 btn btn-info">ایجاد استان جدید</a>
              @endcan
          </div>
          </div>
        </div>
        <div class="card-body table-responsive p-0">
          <table class="table table-hover">
            <tbody>
              <tr>
                <th>#</th>
                <th>{{__('app.just_name')}}</th>
                <th>{{__('app.action')}}</th>
              </tr>
              @foreach ($states as $state)
                <tr>  
                  <td>{{$state->id}}</td>
                  <td>{{$state->name}}</td>
  
                  <td>
                    @can('edit-state')
                      <a href="{{route('admin.state.edit',$state->id)}}" class="btn-primary btn ml-1 float-right">{{__('app.edit')}}</a>
                    @endcan
                    @can('delete-state')
                      
                        <form class="float-right" method="POST" action="{{route('admin.state.destroy',$state->id)}}">
                          @csrf
                          @method('DELETE')
                          <button class="btn-danger btn ml-1"> {{__('app.remove')}}</button>
                        </form>
                      
                    @endcan
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="card-footer">
          {{$states->render()}}
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
  @endcomponent