@component('admin/layouts.content',['title'=>'فرم ایجاد صفحه جدید'])

@slot('breadcrumb')
<li class="breadcrumb-item "> <a href="{{route('admin.')}}">پنل مدیریت</a> </li>
<li class="breadcrumb-item "><a href="{{route('admin.additionalPage.index')}}"> لیست صفحات اضافه</a> </li>
<li class="breadcrumb-item active">صفحه جدید </li>
@endslot    
@slot('script')
<script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>
<script>
    var options = {
      filebrowserImageBrowseUrl: '/filemanager?type=Images',
      filebrowserImageUploadUrl: '/filemanager/upload?type=Images&_token=',
      filebrowserBrowseUrl: '/filemanager?type=Files',
      filebrowserUploadUrl: '/filemanager/upload?type=Files&_token='
    };
    CKEDITOR.replace('description2',options);
    </script>
@endslot
@slot('buttonBox')
    <a href="{{ route('admin.additionalPage.index') }}" class="btn btn-default"><i class="fa fa-reply"></i></a>
    <button type="submit" form="creat-form" class="btn btn-primary"><i class="fa fa-save"></i></button>
@endslot
<div class="row">
    <div class="col-12">
        <div class="card">
          <form class="form-horizontal" id="creat-form" method="POST"  action="{{ route('admin.additionalPage.store') }}">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="input-name" class="col-sm-4 control-label">نام صفحه</label>
                    <div class="col-sm-12">
                        <input type="text" name="name" value="{{ old('name') }}" class="@error('name') is-invalid @enderror form-control" id="input-name" placeholder="نام صفحه  ">
                        @error('name')
                            <span class="invalid-feedback" role="alert"> 
                                {{$message}}
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="input-slug " class="col-sm-4 control-label">لینک</label>
                    <div class="col-sm-12">
                        <input type="text" name="slug" value="{{ old('slug') }}" class=" @error('slug') is-invalid @enderror  form-control" id="input-slug" placeholder="لینک صفحه به انگلیسی بدون فاصله">
                        @error('slug')
                            <span class="invalid-feedback" role="alert"> 
                                {{$message}}
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="input-description" class="col-sm-4 control-label">توضیحات </label>
                    <div class="col-sm-12">
                        <textarea name="description" id="description2" cols="30" rows="10" class="@error('description') is-invalid @enderror form-control">{{ old('description') }}</textarea>
                        @error('description')
                            <span class="invalid-feedback" role="alert"> 
                                {{$message}}
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="input-meta_title" class="col-sm-4 control-label">متای عنوان </label>
                    <div class="col-sm-12">
                        <input type="text" name="meta_title" value="{{ old('meta_title') }}" class="@error('meta_title') is-invalid @enderror form-control" id="input-meta_title" placeholder="متای عنوان">
                        @error('meta_title')
                            <span class="invalid-feedback" role="alert"> 
                                {{$message}}
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="input-meta_description" class="col-sm-4 control-label">متای توضیحات </label>
                    <div class="col-sm-12">
                        <textarea name="meta_description" id="meta_description" cols="30" rows="3" class="@error('meta_description') is-invalid @enderror form-control">{{ old('meta_description') }}</textarea>
                        @error('meta_description')
                            <span class="invalid-feedback" role="alert"> 
                                {{$message}}
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="status" class="col-sm-4 control-label">وضعیت </label>
                    <select name="status" id="status" class="form-control @error('status') is-invalid @enderror ">
                        <option value="">{{__('app.select')}}</option>
                        <option value="0">{{__('app.deactive')}}</option>
                        <option value="1">{{__('app.active')}}</option>
                    </select>
                    @error('status')
                            <span class="invalid-feedback" role="alert"> 
                                {{$message}}
                            </span>
                        @enderror
                </div>
            </div>
          </form>
        </div>
    </div>
</div>

@endcomponent