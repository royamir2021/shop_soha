@component('admin.layouts.content',['title'=>'ایجاد شرکت'])
    
  @slot('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
    <li class="breadcrumb-item "><a href="{{route('admin.transport.index')}}">لیست شرکت ها</a></li>
    <li class="breadcrumb-item active">ایجاد شرکت جدید</li>
  @endslot
@slot('buttonBox')
    <a href="{{ route('admin.transport.index') }}" class="btn btn-default"><i class="fa fa-reply"></i></a>
    <button type="submit" form="creat-form" class="btn btn-primary"><i class="fa fa-save"></i></button>
@endslot
@slot('script')
  <script>
    $('.city_select').select2({
      'placeholder' : ' شهر را مشخص کنید'
    })
    $('#add_cities').click(function(){
      let cities = $('#cities');
      let id = cities.children().length;
      let nameCities = $('#nameCities').data('cities');
      cities.append(
        createNewCity({
          cities : nameCities,
          id
        })
      );
      $('.city_select').select2({
        'placeholder' : ' شهر را مشخص کنید'
      })
    });

    let createNewCity = ({cities,id})=>{
        return `
        <div class="row" id="city-${id}">
            <div class="col-5">
                <div class="form-group">
                        <label> شهر</label>
                        <select name="cities[${id}][city_id]" class="city_select form-control">
                        <option value="">انتخاب کنید</option>
                        ${   
                          Object.entries(cities).map(function(value) {
                            return `<option value="${value[1]}">${value[0]}</option>`
                          })
                        }
                        </select>
                </div>
            </div>
        <div class="col-5">
            <div class="form-group">
                <label> قیمت</label>
                <input class="form-control " type="text" name="cities[${id}][price]">
            </div>
        </div>
        <div class="col-2">
            <div class="form-group">
                <label> اقدامات</label> <br>
                <button type="button" class="btn btn-sm btn-warning" onclick="document.getElementById('city-${id}').remove()">{{__('app.remove')}}</button>
            </div>
        </div>
        </tr>`;
    }
  </script>
@endslot
<div id="nameCities" data-cities = "{{json_encode(\App\City::all()->pluck('id','name'))}}"></div>
  <div class="row">
    <div class="col-md-12">
      <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">فرم ایجاد شرکت جدید</h3>
        </div>
        <form action="{{route('admin.transport.store')}}" id="creat-form" method="POST" class="form-horizontal">
          @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="input-name" class="col-12 control-label">{{__('app.just_name')}}</label>
              <div class="col-12">
                <input type="text" name="name" value="{{old('name')}}" class="form-control @error('name') is-invalid @enderror" id="input-name" placeholder="{{__('app.just_name')}} ">
                @error('name')
                  <span class="invalid-feedback" role="alert">
                    {{$message}}
                  </span>
                @enderror
              </div>
            </div>
            <div class="form-group">
                <label for="input-name" class="col-12 control-label">{{__('app.status')}}</label>
                <div class="col-12">
                    <select name="status" class="form-control">
                        <option value="">{{__('app.select')}}</option>
                        <option value="0">{{__('app.deactive')}}</option>
                        <option value="1">{{__('app.active')}}</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div id="cities"></div>
                <button class="btn btn-sm btn-success mt-3 mb-3" type="button" id="add_cities">شهر جدید</button>
            </div>
        </div>
        </form>
      </div>
    </div>
  </div>
@endcomponent