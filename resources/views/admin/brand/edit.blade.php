@component('admin.layouts.content',['title'=>'ویرایش برند'])
    
  @slot('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
  <li class="breadcrumb-item "><a href="{{route('admin.brand.index')}}">لیست برند ها</a></li>
    <li class="breadcrumb-item active">ویرایش برند جدید</li>
  @endslot
  @slot('buttonBox')
    <a href="{{ route('admin.brand.index') }}" class="btn btn-default"><i class="fa fa-reply"></i></a>
    <button type="submit" form="edit-form" class="btn btn-primary"><i class="fa fa-save"></i></button>
@endslot
  <div class="row">
    <div class="col-md-12">
      <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">فرم ویرایش برند جدید</h3>
        </div>
        <form action="{{route('admin.brand.update',$brand->id)}}" id="edit-form" method="POST" class="form-horizontal">
          @csrf
          @method('PATCH')
          <div class="card-body">
            <div class="row mb-3">
              <div class="col">
                <label for="input-brand" class="col-12 control-label">نام فارسی برند</label>
                <div class="col-12">
                  <input type="text" name="brand" value="{{$brand->brand ? $brand->brand:old('brand')}}" class="form-control @error('brand') is-invalid @enderror" id="input-name" placeholder="نام فارسی برند ">
                  @error('brand')
                    <span class="invalid-feedback" role="alert">
                      {{$message}}
                    </span>
                  @enderror
                </div>
              </div> 
              <div class="col">
                <label for="input-english_brand" class="col-12 control-label">نام انگلیسی برند</label>
                <div class="col-12">
                  <input type="text" name="english_brand" value="{{$brand->english_brand ? $brand->english_brand:old('brand')}}" class="form-control @error('english_brand') is-invalid @enderror" id="input-name" placeholder="نام انگلیسی ">
                  @error('english_brand')
                    <span class="invalid-feedback" role="alert">
                      {{$message}}
                    </span>
                  @enderror
                </div>
              </div> 
            </div> 
          </div>
        </form>
      </div>
    </div>
  </div>
@endcomponent