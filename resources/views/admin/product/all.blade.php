@component('admin.layouts.content',['title'=>'لیست محصولات'])
  @slot('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
  <li class="breadcrumb-item ">لیست محصولات</li>
  @endslot
  @slot('buttonBox')
    @can('create-product')
      <a href="{{route('admin.product.create')}}" class="btn btn btn-success" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="افزودن محصول جدید">
        <i class="fa fa-plus"></i>
      </a>
    @endcan
    @endslot
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header pb-3">
          <h3 class="card-title">لیست محصولات </h3>
          <div class="card-tools admin-search-box d-flex">
            <div class="input-group input-group-sm">
              <form action="">
                <input type="text"  value="{{request('search')}}" name="search" class="form-control float-right" placeholder="جستجو">
                <div class="input-group-append">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </form>
            </div>
            
          </div>
        </div>
        <div class="card-body table-responsive p-0">
          <table class="table table-hover">
            <tbody>
              <tr>
                <th>#</th>
                <th>{{__('app.just_name')}}</th>
                <th>{{__('app.status')}}</th>
                <th>{{__('app.action')}}</th>
              </tr>
              @foreach ($products as $product)
                <tr>  
                  <td>{{$product->id}}</td>
                  <td>{{$product->name}}</td>
                  <td>
                    @foreach (\App\Status::all() as $status)
                      @if($status->id == $product->status_id)
                        {{$status->name}}
                      @endif
                    @endforeach
                  </td>
                  <td>
                    @can('edit-product')
                      <a href="{{route('admin.product.edit',$product->id)}}" class="btn-primary btn ml-1 float-right" data-toggle="tooltip" data-original-title="ویرایش"><i class="fa fa-pencil"></i></a>
                    @endcan
                    @can('delete-product')
                      <form class="float-right" method="POST" action="{{route('admin.product.destroy',$product->id)}}">
                        @csrf
                        @method('DELETE')
                        <button class="btn-danger btn ml-1" data-toggle="tooltip" data-original-title="حذف"> <i class="fa fa-trash-o"></i></button>
                      </form>
                    @endcan
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="card-footer">
          {{-- {{$products->render()}} --}}
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
  @endcomponent