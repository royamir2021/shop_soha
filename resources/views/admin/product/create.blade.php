@component('admin/layouts.content',['title'=>'فرم ایجاد محصول جدید'])

@slot('breadcrumb')
<li class="breadcrumb-item "> <a href="{{route('admin.')}}">پنل مدیریت</a> </li>
<li class="breadcrumb-item "><a href="{{route('admin.product.index')}}"> لیست محصولات</a> </li>
<li class="breadcrumb-item active">محصول جدید </li>
@endslot    
@slot('script')
<script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>
<script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
<script>
    var options = {
      filebrowserImageBrowseUrl: '/filemanager?type=Images',
      filebrowserImageUploadUrl: '/filemanager/upload?type=Images&_token=',
      filebrowserBrowseUrl: '/filemanager?type=Files',
      filebrowserUploadUrl: '/filemanager/upload?type=Files&_token='
    };
    CKEDITOR.replace('description',options);
    var route_prefix = "/filemanager";
    $('.lfm').filemanager('image', {prefix: route_prefix});
        {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/stand-alone-button.js')) !!}
  </script>
    <script>
        
        $('#categories').select2({
            'placeholder' : 'دسترسی مورد نظر را انتخاب کنید'
        })
        $('#status').select2({
            'placeholder' : 'وضعیت کالا را مشخص کنید'
        })

        $('.attribute-select').select2({ tags : true });

        let changeAttributeValues = (event , id) => {
            let valueBox = $(`select[name='attributes[${id}][value]']`);
            $.ajaxSetup({
                headers : {
                    'X-CSRF-TOKEN' : document.head.querySelector('meta[name="csrf-token"]').content,
                    'Content-Type' : 'application/json'
                }
            })
            $.ajax({
                type : 'POST',
                url : '/admin/attribute/values',
                data : JSON.stringify({
                    name : event.target.value
                }),
                success : function(data) {
                    valueBox.html(`
                        <option selected>انتخاب کنید</option>
                        ${
                        data.map(function (item) {
                            return `<option value="${item}">${item}</option>`
                        })
                    }
                    `);

                    $('.attribute-select').select2({ tags : true });
                }
            });
        }

        let createNewAttr = ({ attributes , id }) => {
            return `
                <div class="row" id="attribute-${id}">
                    <div class="col-5">
                        <div class="form-group">
                                <label>عنوان ویژگی</label>
                                <select name="attributes[${id}][name]" onchange="changeAttributeValues(event, ${id});" class="attribute-select form-control">
                                <option value="">انتخاب کنید</option>
                                ${
                                    attributes.map(function(item) {
                                        return `<option value="${item}">${item}</option>`
                                    })
                                }
                                </select>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="form-group">
                                <label>مقدار ویژگی</label>
                                <select name="attributes[${id}][value]" class="attribute-select form-control">
                                    <option value="">انتخاب کنید</option>
                                </select>
                        </div>
                    </div>
                        <div class="col-2">
                        <label >{{__('app.action')}}</label>
                        <div>
                            <button type="button" class="btn btn-sm btn-warning" onclick="document.getElementById('attribute-${id}').remove()">{{__('app.remove')}}</button>
                        </div>
                    </div>
                </div>
            `
        }

        $('#add_product_attribute').click(function() {
            let attributesSection = $('#attribute_section');
            let id = attributesSection.children().length;
            let attributes = $('#attributes').data('attributes');
            attributesSection.append(
                createNewAttr({
                    attributes : attributes,
                    id
                })
            );

            $('.attribute-select').select2({ tags : true });
        });

        // Upload Image 

        $('#add_product_image').click(function(){
            let images = $('#gallery');
            let id = images.children().length;
            images.append(
                createNewImage({
                    id
                })
            );
            runFileManager();
        });

        function runFileManager(){
            $('.lfm').filemanager('image', {prefix: route_prefix});
            {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/stand-alone-button.js')) !!}
        }
        let createNewImage = ({id})=>{
            return `
            <div class="input-group" style="align-items:center" id="image-row-${id}">
            <div class="col-md-9">
                <span class="input-group-btn float-right flo">
                    <a class="lfm" data-input="thumbnail-${id}" data-preview="holder-${id}" class="btn btn-primary">
                        <div id="holder-${id}" class="ml-3">
                            <img style="height: 5rem;" class="img-fluid" src="{{asset('/img/no-image.jpg')}}" alt="">
                        </div>
                    </a>
                </span>
                <input id="thumbnail-${id}" class="form-control mt-3 w-75" type="text" name="images[${id}]">
            </div>
            <div class="col-md-3">
                <button type="button" class="btn btn-sm btn-warning" onclick="document.getElementById('image-row-${id}').remove()">{{__('app.remove')}}</button>
            </div>
            </div> <hr>`;
        }
    </script>
@endslot
@slot('buttonBox')
    <a href="{{ route('admin.product.index') }}" class="btn btn-default"><i class="fa fa-reply"></i></a>
    <button type="submit" form="creat-form" class="btn btn-primary"><i class="fa fa-save"></i></button>
@endslot
<div id="attributes" data-attributes="{{json_encode(\App\Attribute::all()->pluck('name'))}}""></div>

<div class="row">
    <div class="col-12">
        <div class="card">
          <div class="card-header d-flex p-0">
            <ul class="nav nav-pills ml-auto p-2">
              <li class="nav-item"><a class="nav-link active show" href="#general" data-toggle="tab">عمومی </a></li>
              <li class="nav-item"><a class="nav-link " href="#seo" data-toggle="tab"> سئو</a></li>
              <li class="nav-item"><a class="nav-link " href="#links" data-toggle="tab"> لینک</a></li>
              <li class="nav-item"><a class="nav-link " href="#attribute-pannel" data-toggle="tab">مشخصات</a></li>
              <li class="nav-item"><a class="nav-link" href="#images" data-toggle="tab"> عکس</a></li>
            </ul>
          </div>
          <form class="form-horizontal" id="creat-form" method="POST"  action="{{ route('admin.product.store') }}">
            @csrf
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane active show" id="general">
                        <div class="form-group">
                            <label for="input-name" class="col-sm-4 control-label">نام محصول</label>
                            <div class="col-sm-12">
                                <input type="text" name="name" value="{{ old('name') }}" class="@error('name') is-invalid @enderror form-control" id="input-name" placeholder="نام محصول   ">
                                @error('name')
                                    <span class="invalid-feedback" role="alert"> 
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-slug " class="col-sm-4 control-label">لینک</label>
                            <div class="col-sm-12">
                                <input type="text" name="slug" value="{{ old('slug') }}" class=" @error('slug') is-invalid @enderror  form-control" id="input-slug" placeholder="لینک محصول به انگلیسی بدون فاصله">
                                @error('slug')
                                    <span class="invalid-feedback" role="alert"> 
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-short_description" class="col-sm-4 control-label">توضیح کوتاه </label>
                            <div class="col-sm-12">
                                <textarea type="text" name="short_description" class="@error('short_description') is-invalid @enderror form-control" id="input-short_description" placeholder="توضیح کوتاه">{{ old('short_description') }}</textarea>
                                @error('short_description')
                                    <span class="invalid-feedback" role="alert"> 
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group d-none">
                            <label for="input-price" class="col-sm-4 control-label">{{__('app.price')}} </label>
                            <div class="col-sm-12">
                                <input type="text" name="price" value="{{ old('price') }}" class="@error('price') is-invalid @enderror form-control" id="input-price" placeholder="{{__('app.price')}}">
                                @error('price')
                                    <span class="invalid-feedback" role="alert"> 
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="categories" class="col-sm-4 control-label">دسته بندی ها </label>
                            <select name="categories[]" id="categories" class="form-control @error('categories') is-invalid @enderror" multiple>
                                @foreach (\App\Category::all() as $category)
                                    <option value="{{ $category->id }}">{{$category->name}}</option>
                                @endforeach
                            </select>
                            @error('categories')
                                <span class="invalid-feedback" role="alert"> 
                                    {{$message}}
                                </span>
                            @enderror
                        </div>
                        <div class="form-group d-none">
                            <label for="status" class="col-sm-4 control-label">وضعیت </label>
                            <select name="status_id" id="status" class="form-control @error('status_id') is-invalid @enderror ">
                                @foreach (\App\Status::all() as $status)
                            <option value="{{ $status->id }}">{{$status->name}}</option>
                                @endforeach
                            </select>
                            @error('status_id')
                                <span class="invalid-feedback" role="alert"> 
                                    {{$message}}
                                </span>
                            @enderror
                        </div>
                        <div class="form-group d-none">
                            <label for="input-inventory" class="col-sm-4 control-label">تعداد </label>
                            <div class="col-sm-12">
                                <input type="text" name="inventory" value="{{ old('inventory') }}" class="@error('inventory') is-invalid @enderror form-control" id="input-inventory" placeholder="تعداد">
                                @error('inventory')
                                    <span class="invalid-feedback" role="alert"> 
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane " id="seo">
                        <div class="form-group">
                            <label for="input-description" class="col-sm-4 control-label">توضیحات </label>
                            <div class="col-sm-12">
                                <textarea name="description" id="description" cols="30" rows="10" class="@error('description') is-invalid @enderror form-control">{{ old('description') }}</textarea>
                                @error('description')
                                    <span class="invalid-feedback" role="alert"> 
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-meta_title" class="col-sm-4 control-label">متای عنوان </label>
                            <div class="col-sm-12">
                                <input type="text" name="meta_title" value="{{ old('meta_title') }}" class="@error('meta_title') is-invalid @enderror form-control" id="input-meta_title" placeholder="متای عنوان">
                                @error('meta_title')
                                    <span class="invalid-feedback" role="alert"> 
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-meta_description" class="col-sm-4 control-label">متای توضیحات </label>
                            <div class="col-sm-12">
                                <textarea name="meta_description" id="meta_description" cols="30" rows="3" class="@error('meta_description') is-invalid @enderror form-control">{{ old('meta_description') }}</textarea>
                                @error('meta_description')
                                    <span class="invalid-feedback" role="alert"> 
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane " id="links"></div>
                    <div class="tab-pane " id="attribute-pannel">
                        <h6>ویژگی محصول</h6>
                        <hr>
                        <div id="attribute_section">

                        </div>
                        <button class="btn btn-sm btn-success" type="button" id="add_product_attribute">ویژگی جدید</button>
                    </div>
                    <div class="tab-pane" id="images">

                        <div id="gallery">
                            <div class="input-group" style="align-items: center;">
                                <span class="input-group-btn flo ">
                                  <a class="lfm d-block" data-input="thumbnail" data-preview="holder">
                                    <div id="holder">
                                        <img style="height: 5rem;" class="img-fluid" src="{{asset('/img/no-image.jpg')}}" alt="">
                                    </div>
                                  </a>
                                </span>
                                <input dir="ltr" id="thumbnail" value="{{ old('image')}}" class="form-control" type="text" name="image">
                            </div>
                            <hr>
                        </div>
                        <button class="btn btn-sm btn-success mt-3 mb-3" type="button" id="add_product_image">عکس جدید</button>
                         
                    </div>
                </div>
            </div>
          </form>
        </div>
    </div>
</div>

@endcomponent