@component('admin.layouts.content',['title'=>'لیست کاربران'])
  @slot('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
  <li class="breadcrumb-item ">لیست کاربران</li>
  @endslot
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header pb-3">
          <h3 class="card-title">لیست کاربران </h3>
          <div class="card-tools admin-search-box d-flex">
            <div class="input-group input-group-sm">
              <form action="">
                <input type="text"  value="{{request('search')}}" name="search" class="form-control float-right" placeholder="جستجو">
                <div class="input-group-append">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </form>
            </div>
            <div class="btn-group-sm mr-1">
              @can('create-user')
              <a href="{{route('admin.user.create')}}" class="p-2 btn btn-info">ایجاد کاربر جدید</a>
              @endcan
          </div>
          </div>
        </div>
        <div class="card-body table-responsive p-0">
          <table class="table table-hover">
            <tbody>
              <tr>
                <th>{{__('app.just_name')}}</th>
                <th>شماره موبایل</th>
                <th>نوع کاربری</th>
                <th>{{__('app.action')}}</th>
              </tr>
              @foreach ($users as $user)
                <tr>  
                  <td>{{$user->name}}</td>
                  <td>{{$user->phone}}</td>
                  <td>
                    @foreach (\App\UserGroup::all() as $group)
                      {{$user->group_id == $group->id ? $group->name :''}}
                    @endforeach
                  </td>
                  <td>
                    @can('edit-user')
                      <a href="{{route('admin.user.edit',$user->id)}}" class="btn-primary btn ml-1 float-right">{{__('app.edit')}}</a>
                    @endcan
                    @can('delete-user')
                      @if ((auth()->user()->id != $user->id))
                        <form class="float-right" method="POST" action="{{route('admin.user.destroy',$user->id)}}">
                          @csrf
                          @method('DELETE')
                          <button class="btn-danger btn ml-1"> {{__('app.remove')}}</button>
                        </form>
                      @endif
                    @endcan
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="card-footer">
          {{$users->render()}}
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
  @endcomponent