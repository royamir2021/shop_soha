@component('admin.layouts.content',['title'=>'ایجاد کاربر'])
    
  @slot('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
  <li class="breadcrumb-item "><a href="{{route('admin.user.index')}}">لیست کاربران</a></li>
    <li class="breadcrumb-item active">ایجاد کاربر جدید</li>
  @endslot
  @slot('script')
    <script>
      $('#permissions').select2({});
      $('#roles').select2({});
    </script>
  @endslot
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">فرم ایجاد کاربر جدید</h3>
        </div>
        <form action="{{route('admin.user.store')}}" method="POST" class="form-horizontal">
          @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="input-name" class="col-12 control-label">{{__('app.just_name')}}</label>
              <div class="col-12">
                <input type="text" name="name" value="{{old('name')}}" class="form-control @error('name') is-invalid @enderror" id="input-name" placeholder="نام و نام خانوادگی">
                @error('name')
                  <span class="invalid-feedback" role="alert">
                    {{$message}}
                  </span>
                @enderror
              </div>
            </div>
            <div class="form-group">
              <label for="input-phone" class="col-12 control-label">شماره موبایل</label>
              <div class="col-12">
                <input type="text" name="phone" value="{{old('phone')}}" class="form-control @error('phone') is-invalid @enderror " id="input-phone" placeholder="شماره موبایل">
                  @error('phone')
                    <span class="invalid-feedback" role="alert">
                      {{$message}}
                    </span>
                  @enderror
              </div>
            </div>
            <div class="form-group">
              <label for="permissions" class="col-sm-4 control-label">دسترسی ها </label>
              <select name="permissions[]" id="permissions" class="form-control @error('permissions') is-invalid @enderror " multiple>
                @foreach (\App\Permission::all() as $permission)
                  <option value="{{ $permission->id }}">{{$permission->label}}</option>
                @endforeach
              </select>
              @error('permissions')
                <span class="invalid-feedback" role="alert"> 
                  {{$message}}
                </span>
              @enderror
            </div>
            <div class="form-group">
              <label for="roles" class="col-sm-4 control-label">گروه کاربران </label>
              <select name="roles[]" id="roles" class="form-control @error('roles') is-invalid @enderror " multiple>
                @foreach (\App\Role::all() as $role)
                  <option value="{{ $role->id }}">{{$role->label}}</option>
                @endforeach
              </select>
              @error('roles')
                <span class="invalid-feedback" role="alert"> 
                  {{$message}}
                </span>
              @enderror
            </div>
          </div>
          <div class="card-footer">
            <a href="{{route('admin.user.index')}}" type="submit" class="btn btn-default">لغو</a>
              <button type="submit" class="btn btn-info float-left">ثبت</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endcomponent