@component('admin.layouts.content',['title'=>'ویرایش شهر'])
    
  @slot('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
  <li class="breadcrumb-item "><a href="{{route('admin.city.index')}}">لیست شهر ها</a></li>
    <li class="breadcrumb-item active">ویرایش شهر جدید</li>
  @endslot
  @slot('buttonBox')
    <a href="{{ route('admin.city.index') }}" class="btn btn-default"><i class="fa fa-reply"></i></a>
    <button type="submit" form="creat-form" class="btn btn-primary"><i class="fa fa-save"></i></button>
@endslot
@slot('script')
    <script>
    $('#state').select2({});
    </script>
@endslot
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">فرم ویرایش شهر </h3>
        </div>
        <form action="{{route('admin.city.update',$city->id)}}" id="creat-form" method="POST" class="form-horizontal">
          @csrf
          @method('PATCH')
          <div class="card-body">
            <div class="form-group">
              <label for="input-name" class="col-12 control-label">{{__('app.city_name')}}</label>
              <div class="col-12">
                <input type="text" name="name" value="{{$city->name}}" class="form-control @error('name') is-invalid @enderror" id="input-name" placeholder="{{__('app.city_name')}} ">
                @error('name')
                  <span class="invalid-feedback" role="alert">
                    {{$message}}
                  </span>
                @enderror
              </div>
            </div>
            <div class="form-group">
                <label for="state" class="col-sm-4 control-label">{{__('app.state_name')}} </label>
                <select name="state_id" id="state" class="form-control @error('state_id') is-invalid @enderror ">
                    @foreach (\App\State::all() as $state)
                <option value="{{$state->id }}" {{$city->state_id == $state->id ? 'selected' :''}}>{{$state->name}}</option>
                    @endforeach
                </select>
                @error('state_id')
                    <span class="invalid-feedback" role="alert"> 
                        {{$message}}
                    </span>
                @enderror
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endcomponent