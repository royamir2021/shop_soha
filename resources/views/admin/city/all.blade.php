@component('admin.layouts.content',['title'=>'لیست شهر ها'])
  @slot('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
  <li class="breadcrumb-item ">لیست شهر ها</li>
  @endslot
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header pb-3">
          <h3 class="card-title">لیست شهر ها </h3>
          <div class="card-tools admin-search-box d-flex">
            <div class="input-group input-group-sm">
              <form action="">
                <input type="text"  value="{{request('search')}}" name="search" class="form-control float-right" placeholder="جستجو">
                <div class="input-group-append">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </form>
            </div>
            <div class="btn-group-sm mr-1">
              @can('create-city')
              <a href="{{route('admin.city.create')}}" class="p-2 btn btn-info">ایجاد شهر جدید</a>
              @endcan
          </div>
          </div>
        </div>
        <div class="card-body table-responsive p-0">
          <table class="table table-hover">
            <tbody>
              <tr>
                <th>#</th>
                <th>{{__('app.just_name')}}</th>
                <th>{{__('app.action')}}</th>
              </tr>
              @foreach ($cities as $city)
                <tr>  
                  <td>{{$city->id}}</td>
                  <td>{{$city->name}}</td>
  
                  <td>
                    @can('edit-city')
                      <a href="{{route('admin.city.edit',$city->id)}}" class="btn-primary btn ml-1 float-right">{{__('app.edit')}}</a>
                    @endcan
                    @can('delete-city')
                      
                        <form class="float-right" method="POST" action="{{route('admin.city.destroy',$city->id)}}">
                          @csrf
                          @method('DELETE')
                          <button class="btn-danger btn ml-1"> {{__('app.remove')}}</button>
                        </form>
                      
                    @endcan
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="card-footer">
          {{$cities->render()}}
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
  @endcomponent