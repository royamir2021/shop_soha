@component('admin.layouts.content',['title'=>'تنظیمات'])
    
@slot('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
    <li class="breadcrumb-item active"> {{__('admin/setting.setting')}}</li>
@endslot

@slot('buttonBox')
    <a href="{{ route('admin.') }}" class="btn btn-default"><i class="fa fa-reply"></i></a>
    <button type="submit" form="setting-form" class="btn btn-primary"><i class="fa fa-save"></i></button>
@endslot

@slot('script')
    <script src="{{asset('vendor/laravel-filemanager/js/stand-alone-button.js')}}"></script>
    <script>
        var route_prefix = "/filemanager";
        $('.lfm').filemanager('image', {prefix: route_prefix});
    </script>
@endslot

  <div class="row">
    <div class="col-md-12">
      <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">{{__('admin/setting.setting')}}</h3>
        </div>
        <form action="{{route('admin.setting.update',$info->id)}}" id="setting-form" method="POST" class="form-horizontal">
            @csrf
            <div class="card-body">
                <div class="form-group col-12">
                    <div class="col-md-6 col-12 float-right">
                    <label for="input-store_name" class="col-12 control-label">{{__('admin/setting.shop_name')}}</label>
                    <div class="col-12">
                        <input type="text" name="store_name" value="{{$info->store_name?$info->store_name : old('store_name')}}" class="form-control @error('store_name') is-invalid @enderror" id="input-store_name" placeholder="{{__('admin/setting.shop_name')}} ">
                        @error('store_name')
                        <span class="invalid-feedback" role="alert">
                            {{$message}}
                        </span>
                        @enderror
                    </div>
                    </div>
                    <div class=" col-md-6 col-12 float-right">
                        <label for="input-owner_name" class="col-12 control-label">{{__('admin/setting.owner_name')}}</label>
                        <div class="col-12">
                        <input type="text" name="store_owner" value="{{$info->store_owner?$info->store_owner : old('store_owner')}}" class="form-control @error('store_owner') is-invalid @enderror" id="input-owner_name" placeholder="{{__('admin/setting.owner_name')}} ">
                        @error('store_owner')
                            <span class="invalid-feedback" role="alert">
                            {{$message}}
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group col-12">
                    <div class="col-md-6 col-12 float-right">
                        <label for="input-phone" class="col-12 control-label">{{__('admin/setting.phone')}}</label>
                        <div class="col-12">
                        <input type="text" name="store_phone" value="{{$info->store_phone?$info->store_phone : old('store_phone')}}" class="form-control @error('store_phone') is-invalid @enderror" id="input-phone" placeholder="{{__('admin/setting.phone')}} ">
                        @error('store_phone')
                            <span class="invalid-feedback" role="alert">
                            {{$message}}
                            </span>
                        @enderror
                        </div>
                    </div>
                    <div class="col-md-6 col-12 float-right">
                        <label for="input-phone2" class="col-12 control-label">{{__('admin/setting.phone2')}}</label>
                        <div class="col-12">
                        <input type="text" name="store_phone2" value="{{$info->store_phone2?$info->store_phone2 : old('store_phone2')}}" class="form-control" id="input-phone2" placeholder="{{__('admin/setting.phone2')}} ">
                        </div>
                    </div>
                </div>
                <div class="form-group col-12">
                    <label for="input-address" class="col-12 control-label">{{__('admin/setting.address')}}</label>
                    <div class="col-12">
                    <textarea type="text" cols="30" rows="2" name="address"  class="form-control @error('address') is-invalid @enderror" id="input-description" placeholder="{{__('admin/setting.address')}} ">{{$info->address?$info->address:old('address')}}</textarea>
                    @error('address')
                        <span class="invalid-feedback" role="alert">
                        {{$message}}
                        </span>
                    @enderror
                    </div>
                </div>
                <div class="form-group col-12">
                    <label for="input-title" class="col-12 control-label">{{__('admin/setting.title')}}</label>
                    <div class="col-12">
                    <input type="text" name="meta_title" value="{{$info->meta_title?$info->meta_title : old('meta_title')}}" class="form-control @error('meta_title') is-invalid @enderror" id="input-title" placeholder="{{__('admin/setting.title')}} ">
                    @error('meta_title')
                        <span class="invalid-feedback" role="alert">
                        {{$message}}
                        </span>
                    @enderror
                    </div>
                </div>
                <div class="form-group col-12">
                    <label for="input-description" class="col-12 control-label">{{__('admin/setting.description')}}</label>
                    <div class="col-12">
                    <textarea type="text" cols="30" rows="2" name="meta_description"  class="form-control @error('meta_description') is-invalid @enderror" id="input-description" placeholder="{{__('admin/setting.description')}} ">{{$info->meta_description?$info->meta_description : old('meta_description')}}</textarea>
                    @error('meta_description')
                        <span class="invalid-feedback" role="alert">
                        {{$message}}
                        </span>
                    @enderror
                    </div>
                </div>
                <div class="form-group col-12">
                    <div class="input-group col-md-6 col-12 float-right" style="align-items: center;">
                        <label for="">{{__('admin/setting.logo')}}</label>
                        <div class="input-group-btn flo ">
                        <a class="lfm d-block" data-input="thumbnail" data-preview="holder">
                            <div id="holder">
                                <img style="height: 5rem;" class="img-fluid" src="{{$info->store_logo?$info->store_logo:asset('/img/no-image.jpg')}}" alt="">
                            </div>
                        </a>
                        </div>
                        <input dir="ltr" id="thumbnail" value="{{$info->store_logo?$info->store_logo: old('image')}}" class="form-control" type="hidden" name="store_logo">
                    </div>
                    <div class="input-group col-md-6 col-12 float-right" style="align-items: center;">
                        <label for="">{{__('admin/setting.icon')}}</label>
                        <div class="input-group-btn flo ">
                        <a class="lfm d-block" data-input="icon" data-preview="holder2">
                            <div id="holder2">
                                <img style="height: 5rem;" class="img-fluid" src="{{$info->store_icon?$info->store_icon:asset('/img/no-image.jpg')}}" alt="">
                            </div>
                        </a>
                        </div>
                        <input dir="ltr" id="icon" value="{{$info->store_icon?$info->store_icon: old('image')}}" class="form-control" type="hidden" name="store_icon">
                    </div>
                </div>
                <div class="form-group col-12">
                    <div class="col-12 col-md-6 float-right">
                        <label for="input-maintenance" class="col-12 control-label">{{__('admin/setting.maintenance')}}</label>
                        <div class="col-12">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="maintenance" id="maintenance1" value="1" {{$info->maintenance == 1 ? "checked" : ""}}>
                                <label class="form-check-label" for="maintenance1">
                                    {{__('app.active')}}
                                </label><br>
                                <input class="form-check-input" type="radio" name="maintenance" id="maintenance2" value="0" {{$info->maintenance == 0 ? "checked" : ""}}>
                                <label class="form-check-label" for="maintenance2">
                                    {{__('app.deactive')}}
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class=" col-12 col-md-6 float-right">
                        <label for="input-review_guest" class="col-12 control-label">{{__('admin/setting.allow_review')}}</label>
                        <div class="col-12">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="review_guest" id="review_guest1" value="1" {{$info->review_guest == 1 ? "checked" : ""}}>
                                <label class="form-check-label" for="review_guest1">
                                    {{__('app.all')}}
                                </label><br>
                                <input class="form-check-input" type="radio" name="review_guest" id="review_guest2" value="0" {{$info->review_guest == 0 ? "checked" : ""}}>
                                <label class="form-check-label" for="review_guest2">
                                    {{__('app.user')}}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-12">
                    <div class="col-md-6 col-12 float-right">
                        <label for="input-limit_admin" class="col-12 control-label">{{__('admin/setting.limit_admin')}}</label>
                        <div class="col-12">
                            <input type="text" name="limit_admin" value="{{$info->limit_admin?$info->limit_admin:old('limit_admin')}}" class="form-control @error('limit_admin') is-invalid @enderror" id="input-limit_admin" placeholder="{{__('admin/setting.limit_admin')}} ">
                            @error('limit_admin')
                            <span class="invalid-feedback" role="alert">
                                {{$message}}
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6 col-12 float-right">
                        <label for="input-open_time" class="col-12 control-label">{{__('admin/setting.open_time')}}</label>
                        <div class="col-12">
                            <input type="text" name="open_time" value="{{$info->open_time?$info->open_time:old('open_time')}}" class="form-control @error('open_time') is-invalid @enderror" id="input-open_time" placeholder="{{__('admin/setting.open_time')}} ">
                            @error('open_time')
                            <span class="invalid-feedback" role="alert">
                                {{$message}}
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group col-12">
                    <label for="input-comment" class="col-12 control-label">{{__('admin/setting.comment')}}</label>
                    <div class="col-12">
                    <textarea type="text" cols="30" rows="2" name="comment"  class="form-control @error('comment') is-invalid @enderror" id="input-description" placeholder="{{__('admin/setting.comment')}} ">{{$info->comment?$info->comment:old('comment')}}</textarea>
                    @error('comment')
                        <span class="invalid-feedback" role="alert">
                        {{$message}}
                        </span>
                    @enderror
                    </div>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
@endcomponent