@component('admin.layouts.content',['title'=>'ایجاد بنر'])
    
  @slot('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
  <li class="breadcrumb-item "><a href="{{route('admin.banner.index')}}">لیست بنر ها</a></li>
    <li class="breadcrumb-item active">ایجاد بنر جدید</li>
  @endslot
  @slot('buttonBox')
    <a href="{{ route('admin.banner.index') }}" class="btn btn-default"><i class="fa fa-reply"></i></a>
    <button type="submit" form="creat-form" class="btn btn-primary"><i class="fa fa-save"></i></button>
@endslot
@slot('script')
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script>
    // Upload Image 
    
    $('.lfm').filemanager('image', {prefix: route_prefix});
    {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/stand-alone-button.js')) !!}
        
    var route_prefix = "/filemanager";
    $('#add_product_image').click(function(){
            let images = $('#gallery tbody');
            let id = images.children().length;
            images.append(
                createNewImage({
                    id
                })
            );
            runFileManager();
        });

        function runFileManager(){
            $('.lfm').filemanager('image', {prefix: route_prefix});
            {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/stand-alone-button.js')) !!}
        }
        let createNewImage = ({id})=>{
            return `
            <tr id="image-row-${id}">
            <td scope="col text-center">
                <span class="input-group-btn float-right flo">
                    <a class="lfm" data-input="thumbnail-${id}" data-preview="holder-${id}" class="btn btn-primary">
                        <div id="holder-${id}" class="ml-3">
                            <img style="height: 5rem;" class="img-fluid" src="{{asset('/img/no-image.jpg')}}" alt="">
                        </div>
                    </a>
                </span>
                <input id="thumbnail-${id}" class="form-control mt-3 w-75 d-none " type="text" name="banners[${id}][images]">
            </td>
            <td scope="col text-center">
                <input id="thumbnail-${id}" class="form-control mt-3 w-75 " type="text" name="banners[${id}][url]">
            </td>
            <td scope="col">
                <button type="button" class="btn btn-sm btn-warning" onclick="document.getElementById('image-row-${id}').remove()">{{__('app.remove')}}</button>
            </td>
            </tr>`;
        }
    </script>
@endslot
  <div class="row">
    <div class="col-md-12">
      <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">فرم ایجاد بنر جدید</h3>
        </div>
        <form action="{{route('admin.banner.store')}}" id="creat-form" method="POST" class="form-horizontal">
          @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="input-name" class="col-12 control-label">{{__('app.banner_name')}}</label>
              <div class="col-12">
                <input type="text" name="name" value="{{old('name')}}" class="form-control @error('name') is-invalid @enderror" id="input-name" placeholder="{{__('app.banner_name')}} ">
                @error('name')
                  <span class="invalid-feedback" role="alert">
                    {{$message}}
                  </span>
                @enderror
              </div>
            </div>
            <div class="form-group">
                <label for="input-name" class="col-12 control-label">{{__('app.banner_name')}}</label>
                <div class="col-12">
                    <select name="status" class="form-control">
                        <option value="0">{{__('app.deactive')}}</option>
                        <option value="1">{{__('app.active')}}</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
              <label for="input-name" class="col-12 control-label">{{__('app.banner_location')}}</label>
              <div class="col-12">
                <select name="location" class="form-control">
                  <option value="">-- {{__('app.select')}} --</option>
                  <option value="home-banner">اسلایدر</option>
                  <option value="home-top-banner-1">بنر ۱ صفحه اصلی بالا </option>
                  <option value="home-top-banner-2">بنر ۲ صفحه اصلی بالا </option>
                </select>
              </div>
          </div>
            <div class="form-group">
                <div id="gallery">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col text-center">عکس</th>
                            <th scope="col text-center">لینک</th>
                            <th scope="col"></th>
                          </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                      </table>
                </div>
                <button class="btn btn-sm btn-success mt-3 mb-3" type="button" id="add_product_image">عکس جدید</button>
        
            </div>
        </div>
        </form>
      </div>
    </div>
  </div>
@endcomponent