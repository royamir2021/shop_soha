@component('admin.layouts.content',['title'=>'لیست بنر ها'])
  @slot('breadcrumb')
  <li class="breadcrumb-item"><a href="{{route('admin.')}}">خانه</a></li>
  <li class="breadcrumb-item ">لیست بنر ها</li>
  @endslot
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header pb-3">
          <h3 class="card-title">لیست بنر ها </h3>
          <div class="card-tools admin-search-box d-flex">
            <div class="input-group input-group-sm">
              <form action="">
                <input type="text"  value="{{request('search')}}" name="search" class="form-control float-right" placeholder="جستجو">
                <div class="input-group-append">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </form>
            </div>
            <div class="btn-group-sm mr-1">
              @can('create-banner')
              <a href="{{route('admin.banner.create')}}" class="p-2 btn btn-info">ایجاد بنر جدید</a>
              @endcan
          </div>
          </div>
        </div>
        <div class="card-body table-responsive p-0">
          <table class="table table-hover">
            <tbody>
              <tr>
                <th>#</th>
                <th>{{__('app.just_name')}}</th>
                <th>{{__('app.action')}}</th>
              </tr>
              @foreach ($banners as $banner)
                <tr>  
                  <td>{{$banner->id}}</td>
                  <td>{{$banner->name}}</td>
  
                  <td>
                    @can('edit-banner')
                      <a href="{{route('admin.banner.edit',$banner->id)}}" class="btn-primary btn ml-1 float-right">{{__('app.edit')}}</a>
                    @endcan
                    @can('delete-banner')
                      
                        <form class="float-right" method="POST" action="{{route('admin.banner.destroy',$banner->id)}}">
                          @csrf
                          @method('DELETE')
                          <button class="btn-danger btn ml-1"> {{__('app.remove')}}</button>
                        </form>
                      
                    @endcan
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
  @endcomponent